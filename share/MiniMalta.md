# MiniMalta {#PackageMiniMalta}

This is the sofware for Mini-MALTA

## Applications
- MiniMaltaThresholdScan: threshold scan 
- MiniMaltaNoiseScan: noise scan

## Libraries
- MiniMalta

## MiniMalta Library classes
- MiniMaltaTree: write MiniMalta data to root files
- MiniMaltaModule: use MiniMalta in MaltaMultiDAQ application
- MiniMalta: control MiniMalta
- MiniMaltaData: data format of MiniMalta
- MiniMaltaMaskLoop: helper for threshold scans
- MiniMaltaNoiseHandler: helper for noise scans
 
