import PyMiniMalta
import Herakles
import MaltaBase
import MaltaSetup
import time
import sys

connstr=MaltaSetup.MaltaSetup().getConnStr()

from MiniMaltaMap import *

mm=PyMiniMalta.MiniMalta()
mm.Connect(connstr)

mm.Reset()
time.sleep(1)

sys.exit()
