#!/usr/bin/env python

import os
import sys
import PyMiniMaltaData

md=PyMiniMaltaData.MiniMaltaData()

md.setPixel(1)
md.setBcid(4)
md.setGroup(2)
md.setParity(1)
md.setDcolumn(8)

md.pack()
print("Dgroup: %x" % md.getDummyGroup())

#print("Before: %08x%08x" % (md.getWord1(),md.getWord2()))
print("Before: %s" % md.toString())

md.unpack()

print("Dgroup: %x" % md.getDummyGroup())
#print("After:  %08x%08x" % (md.getWord1(),md.getWord2()))
print("After : %s" % md.toString())

print("Have a nice day")
