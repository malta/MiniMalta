import PyMiniMalta
import PyMiniMaltaData
import Herakles
import MaltaBase
import MaltaSetup
import time
import argparse
import sys

parser=argparse.ArgumentParser()
#parser.add_argument("reg",help="reg name")
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)

args=parser.parse_args()
chip=args.chip
connstr=MaltaSetup.MaltaSetup().getConnStr(chip)

from MiniMaltaMap import *

mm=PyMiniMalta.MiniMalta()
mm.Connect(connstr)

for i in range(0,1000000):
    time.sleep(0.01)     
    mm.Pulse(True,0x00000000)
    pass

sys.exit()
