#!/usr/bin/env python
import PyMiniMalta
import Herakles
import MaltaBase
import MaltaSetup
import time
import sys
import argparse
from MiniMaltaMap import *

parser=argparse.ArgumentParser()
#parser.add_argument("reg",help="reg name")
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)

args=parser.parse_args()
chip=args.chip
connstr=MaltaSetup.MaltaSetup().getConnStr(chip)

mm=PyMiniMalta.MiniMalta()
mm.Connect(connstr)

mm.Reset()

#mm.SetSlowReadOut(False)
mm.SetFastClock(False)
mm.SetSlowClock(False)
mm.SetSCClock(True)

mm.SetDefaults()

#time.sleep(1)

mm.Write(SC_IDB, 0x01ff)
mm.Write(SC_ICASN, 0x0000)
#mm.Write(SC_ITHR, 127)

mm.Write(SC_VPULSE_H, 0x00ff)
mm.Write(SC_VPULSE_L, 0)
#mm.Write(SC_VRESET_P, 0x01ff)
#mm.Write(SC_VRESET_D, 148)
#mm.Write(SC_VCASN, 70)

#mm.Write(SC_ENABLE_CONFIG, 0x001B)

mm.Write(SC_MASK_DIAG, 0x0000)
mm.Write(SC_MASK_COL, 0x0000)
mm.Write(SC_MASK_ROW_1, 0x00000000)
mm.Write(SC_MASK_ROW_2, 0x00000000)

#mm.Write(SC_PULSE_COL, 0x00ff)
#mm.Write(SC_PULSE_ROW_1, 0x000000ff)
#mm.Write(SC_PULSE_ROW_2, 0x00000000)

#mm.Write(SC_PIX_MON_IDAC, 0x0095)

mm.Send()
#for i in range (0,1000000):
#    time.sleep(0.1)
#    ####mm.Send()
#    mm.Recv()
#    print " "
#    mm.Dump(True);

mm.Recv()
mm.Dump(True);



mm.SetFastClock(True)
mm.SetSlowClock(True)
#mm.SetSlowReadOut(False)
mm.SetSCClock(False)

sys.exit()
