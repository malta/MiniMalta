#ifndef MINIMALTADATA_H
#define MINIMALTADATA_H

/**********************
 * Class MiniMaltaData
 * Ignacio.Asensi@cern.ch
 * Carlos.Solans@cern.ch
 * January 2019
 **********************/

#include <vector>
#include <cstdint>
#include <string>

/**
 * A MiniMALTA word is composed of 40 bits, and can contain up to 16 hits, 
 * which are read-out from the FPGA 
 * by the consecutive reading of two 32-bit words with MiniMalta::ReadMaltaWord.
 * These readings are identified as \c word1 and \c word2, and are decoded by MiniMaltaData.
 * MiniMaltaData requires an object to be created, and the value of \c word1, and 
 * \c word2 to be passed to MaltaData::setWord1 and MaltaData::setWord2.
 * The contents of the words then decoded by MaltaData::unpack, and the meaning
 * of the bits stored in internal memories that are accessible through dedicated
 * methods: MiniMaltaData::getRefbit, MiniMaltaData::getPixel, MiniMaltaData::getGroup, 
 * MiniMaltaData::getParity, MiniMaltaData::getDcolumn, MiniMaltaData::getBcid,
 * MiniMaltaData::getChipbcid, MiniMaltaData::getChipid, MiniMaltaData::getPhase, 
 * MiniMaltaData::getWinid, MiniMaltaData::getL1id. 
 * 
 * Additionally, the row and column values of each hit encoded in a MALTA word, 
 * are available through the MaltaData::getHitRow and MaltaData::getHitColumn.
 * The number of avilable hits in the MALTA word is MiniMaltaData::getNhits.
 *
 * It is possible to encode the MiniMALTA word as two 32-bit words for testing purposes.
 * This is done by setting the value of the bits with dedicated methods (
 * MiniMaltaData::setRefbit, MiniMaltaData::setPixel, MiniMaltaData::setGroup, 
 * MiniMaltaData::setParity, MiniMaltaData::setDcolumn, MiniMaltaData::setBcid, 
 * MiniMaltaData::setChipbcid, MiniMaltaData::setChipid, MiniMaltaData::setPhase, 
 * MiniMaltaData::setWinid, MiniMaltaData::setL1id )
 * and then calling MiniMaltaData::pack that makes the words available through 
 * MiniMaltaData::getWord1 and MiniMaltaData::getWord2.
 * 
 *
 * @brief Tool encode and decode MiniMALTA hit words.
 * @author Carlos.Solans@cern.ch
 * @author Ignacio.Asensi@cern.ch
 * @date January 2019
 **/

class MiniMaltaData{

 public:
  
  /**
   * @brief Initialize internal arrays
   **/
  MiniMaltaData();
  
  /**
   * @brief Delete internal arrays
   **/
  ~MiniMaltaData();

  /**
   * @brief Set Reference bit
   * @param value the reference bit
   **/
  void setRefbit(uint32_t value);

  /**
   * @brief Set pixel hit pattern (16 bits)
   * @param value the pixel hit pattern
   **/
  void setPixel(uint32_t value);

  /**
   * @brief Set group
   * @param value the group
   **/
  void setGroup(uint32_t value);

  /**
   * @brief Set dummy group
   * @param value the dummy group
   **/
  void setDummyGroup(uint32_t value);

  /**
   * @brief Set parity bit
   * @param value the parity bit
   **/
  void setParity(uint32_t value);

  /**
   * @brief Set double column 
   * @param value the double column
   **/
  void setDcolumn(uint32_t value);

  /**
   * @brief Set chip ID
   * @param value the chip ID
   **/
  void setChipid(uint32_t value);

  /**
   * @brief Set chip BCID
   * @param value the chip BCID
   **/
  void setChipbcid(uint32_t value);

  /**
   * @brief Set phase
   * @param value the phase
   **/
  void setPhase(uint32_t value);

  /**
   * @brief Set Window ID
   * @param value the window ID
   **/
  void setWinid(uint32_t value);

  /**
   * @brief Set BCID
   * @param value the BCID to set
   **/
  void setBcid(uint32_t value);

  /**
   * @brief Set L1ID
   * @param value the L1ID to set 
   **/
  void setL1id(uint32_t value);

  /**
   * @brief Set word1 as a uint32_t and use only 31 lower bits.
   * @param value the 1st word
   **/
  void setWord1(uint32_t value);

  /**
   * @brief Set word2 as a uint32_t and use only 31 lower bits.
   * @param value the 2nd word
   **/
  void setWord2(uint32_t value);

  /**
   * @brief Get Reference bit
   * @return uint32_t Reference bit
   **/
  uint32_t getRefbit();

  /**
   * @brief Get pixel group
   * @return uint32_t pixel group
   **/
  uint32_t getPixel();

  /**
   * @brief Get group
   * @return uint32_t group
   **/
  uint32_t getGroup();

  /**
   * @brief Get parity
   * @return uint32_t parity
   **/
  uint32_t getParity();

  /**
   * @brief Get double column
   * @return uint32_t double column
   **/
  uint32_t getDcolumn();

  /**
   * @brief Get BCID
   * @return uint32_t BCID
   **/
  uint32_t getBcid();

  /**
   * @brief Get Chip BCID
   * @return uint32_t Chip BCID
   **/
  uint32_t getChipbcid();

    /**
   * @brief Get chip ID
   * @return uint32_t chip ID
   **/
  uint32_t getChipid();
  
  /**
   * @brief Get phase
   * @return uint32_t phase
   **/
  uint32_t getPhase();
  
  /**
   * @brief Get Win ID
   * @return uint32_t win ID
  **/
  uint32_t getWinid();
  
  /**
   * @brief Get the 3 dummy bits of the groupID which should always be "111"
   * @return valid mM words will give "111"
  **/  
  uint32_t getDummyGroup();
  
  /**
   * @brief Get L1ID
   * @return uint32_t L1ID
   **/
  uint32_t getL1id();
  
  /**
   * @brief Get word1
   * @return uint32_t word
   **/
  uint32_t getWord1();

  /**
   * @brief Get word2
   * @return uint32_t word
   **/
  uint32_t getWord2();

  /**
   * @brief Get number of hits
   * @return uint32_t number of hits
   **/
  uint32_t getNhits();

  /**
   * @brief Get row for given hit
   * @param hit hit number
   * @return uint32_t row of the hit
   **/
  uint32_t getHitRow(uint32_t hit);

  /**
   * @brief Get column for given hit
   * @param hit hit number
   * @return uint32_t column of the hit
   **/
  uint32_t getHitColumn(uint32_t hit);

  /**
   * @brief get validity of dummy bits (refbit = 0b1, dummy bits of group = 0b111
   * @return bool validity of dummy bits
  **/
  bool getValidDummyBits();

  /**
   * @brief Get String representation
   * @return string of bits
   **/
  std::string toString();

  /**
   * @brief Get info
   * @return string 
   **/
  std::string getInfo();
  
  /**
   *@brief Turn a 32 bit unsigned integer into it's binary representation as a std::string
   *@param val the interger to convert into a string
   *@return string binary representation of the integer
   **/
  std::string MakeBinary(uint32_t val);

  /**
   * @brief dump to screen
   **/
  void dump();
    
  /**
   * @brief Encode the bits into a word
   **/
  void pack();

  /**
   * @brief Decode the word into bits
   **/
  void unPack();

  /**
   * @brief Decode the word into bits
   **/
  void unpack();
  
 private:
  uint32_t m_refbit;
  uint32_t m_pixel;
  uint32_t m_group;
  uint32_t m_dummyGroup; // added to be able to check the 3 unused bits of the group ID for word verification (Florian)
  uint32_t m_parity;
  uint32_t m_delay;
  uint32_t m_dcolumn;
  uint32_t m_chipbcid;
  uint32_t m_chipid;
  uint32_t m_phase;
  uint32_t m_winid;
  uint32_t m_bcid;
  uint32_t m_l1id;
  uint32_t m_nhits;
  uint32_t m_word1;
  uint32_t m_word2;
  bool     m_dummiesValid; // check valid dummy bits (Florian)
  std::vector<uint32_t> m_rows;
  std::vector<uint32_t> m_columns;

  uint32_t lst2int(uint32_t pos1, uint32_t pos2);
  void int2lst(uint32_t value, uint32_t pos1, uint32_t pos2);

};

#endif
