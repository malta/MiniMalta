#ifndef MINIMALTATREE_H
#define MINIMALTATREE_H

#include "TFile.h"
#include "TTree.h"
#include <string>
#include "MiniMalta/MiniMaltaData.h"
#include <chrono>

/**
 * MiniMaltaTree is a tool to store MiniMALTA data as a ROOT TTree.
 * A new file can be openned with MiniMaltaTree::Open. 
 * Parameters that will be stored for every entry until 
 * changed are run number (MiniMaltaTree::SetRunNumber),
 * ITHRES (MiniMaltaTree::SetIthres), L1ID (MiniMaltaTree::SetL1idC),
 * duplicate flag (MiniMaltaTree::SetIsDuplicate),
 * VHIGH (MiniMaltaTree::SetVhigh), VLOW (MiniMaltaTree::SetVlow),
 * and MASKI (MiniMaltaTree::SetMaskI).
 * MiniMaltaData (MiniMaltaTree::Set) will be decoded and stored as
 * different branches in the tree, as well as a time stamp (timer). 
 * After adding the parameters to MiniMaltaTree, it is necessary 
 * to call MiniMaltaTree::Fill.
 * MiniMaltaData can be written like the following:
 *
 * @verbatim
   MiniMaltaTree * mmt = new MiniMaltaTree();
   mmt->Open("file.root","RECREATE");

   mmt->SetIthres(200);
   mmt->SetRunNumber(5001);
 
   for(...){
     MiniMaltaData * md=...
     mmt->Add(md);
     mmt->Fill();
   }
   
   mmt->Close();
   delete mmt;
   @endverbatim
 *
 * MiniMaltaData can be read like the following:
 *
 * @verbatim
   MiniMaltaTree * mmt = new MiniMaltaTree();
   mmt->Open("file.root","READ");
 
   while(mmt->Next()){
     MiniMaltaData * md = mmt->Get()
     uint32_t l1id = mmt->GetL1idC();
     float timer = mmt->GetTimer();
   }
   
   mmt->Close();
   delete mmt;
   @endverbatim
 *
 * @brief Tool to store MiniMaltaData as a ROOT TTree.
 * @author Carlos.Solans@cern.ch
 * @date January 2019
 **/

class MiniMaltaTree{

 public:

  /**
   * @brief Create an empty MiniMaltaTree.
   **/
  MiniMaltaTree();
  
  /**
   * @brief Delete the MiniMaltaTree. Close any open file.
   **/
  ~MiniMaltaTree();
  
  /**
   * @brief Open a ROOT file.
   * @param filename The path to the file as a string.
   * @param options ROOT options for the file.
   **/
  void Open(std::string filename,std::string options);

  /**
   * @brief Close the ROOT file.
   **/
  void Close();

  /**
   * @brief Add a new entry to the Tree.
   **/
  void Fill();

  /**
   * @brief Add the contents of MiniMaltaData to the current entry.
   *        This also adds a time stamp (timer) to the entry.
   * @param data The MaltaData that needs to be stored in the tree entry.
   **/
  void Set(MiniMaltaData * data);

  /**
   * @brief Set the ITHRES in the current entry.
   * @param v The value of the ITHRES.
   **/
  void SetIthres(int v);

  void SetMaskI(int v);

  /**
   * @brief Set the run number in the current entry.
   * @param v The value of the run number.
   **/
  void SetRunNumber(int v);

  /**
   * @brief Set the L1ID in the current entry.
   * @param v The value of the L1ID.
   **/
  void SetL1idC(uint32_t v);

  /**
   * @brief Mark the MALTA word as duplicate.
   * @param v 1 if duplicate, 0 if not.
   **/
  void SetIsDuplicate(int v);

  /**
   * @brief Set the VHIGH in the current entry.
   * @param v The value of the VHIGH.
   **/
  void SetVhigh(int v);

  /**
   * @brief Set the VLOW in the current entry.
   * @param v The value of the VLOW.
   **/
  void SetVlow(int v);

  /**
   * @brief Get the current entry as a MiniMaltaData object.
   * @return A MiniMaltaData object.
   **/
  MiniMaltaData * Get();

  /**
   * @brief Get the VHIGH in the current entry.
   * @return The value of the VHIGH.
   **/
  int GetVhigh();

  /**
   * @brief Get the VLOW in the current entry.
   * @return The value of the VLOW.
   **/
  int GetVlow();

  /**
   * @brief Get the ITHRES in the current entry.
   * @return The value of the ITHRES.
   **/
  int GetIthres();

  /**
   * @brief Get the run number in the current entry.
   * @return The value of the run number.
   **/
  int GetRunNumber();

  /**
   * @brief Get the L1ID of the current entry.
   * @return The value of the L1ID.
   **/
  uint32_t GetL1idC();

  /**
   * @brief Get the duplicate flag of the entry.
   * @return The duplicate flag.
   **/
  int GetIsDuplicate();

  /**
   * @brief Get the timestamp of the entry.
   * @return The timestamp of the run number.
   **/
  float GetTimer();

  /**
   * @brief Move the internal pointer to the next entry in the tree.
   * @return 0 if there are no more entries to read, -1 if there was an error.
   **/
  int Next();

  /**
   * @brief Get underlaying ROOT file used by MiniMaltaTree.
   * @return The ROOT TFile object.
   **/
  TFile* GetFile();

 private:
  
  TFile * m_file;
  TTree * m_tree;
  std::chrono::steady_clock::time_point m_t0;
  
  uint32_t pixel;
  uint32_t group;
  uint32_t parity;
  uint32_t delay;
  uint32_t dcolumn;
  uint32_t chipbcid;
  uint32_t chipid;
  uint32_t phase;
  uint32_t winid;
  uint32_t bcid;
  uint32_t l1id;
  uint32_t run;
  uint32_t l1idC;
  float    timer;
  uint32_t word1;
  uint32_t word2;
  uint32_t coordX;
  uint32_t coordY;
  uint32_t ithres;
  uint32_t vhigh;
  uint32_t vlow;
  uint32_t maskI;
  uint32_t isDuplicate;

  MiniMaltaData m_data;
  uint64_t m_entry;
};

#endif
