#ifndef MINIMALTANOISEHANDLER_H
#define MINIMALTANOISEHANDLER_H

#include <vector>
#include <map>
#include <string>
#include <cstdint>

/**
 * @brief Noise handler for MiniMalta
 * @author Valerio.Dao@cern.ch
 * @date February 2019
 */
class MiniMaltaNoiseHandler{
 public:
  
  /**
   * @brief Create an empty MiniMaltaNoiseHandler class. 
   * @param chipName name of the chip (WXRY)
   * @param is_irradiated true if the chip is irradiated
   */
  MiniMaltaNoiseHandler(std::string chipName, bool is_irradiated );

  /**
   * @brief Clear the internal memories.
   **/
  ~MiniMaltaNoiseHandler();

  /**
   * @brief Enable the verbose mode
   * @param enable enable verbose if true
   **/
  void SetVerbose(bool enable);

  /**
   * @brief return number of steps
   * @return The number of steps
   **/
  int GetNSteps();

  /**
   * @brief Main method to load pixel mask pattern from pixel coordinates
   * @param xpair Vector of integers with X positions.
   * @param ypair Vector of integers with Y positions.
   * @return True if file was processed properly.
   **/
  bool ProcessPairs(std::vector<int> xpair, std::vector<int> ypair);

  /**
   * @brief Main method to load pixel mask pattern from automatized analysis
   * @param fileName Path to the file to process.
   * @return True if file was processed properly.
   **/
  bool ProcessFile(std::string fileName); 

  /**
   * @brief Main method to load pixel mask pattern from automatized analysis
   * @param fileName Path to the file to process.
   * @return True if file was processed properly.
   **/
  bool ProcessTextFile(std::string fileName);

  /**
   * @brief Return vector of pixel X positions
   * @return Vector of integers with X positions
   **/
  std::vector<int> GetPixelPosX();

  /**
   * @brief Return vector of pixel Y positions
   * @return Vector of integers with Y positions
   **/
  std::vector<int> GetPixelPosY();

  /**
   * @brief Return col masking word
   * @return The column masking word
   **/
  uint32_t GetMaskColWord();

  /**
   * @brief Return row masking word1
   * @return The row masking word1
   **/
  uint32_t GetMaskRowWord1();

  /**
   * @brief Return raw masking word2
   * @return The row masking word2
   **/
  uint32_t GetMaskRowWord2();
  
  /**
   * @brief Return diag masking word
   * @return The diagonal masking word1
   **/
  uint32_t GetMaskDiagWord();

  /**
   * @brief Dump the configuration on the screen.
   **/
  void Dump();

 private:

  int m_loopIndex;

  ////////MaskStrategy m_strategy;
  
  std::vector<std::string> m_chipNames;
  std::string m_chipName;
  
  bool m_verbose;
  bool m_is_irradiated;
  std::vector<int> m_posX;
  std::vector<int> m_posY;
  
  uint32_t m_pulseCol;
  uint32_t m_pulseRow1;
  uint32_t m_pulseRow2;

  uint32_t m_maskCol;
  uint32_t m_maskRow1;
  uint32_t m_maskRow2;
  uint32_t m_maskDiag;

};
#endif

