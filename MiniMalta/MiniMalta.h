#ifndef MINIMALTA_H
#define MINIMALTA_H

#include "ipbus/Uhal.h"
#include <map>

/**
 * MiniMalta contains all the necessary methods to control the 
 * MiniMALTA Pixel detector prototype through ipbus.
 * Upon creation of a new MiniMalta, there is no connection to the FPGA.
 * MiniMalta::Connect opens the communication with the FPGA through ipbus.
 * The configuration of MiniMALTA is stored in a single shift register
 * that is divided into 11 DACs, and is sent to the FPGA with 
 * MiniMalta::Send.
 * The DACs are \c IDB, \c ITHR, \c IBIAS, \c IRESET, \c ICASN, \c VCASN, 
 * \c VCLIP, \c VPULSE_HIGH, \c VPULSE_LOW, \c VRESET_P, and \c VRESET_D.
 * Each DAC contains 16 bits, typically one-hot encoded, and two extra bits
 * to indicate if the value is being monitored or overridden externally.
 * The default, and actual value of the individual registers, 
 * is stored in the FPGA project and accessible through MiniMalta::Write.
 *
 * @verbatim

   MiniMalta * minimalta = new MiniMalta("udp://192.168.200.10");
   
   minimalta->Write(MiniMalta::SC_IDB,100);
   minimalta->Write(MiniMalta::SC_ITHR, 30);
   minimalta->Write(MiniMalta::SC_IRESET, 10);
   minimalta->Write(MiniMalta::SC_ICASN, 10);
   minimalta->Write(MiniMalta::SC_VCASN, 64);
   minimalta->Write(MiniMalta::SC_VCLIP, 127);
   minimalta->Write(MiniMalta::SC_VPULSE_HIGH, 127);
   minimalta->Write(MiniMalta::SC_VPULSE_LOW, 127);
   minimalta->Write(MiniMalta::SC_VRESET_P, 45);
   minimalta->Write(MiniMalta::SC_VRESET_D, 64);

   minimalta->Send();
   @endverbatim
 *
 *
 * Additionally, the configuration of the chip can be carried out by
 * configuration strings with MiniMalta::SetConfigFromString. 
 * The statements have the same format as the ConfigParser statements 
 * (\c SB_IDB: \c 100 ). 
 * The available registers are listed in MiniMalta::REGISTERS.
 *
 * @verbatim

   MiniMalta * malta = new MiniMalta("udp://192.168.200.10");

   minimalta->SetConfigFromString("SC_IDB: 100");
   minimalta->SetConfigFromString("SC_ITHR: 30");
   minimalta->SetConfigFromString("SC_IRESET: 10");
   minimalta->SetConfigFromString("SC_ICASN: 10");
   minimalta->SetConfigFromString("SC_VCASN: 64");
   minimalta->SetConfigFromString("SC_VCLIP: 127");
   minimalta->SetConfigFromString("SC_VPULSE_HIGH: 127");
   minimalta->SetConfigFromString("SC_VPULSE_LOW: 127");
   minimalta->SetConfigFromString("SC_VRESET_P: 45");
   minimalta->SetConfigFromString("SC_VRESET_D: 64");

   minimalta->Send();
   @endverbatim
 *
 * 
 * @brief MiniMALTA control and read-out class
 * @author Ignacio.Asensi@cern.ch
 * @author Carlos.Solans@cern.ch
 * @date January 2019
 **/
class MiniMalta{
 public:
  
  static const uint32_t SCCONTROL=1;
  
  static const uint32_t SC_REG0=4;
  static const uint32_t SC_REGN=30;
  static const uint32_t SC_NREGS=27;

  static const uint32_t SC_RESET=4;
  static const uint32_t SC_PULSE=5;
  static const uint32_t TRIGGER=10;
  static const uint32_t SC_CLKEN=6;
  static const uint32_t FAST_K_EN =7;
  static const uint32_t SLOW_K_EN =8;
  static const uint32_t SC_SLEN =9;
  static const uint32_t RO_INFO=45;

  static const uint32_t RO_INFO_SLOW_FULL=23;
  static const uint32_t RO_INFO_SLOW_EMPTY=24;
  static const uint32_t RO_INFO_SLOW_PROG_EMPTY=25;
  static const uint32_t RO_INFO_SLOW_ALMOST_EMPTY=26;
  static const uint32_t IPBUS_FIFO_FILL_STATUS=48;

  static const uint32_t RO_CONFIG_1=40;
  static const uint32_t RO_CONFIG_2=41;
  static const uint32_t RO_CONFIG_3=42;
  static const uint32_t RO_SLOW_FIFO_EN=2;
  static const uint32_t RO_RESET=8;
  
  static const uint32_t TIMING_CONTROL=3;
  static const uint32_t L1A_COUNTER=47;

  enum REGISTERS {
    SC_VCASN = 4 ,
    SC_VCASP = 5 ,
    SC_VRESET_D = 6 ,
    SC_VRESET_P = 7 ,
    SC_VPULSE_H = 8 ,
    SC_VPULSE_L = 9 ,
    SC_VCLIP = 10 ,
    SC_ICASN = 11 ,
    SC_IBIAS = 12 ,
    SC_ITHR = 13 ,
    SC_IDB = 14 ,
    SC_IRESET = 15 ,
    SC_MASK_DIAG = 16 ,
    SC_MASK_COL = 17 ,
    SC_MASK_ROW_1 = 18 ,
    SC_MASK_ROW_2 = 19 ,
    SC_PULSE_COL = 20 ,
    SC_PULSE_ROW_1 = 21 ,
    SC_PULSE_ROW_2 = 22 ,
    SC_PIX_MON_IDAC = 23 ,
    SC_ENABLE_CONFIG = 24 ,
    SC_TEMP_0 = 25 ,
    SC_TEMP_1 = 26 ,
    SC_TEMP_2 = 27 ,
    SC_DTU_1 = 28 ,
    SC_DTU_2 = 29 ,
    SC_SEU_COUNT = 30 ,
  };
  
  
  /**
   * @brief Create an empty MiniMalta class. Initialize internal memories.
   */
  MiniMalta();

  /**
   * @brief Clear the internal memories.
   **/
  ~MiniMalta();
  
  /**
   * @brief Enable the verbose mode
   * @param enable enable verbose if true
   **/
  void SetVerbose(bool enable);

  /**
   * @brief Set the internal ipbus pointer
   * @param ipb pointer to ipbus::Uhal object
   **/
  void SetIPbus(ipbus::Uhal * ipb);

  /**
   * @brief Get the internal ipbus pointer
   * @return ipbus::Uhal pointer
   **/
  ipbus::Uhal * GetIPbus();
  
  /**
   * @brief Send the configuration to the chip through the SC_DATA
   * @return true if it worked
   **/
  bool Send();

  /**
   * @brief Receive the configuration from the chip through the SC_OUT and SC_ACK
   * @return true if it worked
   **/
  bool Recv();
  
  /**
   * @brief Write a register
   * @param pos register to write
   * @param value value to write
   * @param force write immediately to the chip
   **/
  void Write(int pos, int value, bool force=true);
  
  /**
   * @brief Write all the registers to the chip from memory
   **/
  void WriteAll();
  
  /**
  * @brief read
  * @param pos register to read
  * @param force true to read from the chip
  * @return uint32_t word
  **/
  uint32_t Read(int pos, bool force=true);
  
  /**
   * @brief Read all the registers from the chip into memory
   **/
  void ReadAll();

  /**
   * @brief Print the value of the registers to screen
   * @param update force to read from the chip
   **/
  void Dump(bool update=false);
  
  /**
   *@brief Turn a 32 bit unsigned integer into its binary representation as a std::string
   *@param val the interger to convert into a string
   *@return string binary representation of the integer
  **/  
  std::string MakeBinary(uint32_t val);
    
  /**
   * @brief force the defaults into the internal memory
   * @param force true to write values to chip
   **/
  void SetDefaults(bool force=true);
  
  /**
   * @brief Enable or disable the slow control clock
   * @param enable Enable the slow control clock if true.
   **/
  void SetSCClock(bool enable=true);

  /**
   * @brief Enable or disable the fast clock
   * @param enable Enable the fast clock if true.
   **/
  void SetFastClock(bool enable=true);

  /**
   * @brief Enable or disable the slow clock
   * @param enable Enable the slow clock if true.
   **/
  void SetSlowClock(bool enable=true);

  /**
   * @brief enable the slow readout
   * @param enable enable if true
   **/
  void SetSlowReadOut(bool enable=true);

  /**
   * @brief enable the slow fifo
   * @param enable enable if true
   **/
  void SetSlowReadOutFifo(bool enable=true);
  
  /**
   * @brief check if prog fifo is full
   * @return true if ipbus prog fifo is full
   **/
  bool IsFifoIpSlowProgEmpty();

  /**
   * @brief check if readout fifo is empty
   * @return true if ipbus slow fifo is empty
   **/
  bool IsFifoIpSlowEmpty();

  /**
   * @brief check if readout fifo is almost empty
   * @return true if ipbus slow fifo is almost empty
   **/
  bool IsFifoIpSlowAlmostEmpty();

  /**
   * @brief check if readout fifo is full
   * @return true if ipbus slow fifo is full
   **/
  bool IsFifoIpSlowFull();
  
  /**
   * @brief resetting slow fifo
   **/
  void ResetSlowFifo();
  
  /**
   * @brief setting full config from text file (for AIDA telescope)
   * @param str string to parse. Might contain multiple lines
   **/
  void SetConfigFromString(const std::string& str);
  
  /**
   * @brief get fifo status and update local value
   * @param quiet print status if true
   **/
  void GetFifoStatus(bool quiet=false); 

  /**
   * @brief get the number of words in the fifo
   **/
  uint32_t GetFifoFillStatus(); 

  /**
   * @brief Get the default value of a register
   * @param pos Register to get
   **/
  uint32_t GetDefault(int pos);

  /**
   * @brief Pulse the chip
   * @param update previous value (if false will speed up)
   * @param previous value of the slow control register
   **/
  void Pulse(bool update, uint32_t previous);

  /**
   * @brief Pulse the chip multiple times
   * @param numPulses number of pulses to send
   * @param length length of each pulse
   **/
  void PulseN(uint32_t numPulses, uint32_t length=50);

  /**
   * @brief Send a trigger signal through ipbus
   * @param update previous value (if false will speed up)
   * @param previous value
   **/
  void Trigger(bool update, uint32_t previous);

  /**
   * @brief Get the override bit in a configuration register.
   * @param pos Register to set
   * @return True if the override value of the register is enabled.
   **/
  bool IsOverriden(int pos);

  /**
   * @brief Set the override bit in a configuration register.
   * @param pos Register to set
   * @param flag Set the override bit if true.
   **/
  void Override(int pos, bool flag);

  /**
   * @brief Get the monitor bit in a configuration register.
   * @param pos Register to set
   * @return True if the monitor value of the register is enabled.
   **/
  bool IsMonitored(int pos);

  /**
   * @brief Set the monitor bit in a configuration register.
   * @param pos Register to set
   * @param flag Set the monitor bit if true.
   **/
  void Monitor(int pos, bool flag);
  
  /**
   * @brief get value of a register from memory. (Hot encoded)
   * @param pos register to get
   * @return uint32_t word
   **/
  uint32_t getValue(int pos);

  /**
   * @brief Connect to the FPGA through ipbus
   * @param connstr Ipbus connection string: "tcp://hostname:port?target=device:port"
   * @return True if communication was established
   **/
  bool Connect(std::string connstr);
  
  /**
   * @brief decode hot encoded from value
   * @param val to decode
   * @return uint32_t value
   **/
  uint32_t decodeHotEncoded(int val);

  /**
   * @brief method to test new things
   * @return uint32_t value
   **/
  int TestMethod();

  /**
   * @brief Reset the chip
   **/
  void Reset();

  /**
   * @brief Read the MALTA words
   * @param values pointer to the array of words to read
   * @param numwords number of words to read
   **/
  void ReadMaltaWord(uint32_t * values, uint32_t numwords);
  
  /**
   * @brief Check that the set values are the same as the read values
   * @return true if consistent
   **/
  bool CheckConsistency();
  
  /**
   * @brief Set the L1A counter to external mode
   * @param enable enable or disable the external mode L1A counter
   **/
  void SetExternalL1A(bool enable);
  
  /**
   * @brief Reset the L1A counter
   **/
  void ResetL1Counter();
  
  /**
   * @brief Get the L1A counter
   * @return the L1A counter
   **/
  uint32_t GetL1Acounter();

  /**
   * @brief Get the name of the given MiniMALTA register
   * @return string with the register name
   **/
  std::string GetRegisterName(uint32_t reg);

  /**
   * @brief Mask all the matrix
   * @param enable to mask or unmask
   **/
   void MaskAll(bool enable);

 private:
  ipbus::Uhal * m_ipb;
  std::map<uint32_t, uint32_t> defs;
  std::map<uint32_t, uint32_t> values;
  std::map<uint32_t, uint32_t> values_2;
  std::map<uint32_t, std::string> names;
  bool verbose;
  uint32_t m_fifo_info;
  uint32_t m_fifo_fill_status;
  uint32_t m_sccontrol;
  
};
#endif

