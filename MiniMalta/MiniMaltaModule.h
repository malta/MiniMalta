#ifndef MINIMALTAMODULE_H
#define MINIMALTAMODULE_H

#include "MaltaDAQ/ReadoutModule.h"
#include "MiniMalta/MiniMalta.h"
#include "MiniMalta/MiniMaltaTree.h"

#include <string>
#include <thread>

/**
 * MiniMaltaModule is an implementation of a ReadoutModule for 
 * the test-beam data-taking application (MaltaMultiDAQ) to 
 * control MiniMALTA.
 * MiniMaltaModule can be loaded dynamically thrhough the ModuleLoader 
 * as a ReadoutModule object.
 *
 * @verbatim

   ReadoutModule * malta = ModuleLoader::getInstance()->newReadoutModule("MiniMalta","MiniMaltaModule",
                                                                         "PLANE_1","192.168.200.10","");
																		 
   @endverbatim
 * 
 * MiniMaltaModule connects to the MiniMALTA FPGA on creation, and configures it
 * for operation with MiniMaltaModule::Configure. 
 * The configuration is steered from the ReadoutConfig object that is 
 * loaded from a configuration text file through ConfigParser.
 * 
 * @verbatim

   [PLANE 1]
   type: MiniMALTA
   wafer: W4R10
   address: 192.168.200.10
   SC_IDB: 100
   SC_ITHR: 30
   SC_IRESET: 10
   SC_ICASN: 10
   SC_VCASN: 64
   SC_VCLIP: 127
   SC_VPULSE_HIGH: 127
   SC_VPULSE_LOW: 127
   SC_VRESET_P: 45
   SC_VRESET_D: 64
   @endverbatim
 *
 * After configuration, the module has to be set to running mode with  
 * MiniMaltaModule::Start that starts the MiniMaltaModule::Run thread,
 * that is stopped with MiniMaltaModule::Stop. The MiniMaltaModule::Run 
 * thread will continue to run until the MiniMaltaModule::Stop has been called.
 * 
 * While running, MiniMaltaModule fills 3 histograms, the distribution of the 
 * time of the hits (ReadoutModule::GetTimingHisto),
 * the number of hits per event (ReadoutModule::GetNHitHisto),
 * the hit map (ReadoutModule::GetHitMapHisto).

 * The methods implemented in this class are the following:
 * * MiniMaltaModule::SetInternalTrigger configures the plane for internal triggering.
 * * MiniMaltaModule::Configure configures the plane for data taking. 
 * * MiniMaltaModule::Start starts the data taking on the plane by spawning the Run method.
 * * MiniMaltaModule::Stop stops the data taking on the plane by stopping the run thread.
 * * MiniMaltaModule::Run implements the data taking loop, histogram and ntuple filling.
 * * MiniMaltaModule::EnableFastSignal enables the trigger signal out from the plane if any.
 * * MiniMaltaModule::DisableFastSignal disables the trigger signal out from the plane if any.
 *
 *
 * @brief MiniMALTA ReadoutModule for MaltaMultiDAQ
 * @author Carlos.Solans@cern.ch
 * @author Ignacio.Asensi@cern.ch
 * @date December 2018
 **/


class MiniMaltaModule: public ReadoutModule{
  
 public:
  
  MiniMaltaModule(std::string name, std::string address, std::string outdir);

  ~MiniMaltaModule();
    
  /**
   * @brief Configure MALTA for data taking
   **/
  void Configure();
  
  /**
   * @brief Start data acquisition thread
   * @param run the run number as a string
   * @param usePath use the path in the constructor
   **/
  void Start(std::string run, bool usePath = false);

  /**
   * @brief Stop the data acquisition thread
   **/
  void Stop();

  /**
   * @brief The method for the data acquisition thread
   **/
  void Run();

  /**
   * @brief Get the L1A from the FPGA
   * @return The number of L1A 
   **/
  uint32_t GetNTriggers();

  /**
   * @brief Enable the fast signal
   **/
  void EnableFastSignal();

  /**
   * @brief Disable the fast signal
   **/
  void DisableFastSignal();
  
 private:
      
  MiniMalta*      m_malta;
  MiniMaltaTree * m_ntuple;
  ReadoutConfig*  m_config;
};

#endif

