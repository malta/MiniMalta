#ifndef MINIMALTAMASKLOOP_H
#define MINIMALTAMASKLOOP_H

#include <vector>
#include <map>
#include <cstdint>

/**
 * @brief Tool for masking MiniMalta during a threshold scan.
 * @author Valerio.Dao@cern.ch
 * @date February 2019
 */
class MiniMaltaMaskLoop{
 public:
  
  enum MaskStrategy {
    MASK_SINGLE = 0 ,
    MASK_2      = 1 ,
    MASK_2DC    = 2 ,
    MASK_4      = 3 ,
    MASK_8      = 4 ,
    MASK_16     = 5 ,
  };
  
  
  /**
   * @brief Create an empty MiniMaltaMaskLoop class. 
   * @param mask which mask stratety to useenable enable verbose if true
   */
  MiniMaltaMaskLoop(MaskStrategy mask);

  /**
   * @brief Clear the internal memories.
   **/
  ~MiniMaltaMaskLoop();

  /**
   * @brief Enable the verbose mode
   * @param enable enable verbose if true
   **/
  void SetVerbose(bool enable);

  /**
   * @brief return number of steps
   **/
  int GetNSteps();

  /**
   * @brief Main method to load all the information for a given loop stage
   * @param loopIndex which step to consider
   **/
  bool ProcessLoop(int loopIndex); 

  /**
   * @brief Return vector of pixel X positions
   * @return Vector of integers with X positions
   **/
  std::vector<int> GetPixelPosX();

  /**
   * @brief Return vector of pixel Y positions
   * @return Vector of integers with Y positions
   **/
  std::vector<int> GetPixelPosY();

  /**
   * @brief Return col pulsing word
   * @return The col pulsing word
   **/
  uint32_t GetPulseColWord();

  /**
   * @brief Return row pulsing word1
   * @return The row pulsing word1
   **/
  uint32_t GetPulseRowWord1();

  /**
   * @brief Return row pulsing word1
   * @return The row pulsing word2
   **/
  uint32_t GetPulseRowWord2();

  /**
   * @brief Return col masking word
   * @return The col masking word
   **/
  uint32_t GetMaskColWord();

  /**
   * @brief Return raw masking word1
   * @return The row masking word1
   **/
  uint32_t GetMaskRowWord1();

  /**
   * @brief Return raw masking word2
   * @return The row masking word2
   **/
  uint32_t GetMaskRowWord2();

  /**
   * @brief Prepare the loop for single masking
   **/
  void PrepareLoop_Single();

  /**
   * @brief Prepare the loop for masking 1 out of 2
   **/
  void PrepareLoop_2();  

  /**
   * @brief Prepare the loop for masking 2 double columns
   **/
  void PrepareLoop_2dc();  

  /**
   * @brief Prepare the loop for masking 1 out of 4
   **/
  void PrepareLoop_4();

  /**
   * @brief Prepare the loop for masking 1 out of 8
   **/
  void PrepareLoop_8();

  /**
   * @brief Prepare the loop for masking 1 out of 16
   **/
  void PrepareLoop_16();

  /**
   * @brief Dump the configuration on the screen
   **/
  void Dump();

 private:

  int m_loopIndex;

  MaskStrategy m_strategy;
  bool m_verbose;
  std::vector<int> m_posX;
  std::vector<int> m_posY;
  
  uint32_t m_pulseCol;
  uint32_t m_pulseRow1;
  uint32_t m_pulseRow2;

  uint32_t m_maskCol;
  uint32_t m_maskRow1;
  uint32_t m_maskRow2;

};
#endif

