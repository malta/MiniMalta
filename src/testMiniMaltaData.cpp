#include <iostream>
#include <ctime>
#include "MiniMalta/MiniMaltaData.h"

using namespace std;

int main() {
  
  cout << "Test MiniMaltaData unpack" << endl;
  MiniMaltaData md;
    
  md.setPixel(17);
  md.setBcid(4);
  md.setGroup(2);
  md.setParity(1);
  md.setDcolumn(8);
  
  md.pack();

  cout << "Load MiniMalta word: " << md.toString() << endl;

  clock_t start = clock();
  uint32_t N_TEST=1E8;

  for(uint32_t i=0;i<N_TEST; i++){

    md.unpack();

  }

  double duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  double frequency = (double) N_TEST / duration;

  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [Hz]: " << frequency << endl;
  cout << "Have a nice day" << endl;
  return 0;
}
