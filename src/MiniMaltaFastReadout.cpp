//! -*-C++-*-
#include "MaltaDAQ/MiniMalta.h"
#include "MaltaDAQ/MiniMaltaData.h"
#include "MaltaDAQ/MiniMaltaTree.h"
#include "MaltaDAQ/MiniMaltaMaskLoop.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>

#include "TCanvas.h"
#include "TH1.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

int maxPulse=5; //VD test
bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

stringstream systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[]) {

  cout << "#####################################" << endl
       << "# Welcome to MiniMALTA Threshold Scan   #" << endl
       << "#####################################" << endl;
  
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  CmdArgInt  cNdaq('n',"ndaqs","ndaqs","number of pulses");
  CmdArgInt  cIDB('d',"idb" ,"idb", "IDB DAC value (1-255)");
  CmdArgInt  cIBIAS('b',"ibias" ,"ibias", "IBIAS DAC value (1-255)");
  CmdArgInt  cITHR('t',"ithr" ,"ithr", "ITHR DAC value (1-255)");
  CmdArgInt  cChip('c',"chip" ,"chip", "chip to talk to");

  //CmdArgStr cParam('d',"dac","dac","vlow, vhigh");

  CmdLine cmdl(*argv,&cVerbose,&cOutdir,&cNdaq,&cIDB,&cChip,&cITHR,&cIBIAS, NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  int chip=1;
  if (cChip.flags() & CmdArg::GIVEN) {
    chip=cChip;
  }
  ostringstream cString;
  cString << chip;

  string address=systemcall("python -c 'import MaltaSetup; print MaltaSetup.MaltaSetup().getConnStr("+cString.str()+")' | tail -n1").str();
  address.erase(address.size()-1); // to remove newline character

  ostringstream outputDir_str;
  outputDir_str<<cOutdir;
  ostringstream cIDB_str;
  cIDB_str<<cIDB;
  ostringstream cIBIAS_str;
  cIBIAS_str<<cIBIAS;
  ostringstream cITHR_str;
  cITHR_str<<cITHR;

  //Connect to MALTA
  std::cout << "before creating malta" <<std::endl;
  
  MiniMalta mm;
  MiniMaltaData md;
  mm.Connect(address);

  // set some pixels up for pulsing
  std::vector<int> selPixX = {0,8,16};
  std::vector<int> selPixY = {0,1,2};
  
  std::cout << "after creating malta" <<std::endl;
  cout << "#####################################" << endl
       << "# Configure the chip                #" << endl
       << "#####################################" << endl;

  mm.SetFastClock(true);
  mm.SetSlowClock(true);  
  mm.Reset();
  mm.SetDefaults();
  mm.SetSlowReadOut(true);

  uint32_t cols=0;
  uint32_t rows=0;
  uint32_t rows2=0;
  for(uint32_t i=0; i<selPixX.size(); i++){
      cols |= 1<<selPixX[i];
  }
  for(uint32_t i=0; i<selPixY.size(); i++){
      rows |= 1<<selPixY[i];
  }
  cout<<"cols: " <<cols<<endl;
  cout<<"rows: " <<rows<<endl;

  mm.SetFastClock(false);
  mm.SetSlowClock(false);
  
  mm.Reset();
  mm.SetDefaults();
  int limit = 20;
  bool isOk=false;
  int attempt=0;
  do { 
    //load confg        
    mm.SetDefaults();
    mm.Write(MiniMalta::SC_VPULSE_L   , 40+256);
    mm.Write(MiniMalta::SC_VPULSE_H   , 150+256);
    mm.Write(MiniMalta::SC_IDB        , 60+256);
    mm.Write(MiniMalta::SC_IBIAS      , 100);
    mm.Write(MiniMalta::SC_MASK_DIAG  , 0x0000);  
    mm.Write(MiniMalta::SC_VCLIP      , 127);
    mm.Write(MiniMalta::SC_ITHR       , 60);
    mm.Write(MiniMalta::SC_ICASN      , 10);
    mm.Write(MiniMalta::SC_PULSE_COL  , cols);
    mm.Write(MiniMalta::SC_PULSE_ROW_1, rows);
    mm.Write(MiniMalta::SC_PULSE_ROW_2, rows2);
    mm.Send();
    isOk=mm.CheckConsistency();
    attempt+=1;
    cout << "attempt: " << attempt << "/" << limit << "\r" << flush; 
  } while (not isOk and attempt<limit);
  cout << endl;
	
  mm.SetFastClock(true);
  mm.SetSlowClock(true);


  cout << "#####################################" << endl
       << "# Try to read out                   #" << endl
       << "#####################################" << endl;

  mm.ResetL1Counter();
  mm.SetSlowReadOutFifo(false);
  mm.ResetSlowFifo();
  mm.SetSlowReadOutFifo(true);

  uint32_t defaultV=0;
  defaultV=mm.Read(MiniMalta::SCCONTROL);

  for(uint32_t i=0; i<20; i++){
  //Pulse
    mm.Pulse(false,defaultV);
  }

  mm.SetSlowReadOutFifo(false);

  uint32_t words[98];   
  uint32_t numwords=98;
  for (uint32_t i=0;i<numwords;i+=2) {
    words[i+0]=0;
    words[i+1]=0;
  }
  //new code
  mm.GetFifoStatus(true);
  bool fifo2Empty= mm.IsFifoIpSlowProgEmpty();
  while( fifo2Empty==false and !(mm.IsFifoIpSlowEmpty() or mm.IsFifoIpSlowAlmostEmpty()) ) {
    mm.ReadMaltaWord(words,numwords);   
    for (uint32_t i=0;i<numwords;i+=2) {
      md.setWord1(words[i+0]);
      md.setWord2(words[i+1]);
    }
    md.unpack();
    cout << md.toString() << endl;
    mm.GetFifoStatus(true);
    fifo2Empty = mm.IsFifoIpSlowProgEmpty();
  }

  
 



}
