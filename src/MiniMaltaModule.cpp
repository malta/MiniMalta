#include "MiniMalta/MiniMaltaModule.h"
#include "MiniMalta/MiniMaltaNoiseHandler.h"
#include <iostream>
#include <sstream>
#include <istream>
#include <string>
#include <vector>
#include <bitset>

#include "TH2D.h"

using namespace std;

MiniMaltaModule::MiniMaltaModule(string name, string address, string outdir):
  ReadoutModule(name,address,outdir){
  m_malta=new MiniMalta();
  m_malta->Connect(address);
  //Configure();
  m_ntuple = 0;
  ostringstream os;
  os << m_name << "_timing";
  m_Htime=new TH1D(os.str().c_str(),"timing;time since L1A [ns]",2000,0,50000);
  os.str("");
  os << m_name << "_nPixel";
  //m_Hsize=new TH1D(os.str().c_str(),"pixel;# pixel per event",6,-0.5,5.5);
  m_Hsize=new TH1D(os.str().c_str(),"pixel;# pixel per event",2500,0,25000);
  os.str("");
  os << m_name << "_hitMap";
  m_Hmap =new TH2D(os.str().c_str(),"hitMap;pix X;pix Y",16,-0.5,15.5,64,-0.5,63.5);
}

MiniMaltaModule::~MiniMaltaModule(){
  delete m_malta;
}

void MiniMaltaModule::Configure(){
  cout << "Inside MiniMaltaModule::Configure() " << endl;
  //reset
  m_malta->SetFastClock(false);
  m_malta->SetSlowClock(false);
  m_malta->SetSCClock(true);
  m_malta->Reset();
  m_malta->SetSCClock(false);

  //dealing with external stuff
  m_malta->SetSlowReadOut(true);
  m_malta->ResetL1Counter();
  m_malta->SetExternalL1A(true);
  m_malta->SetDefaults();

  cout << " before SC " << endl;

  string wafer = "test";
  std::vector<int> xnoise, ynoise;

  if(GetConfig()){
    cout << "Get config from file" << endl;
    wafer=GetConfig()->GetWafer();
    for(uint32_t i=0;i<GetConfig()->GetMasks().size();i++){
      pair<int32_t,int32_t> mask=GetConfig()->GetMasks().at(i);
      xnoise.push_back(mask.first);
      ynoise.push_back(mask.second);
    }
    for(uint32_t i=0;i<GetConfig()->GetKeys().size();i++){
      string key=GetConfig()->GetKeys().at(i);
      m_malta->SetConfigFromString(key);
    }
  }else{
    xnoise.push_back(12);  ynoise.push_back(27);
    xnoise.push_back(5); ynoise.push_back(23);
    xnoise.push_back(2); ynoise.push_back(58);
    xnoise.push_back(8); ynoise.push_back(49);
    xnoise.push_back(13); ynoise.push_back(63);

    m_malta->Write(MiniMalta::SC_IDB  , 20+256);
  }

  MiniMaltaNoiseHandler nh(wafer, true);
  nh.ProcessPairs(xnoise, ynoise);

  nh.ProcessPairs(xnoise, ynoise);
  
  //nh.ProcessFile("~/MaltaSW/installed/share/data/MiniMaltaNoiseMaps/W5R3.txt");
 
  m_malta->Write(MiniMalta::SC_MASK_COL   , nh.GetMaskColWord()  );
  m_malta->Write(MiniMalta::SC_MASK_ROW_1 , nh.GetMaskRowWord1() );
  m_malta->Write(MiniMalta::SC_MASK_ROW_2 , nh.GetMaskRowWord2() );
  m_malta->Write(MiniMalta::SC_MASK_DIAG  , nh.GetMaskDiagWord() );  

  //m_malta->Write(MiniMalta::SC_MASK_COL   , 0x00fc);
  //m_malta->Write(MiniMalta::SC_MASK_COL   , 0x0 );
  //m_malta->Write(MiniMalta::SC_MASK_ROW_1 , 0x1FF80000 );
  //m_malta->Write(MiniMalta::SC_MASK_ROW_2 , 0x0 );
  //m_malta->Write(MiniMalta::SC_MASK_DIAG  , 0x0 );

  //actual SlowControl
  //m_malta->Write(MiniMalta::SC_IDB  , 20+256);
  //m_malta->Write(MiniMalta::SC_ICASN, 20);
  //copying here the part for masking
  m_malta->Send();
  cout << "Consistency: " << m_malta->CheckConsistency() << endl;
}

void MiniMaltaModule::Start(string run, bool usePath){
  string fname;
  cout << m_name << ": Start run " << run << endl;
  if(!usePath){
    ostringstream os;
    os << m_outdir << "/" << "run_" << run;
    if(run.find(".root")!=string::npos){os << ".root";}
  }
  else{
    string fname = run;
  }  
  
  //Create ntuple
  cout << m_name << ": Create ntuple " << endl;
  if(m_ntuple) delete m_ntuple;
  m_ntuple = new MiniMaltaTree();
  m_ntuple->Open(fname,"RECREATE");
  cout << m_name << ": Prepare for run " << endl;

  m_malta->SetSlowReadOutFifo(false);
  m_malta->ResetL1Counter();
  m_malta->SetFastClock(true);
  m_malta->SetSlowClock(true);
  m_malta->ResetSlowFifo();
  m_malta->SetSlowReadOutFifo(true);

  cout << m_name << ": Create data acquisition thread" << endl;
  m_thread = thread(&MiniMaltaModule::Run,this);
  m_running = true;
  cout << m_name << ": Data acquisition thread running" << endl;
}

void MiniMaltaModule::Stop(){
  cout << m_name << ": Stop" << endl;
  m_cont=false;
  cout << m_name << ": Join data acquisition thread" << endl;
  m_thread.join();
  m_running=false;
  m_malta->SetSlowReadOutFifo(false);
  m_malta->ResetL1Counter();
  m_malta->ResetSlowFifo();
  cout << m_name << ": Close tree" << endl;
  m_ntuple->GetFile()->WriteObject(m_Htime,"Timing");
  m_ntuple->GetFile()->WriteObject(m_Hsize,"NHit"  );
  m_ntuple->GetFile()->WriteObject(m_Hmap ,"HitMap");
  m_ntuple->Close();
}

void MiniMaltaModule::Run(){

  uint32_t words[400];   
  uint32_t numwords=400; 
  for (uint32_t i=0;i<numwords;i+=2) {
    words[i+0]=0;
    words[i+1]=0;
  }
  MiniMaltaData md;

  bool isFirstTEST=true;
  bool isFirst    =true;
  uint32_t l1id1, bcid1;
  uint32_t lastL1id=0;
  m_extL1id=0;
  uint32_t prevL1id=123456;
  uint32_t lastCount=0;
  uint32_t overflow=0;
  uint32_t coutCounter=0;

  uint32_t C_TOT =0;
  //uint32_t C_WOW =0;
  //uint32_t C_PROB=0;
  //uint32_t C_REF0=0;
  uint32_t C_JUMP=0;

  uint32_t readAttempt=0;
  //bool     print=false;
  int nHit=0;

  m_cont=true;


  while(m_cont){
    //cout << m_name << " before" << endl;
    /////////////////m_malta->Sync();
    m_malta->GetFifoStatus(true);
    uint32_t nWordsInFifo = m_malta->GetFifoFillStatus();
    if(nWordsInFifo%2 != 0) cout << "############ Uneven number of words in IPB FIFO!!!" << endl;
    numwords = (nWordsInFifo <= 400)?(nWordsInFifo):(400);
    cout << "##################### Reading " << numwords << " words from IPB FIFO" << endl;
    bool fifo2Empty= m_malta->IsFifoIpSlowProgEmpty();
    if (fifo2Empty){
      coutCounter++;
      if (coutCounter%20==0 && false) {
        cout << m_name << " :: Number of events since last EmptyFifo: " << lastCount
             << "   and last L1id is: " << lastL1id
             << " (" << overflow << ") --> cumulative: " << m_extL1id
             << endl;
      }

      lastCount=0;
      for (uint32_t i=0;i<400;i+=2) {
        words[i+0]=0;
        words[i+1]=0;
      }

      if ( m_maxEvt!=0) {
        if ( m_extL1id>m_maxEvt or C_TOT>m_maxEvt ) {           
          m_cont=false;
	  m_running=false;
        }
      }

      //generate some random L1a (note that the IPBus can't generate L1A which are short enough)
      if (m_internalTrigger) {
        m_malta->Trigger(0,false);
      }
    }
    /*
    if(fifo1Full){
      cout << m_name << " :: FIFO1 FULL : AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << endl;
    }
    if(fifo2Full){
      cout << m_name << " :: FIFO2 FULL : OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;
    }
    */
    if(m_debug) cout << m_name << " Before READ m_cont=" << m_cont << endl;
    //cout << " W1: " << words[0] << " w2: " <<  words[1] << endl;
    for (uint32_t i=0;i<400;i+=2) {
      words[i+0]=0;
      words[i+1]=0;
    }
    m_malta->ReadMaltaWord(words,numwords);
    /////cout << "W2: " << std::bitset<32>(words[1]) << ", W1: " << std::bitset<32>(words[0]) << endl;

    if(m_debug) cout << m_name << " After READ m_cont=" << m_cont << endl;

    // this is maybe outdated atm....
    /*
    uint32_t goodW =0;
    int NoRef =0;
    int emptyW=0;
    int badW  =0;

    for (uint32_t i=0;i<numwords;i+=2) {
      if ( words[i+0]!=0 and words[i+1]!=0 )   {
        if ( words[i]%2!=0) goodW++;
        else                NoRef++;
      }
      else  if ( (words[i+0]+words[i+1])==0 )  emptyW++;
      else                                     badW++;
    }
    */
    //if (goodW!=(numwords/2) ) cout << m_name << " :: PAIRS summary: " << goodW << " / " << NoRef << "/ " << emptyW << " / " << badW << endl;
    readAttempt++;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if(m_debug) cout << m_name << " Before processing m_cont=" << m_cont << endl;

    for(uint32_t i=0;i<numwords;i+=2){

      if(lastCount%1000000==0 && lastCount>0){
        cout << m_name << " :: Events so far: " << lastCount << endl;
      }

      m_extL1id = (lastL1id+overflow*4096);

      /*
      if(words[i+0]==0 or words[i+1]==0){
        if ( (words[i+0]+words[i+1])!=0 ) {
          if ( words[i+0]==0 ) print=true;
          if ( words[i+1]==0 ) print=false;
          cout << m_name << " :: WOW " << i/2 << " (" << readAttempt << ") :: " << words[i+0] << " , " << words[i+1] << endl;
          C_WOW+=1;
        } else {
          if (print) cout << m_name << " :: T: " << i/2 << " (" << readAttempt << ") :: EMPTY " << endl;
        }
        continue;
      } else {
        if (print) cout << m_name << " :: T: " << i/2 << " (" << readAttempt << ") :: " << words[i+0] << " , " << words[i+1] << endl;
      }

      if (words[i+0]%2==0) {
        C_REF0++;
        continue;
      }
      */
      C_TOT++;

      lastCount++;
      if(isFirst) isFirst = false;

      md.setWord1(words[i+0]);
      md.setWord2(words[i+1]);
      md.unpack();
      /////md.dump();
      /////////////if (md.getNhits()==0) md.dump();

      if (isFirstTEST) {
        isFirstTEST=false;
        m_extL1id=md.getL1id();
      }

      m_ntuple->SetL1idC(m_extL1id);

      bcid1  = md.getBcid();
      //phase1 = md.getPhase();
      //winid1 = md.getWinid();
      l1id1  = md.getL1id();

      bool isBad=false;
      lastL1id = l1id1;
      if (lastL1id!=prevL1id){
	if (prevL1id!=123456) {
	  m_Hsize->Fill(nHit);
	  if ( lastL1id!=(prevL1id+1) ) {
	    if ( lastL1id>prevL1id ) {
	      for (uint32_t c=0; c< (lastL1id-prevL1id)-1; c++)  m_Hsize->Fill(0.);
	    } else {
	      int newL1=lastL1id+4096;
	      for (uint32_t c=0; c< (newL1-prevL1id)-1; c++)  m_Hsize->Fill(0.);
	    }
	  }
	}
	nHit=0;
        if (lastL1id<prevL1id and prevL1id!=123456){
          if (prevL1id>lastL1id and (prevL1id-lastL1id)<2000) {// VD : was 3700
            isBad=true;
            C_JUMP++;
          } else {
            cout << m_name << " :: I am going to increase LAST: " << lastL1id << "  prev: " << prevL1id ;
            cout << endl;
            //cout << m_name << " :: " << md2.toString()  << endl;
            //cout << m_name << " :: " << md.toString()  << endl;
          }
          if (not isBad) overflow+=1;
        }
        if (not isBad) prevL1id=lastL1id;
        else           lastL1id=prevL1id;
      }
      
      if (not isBad) m_ntuple->Fill();

      nHit+=md.getNhits();
      //cout << "    motherfucker bcid is: " << bcid1 << endl;
      m_Htime->Fill(bcid1*25);
      for (unsigned int h=0; h<md.getNhits(); ++h) {
	m_Hmap->Fill(md.getHitColumn(h),md.getHitRow(h));
      }


      m_ntuple->Set(&md);
      if (not isBad) m_ntuple->Fill();

    } //words loop

    if(m_debug) cout << m_name << " After processing m_cont=" << m_cont << endl;
  } //while loop

}

uint32_t MiniMaltaModule::GetNTriggers() { return  m_extL1id; }

void MiniMaltaModule::EnableFastSignal() { }

void MiniMaltaModule::DisableFastSignal(){ }
