//! -*-C++-*-
#include "MiniMalta/MiniMalta.h"
#include "MiniMalta/MiniMaltaData.h"
#include "MiniMalta/MiniMaltaTree.h"
#include "MiniMalta/MiniMaltaMaskLoop.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>

#include "TCanvas.h"
#include "TH1.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

int maxPulse=1; //VD test
bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

stringstream systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[]) {

  cout << "#####################################" << endl
       << "# Welcome to MiniMALTA Threshold Scan   #" << endl
       << "#####################################" << endl;
  
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgBool cFast('y',"useFast","turn on fastReadout (experimental)");
  CmdArgStr  cAddress('a',"address","address","ipbus address");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  CmdArgStr  cPath('f',"basefolder","basefolder","output basePath directory",CmdArg::isREQ);
  CmdArgIntList cPixels('p',"pixels","pairs","pixels to select [x y]");
  CmdArgInt  cNdaq('n',"ndaqs","ndaqs","number of pulses",CmdArg::isREQ);
  CmdArgInt  cVlow('l',"vlow" ,"vlow", "Vlow value (0-255)",CmdArg::isREQ);
  CmdArgInt  cVhigh('h',"vhigh" ,"vhigh", "Vhigh value (0-255)",CmdArg::isREQ);
  CmdArgInt  cStep('s',"vstep" ,"vstep", "step of voltage scan (1-255)",CmdArg::isREQ);
  CmdArgInt  cIDB('d',"idb" ,"idb", "IDB DAC value (1-255)",CmdArg::isREQ);
  CmdArgInt  cIBIAS('b',"ibias" ,"ibias", "IBIAS DAC value (1-255)",CmdArg::isREQ);
  CmdArgInt  cITHR('t',"ithr" ,"ithr", "ITHR DAC value (1-255)",CmdArg::isREQ);
  CmdArgInt  cICASN('z',"icasn" ,"icasn", "ICASN DAC value (1-255)",CmdArg::isREQ); 
  CmdArgInt  cChip('c',"chip" ,"chip", "chip to talk to");
  CmdArgInt  cIndex('i',"index" ,"index", "index loop to stop to");
  CmdArgBool cNotSave('q',"notsave","do not save the file thrscan.root");  

  CmdArgInt  cVCASN('m',"vcasn" ,"vcasn", "VCASN DAC value (1-255)",CmdArg::isREQ); 
  CmdArgInt  cVRESETD('r',"vresetd" ,"vresetd", "VRESETD DAC value (1-255)",CmdArg::isREQ); 
	
  CmdArgInt  cSUB('u',"sub" ,"sub", "SUB voltage",CmdArg::isREQ); 		// these are negative in reality, but this is already assumed in the analysis
  CmdArgInt  cPWELL('w',"pwell" ,"pwell", "PWELL voltage",CmdArg::isREQ); 	// script. also should be written 10x the real value (eg.  -6.5V = 65! )


  CmdArgInt  cTID('g',"tid" ,"tid", "TID dose [MRad]");      // these two are optional, TID will default to 0 if not specified, and the temperature to +25
  CmdArgInt  cTEMP('e',"temperature" ,"temperature", "temperature a t which the test was conducted"); 

  CmdArgInt  cSkip('k',"skipRow" ,"skipRow", "just every n-th row will be pulsed if this is enabled"); 

	





  //CmdArgStr cParam('d',"dac","dac","vlow, vhigh");

  CmdLine cmdl(*argv,&cVerbose,&cFast,&cOutdir,&cAddress,&cPixels,&cVlow,&cVhigh,&cStep,&cNdaq,&cIDB,&cChip,&cPath,&cIndex,&cITHR,&cICASN,&cIBIAS,&cVCASN,&cVRESETD,&cSUB,&cPWELL,&cTID,&cTEMP,&cSkip,&cNotSave, NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  int mIndex=66666666;
  if (cIndex.flags() & CmdArg::GIVEN) {
    mIndex=cIndex;
  }

  int chip=1;
  if (cChip.flags() & CmdArg::GIVEN) {
    chip=cChip;
  }
  ostringstream cString;
  cString << chip;


  int TID = 0;
  if(cTID.flags() & CmdArg::GIVEN){
    TID=cTID;
}

  int TEMP = 25;
  if(cTEMP.flags() & CmdArg::GIVEN){
    TEMP=cTEMP;
  }



  string address;
  if(cAddress.flags() & CmdArg::GIVEN){
    address=cAddress;
  }else{
    address=systemcall("python -c 'import MaltaSetup; print MaltaSetup.MaltaSetup().getConnStr("+cString.str()+")' | tail -n1").str();
    address.erase(address.size()-1); // to remove newline character
  }

  std::cout << address <<"qqq"<<std::endl;
  string ChipFolder;
  ChipFolder=cPath;
  string baseDir="ThScansMiniMalta/"+ChipFolder+"/";
  string outdir = baseDir+(cOutdir.flags() & CmdArg::GIVEN?cOutdir:"Base");
  ostringstream cIDB_str;
  cIDB_str<<cIDB;
  ostringstream cIBIAS_str;
  cIBIAS_str<<cIBIAS;
  ostringstream cITHR_str;
  cITHR_str<<cITHR;
  ostringstream cICASN_str; 
  cICASN_str<<cICASN;
  ostringstream cVCASN_str; 
  cVCASN_str<<cVCASN;
  ostringstream cVRESETD_str; 
  cVRESETD_str<<cVRESETD;
  ostringstream cSUB_str; 
  cSUB_str<<cSUB;
  ostringstream cPWELL_str; 
  cPWELL_str<<cPWELL;
  ostringstream cTEMP_str; 
  cTEMP_str<<TEMP;
  ostringstream cTID_str; 
  cTID_str<<TID;

  
  // switch to single pixel pulsing
  bool onePix_pulse = false;
  if(cPixels.flags() & CmdArg::GIVEN){
    onePix_pulse=true;
    cout<<"p arg is given, probing only ONE pixel"<<endl;
  }


  int Skip = 1;
  if(cSkip.flags() & CmdArg::GIVEN){
    Skip=cSkip;
    outdir+="_SKIPPED_ROWS";
	
  }

	

  outdir+="_SUB_"+cSUB_str.str();
  outdir+="_PWELL_"+cPWELL_str.str();
  outdir+="_VCASN_"+cVCASN_str.str();
  outdir+="_VRESETD_"+cVRESETD_str.str();
  outdir+="_IDB_"+cIDB_str.str();
  outdir+="_ITHR_"+cITHR_str.str();
  outdir+="_ICASN_"+cICASN_str.str();
  outdir+="_IBIAS_"+cIBIAS_str.str();
  outdir+="_TID_"+cTID_str.str();
  outdir+="_TEMP_"+cTEMP_str.str();



  if (outdir!="") systemcall("mkdir -p "+outdir);

  vector<int> selPixX,selPixY;  
  for(uint32_t i=0; i<cPixels.count(); i++){
    std::cout << " val: " << cPixels[i] << std::endl;
    if(i%2==0){selPixX.push_back(cPixels[i]);}
    else      {selPixY.push_back(cPixels[i]);}
  }

  //////////signal(SIGINT,handler);

  //MiniMaltaMaskLoop class
  //MiniMaltaMaskLoop ml(MiniMaltaMaskLoop::MASK_SINGLE);
  //MiniMaltaMaskLoop ml(MiniMaltaMaskLoop::MASK_2DC);
  MiniMaltaMaskLoop ml(MiniMaltaMaskLoop::MASK_16);
  ml.SetVerbose(true);
  //for (int l = 0; l<ml.GetNSteps(); l++){
  //  ml.ProcessLoop(l);
  //  if (l==20) return 1;
  //  }
  //return 1;

  //Connect to MALTA
  std::cout << "before creating malta" <<std::endl;
  
  MiniMalta mm;
  mm.Connect(address);
  
  std::cout << "after creating malta" <<std::endl;
  cout << "#####################################" << endl
       << "# Configure the chip                #" << endl
       << "#####################################" << endl;
  
  mm.SetFastClock(true);
  mm.SetSlowClock(true);
  //mm.SetSCClock(true);
  
  mm.Reset();
  mm.SetDefaults();
  mm.SetSlowReadOut(true);


  uint32_t cols=0;
  uint32_t rows=0;
  uint32_t rows2=0;
  for(uint32_t i=0; i<selPixX.size(); i++){
      cols |= 1<<selPixX[i];
  }
  for(uint32_t i=0; i<selPixY.size(); i++){
      rows |= 1<<selPixY[i];
  }
  cout<<"cols: " <<cols<<endl;
  cout<<"rows: " <<rows<<endl;
 

  cout << "#####################################" << endl
       << "# Start Parameter loop              #" << endl
       << "#####################################" << endl;

  MiniMaltaData md;
 

  //Create ntuple
  cout << "Create ntuple: ";
  ostringstream os;
  os << outdir  << "/thrscan.root";
  string fname = os.str();
  cout << fname << endl;
  MiniMaltaTree mt;
  mt.Open(fname,"RECREATE");
  std::cout << "*************" << fname << std::endl;
  int vLow = cVlow;
  if(vLow < 0 || vLow > 149){ //255
    cout<<"you gave me vlow = "<<vLow<<", please give valid cLow number!!!"<<endl;
    return 1;
  } else{
    mm.Write(MiniMalta::SC_VPULSE_L, vLow+256);
    cout<<"vlow = "<<vLow<<endl;
  }

  int vHigh = cVhigh;
  if(vHigh < 0 || vHigh > 149 || vLow >= vHigh){ //255
    cout<<"you gave me vHigh = "<<vHigh<<", please give valid cHigh number!!!"<<endl;
    return 1;
  } else{
    cout<<"vhigh = "<<vHigh<<endl;
    mt.SetVhigh(vHigh);
  } 

  int vStep = cStep;
  //if(vStep > (vHigh-vLow)){
  //  cout<<"you gave me vStep = "<<vStep<<", please give valid cStep number!!!"<<endl;
  //  return 1;
  //}  else{
  //  cout<<"vStep = "<<vStep<<endl;
  //} 
  
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //instantiating stuff
  TH1F* h_base=new TH1F("base","base;V_LOW (DAC);counts",100,vLow-vStep,vHigh+vStep);
  h_base->SetDirectory(0);
  vector<TGraphErrors*> v_plots;
  int max_step = ml.GetNSteps();

  if(onePix_pulse) max_step = 1;

  // mini ntuple to store th scan data
  string outdirtot = outdir+"/"+outdir+"_ntuple.root";
  TFile *th_ntuple = new TFile( (outdirtot).c_str(), "RECREATE");
  TTree *th_tree = new TTree("Thres","DataStream");
  int tree_pixX, tree_pixY;
  float tree_thres, tree_sigma, tree_chi2;
  th_tree->Branch("pixX",   &tree_pixX,   "pixX/i");
  th_tree->Branch("pixY",   &tree_pixY,   "pixY/i");
  th_tree->Branch("thres",  &tree_thres,  "thres/f");
  th_tree->Branch("sigma",  &tree_sigma,  "sigma/f");
  th_tree->Branch("chi2",   &tree_chi2,   "chi2/f");
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // main loop over pixels configurations
  for (int l=0;l<max_step; l++){
    if (l>=mIndex) continue;
    if (l%Skip!=0) continue;   // Skip defaults to 1 if it is not specified
    // if (l%4!=0) continue;
    mt.SetMaskI(l);
    mm.Reset();    

    if(!onePix_pulse){
      cout << "VALERIO LOOP IS: " << l << endl;
      ml.ProcessLoop(l);
      v_plots.clear();
      selPixX=ml.GetPixelPosX();
      selPixY=ml.GetPixelPosY();
    }

    for (unsigned int p=0; p<selPixX.size(); p++) {
      TGraphErrors* g=new TGraphErrors();
      g->SetMarkerStyle(20);
      g->SetMarkerSize(0.9);
      g->SetLineWidth(2);
      v_plots.push_back(g);
    }

    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //Parameter loop
    int paramVal=0;
    cout << "IDB IS: " << cIDB << endl;
    cout << "ITHR IS: " << cITHR << endl;
    cout << "ICASN IS: " << cICASN << endl;
    cout << "IBIAS IS: " << cIBIAS << endl;
    int count=-1;
    mt.SetIthres(cIDB);

    for(paramVal=vLow; paramVal<=vHigh; paramVal+=vStep){
      count++;
      mt.SetVlow(paramVal);
      // cout << "Setting vlow: " << paramVal << " of " << paramMax << endl;
      //cout<<"cNdaq "<<cNdaq<<endl;

      if(!onePix_pulse){     
        cols =ml.GetPulseColWord();
        rows =ml.GetPulseRowWord1();
        rows2=ml.GetPulseRowWord2();
      }

      //ugly shit!!!!
      bool has15=false;
      bool has21=false;
      bool has48=false;
      /*
      for (unsigned int p=0; p<selPixY.size(); p++) {
        if (selPixY.at(p)==15) has15=true;
        if (selPixY.at(p)==21) has21=true;
        if (selPixY.at(p)==48) has48=true;
      }
      */

      mm.SetFastClock(false);
      mm.SetSlowClock(false);
      
      int limit=2;
      if (chip==2) limit=2;
      bool isOk=false;
      int attempt=0;
      do { 
        //load confg        
	mm.SetDefaults();
	mm.Write(MiniMalta::SC_VPULSE_L   , paramVal+256);
	mm.Write(MiniMalta::SC_VPULSE_H   , 150+256); //255
	mm.Write(MiniMalta::SC_PULSE_COL  , cols);
	mm.Write(MiniMalta::SC_PULSE_ROW_1, rows);
	mm.Write(MiniMalta::SC_PULSE_ROW_2, rows2);
	mm.Write(MiniMalta::SC_IDB        , cIDB+256);
        mm.Write(MiniMalta::SC_IBIAS      , cIBIAS);
	mm.Write(MiniMalta::SC_MASK_DIAG  , 0x0000);  
       
       // mm.Write(MiniMalta::SC_VCLIP      , 50); // CHANGED (uncommented) for fe-55 analysis!!!
        mm.Write(MiniMalta::SC_ITHR       , cITHR);
	mm.Write(MiniMalta::SC_ICASN      , cICASN );


	mm.Write(MiniMalta::SC_VRESET_D   , cVRESETD );  // Uncommented for entry upon function call
 	mm.Write(MiniMalta::SC_VCASN      , cVCASN );	   // Uncommented for entry upon function call	(put 170 otherwise)

        //mm.Write(MiniMalta::SC_VCASN      , 170 ); // trick to run the chip at PWELL=-6V

	//mm.Write(MiniMalta::SC_VRESET_D      , 70 ); // CHANGED (uncommented)  for fe-55 analysis!!!

      //IVAN'S STUFF FOR LOWEST EVER THRESHOLD:
      //mm.Write(MiniMalta::SC_IBIAS, 200 );
      //mm.Write(MiniMalta::SC_ITHR , 1 );
      //mm.Write(MiniMalta::SC_ICASN, 40 );

      //IVAN'S STUFF FOR MONITORING SPECTRA:
      //mm.Write(MiniMalta::SC_ITHR , 127 );
      //mm.Write(MiniMalta::SC_ICASN, 0 );
      //mm.Write(MiniMalta::SC_VPULSE_H, 255);
      //mm.Write(MiniMalta::SC_ENABLE_CONFIG, 0x001b); //it was commented 

      if (has15 or has21 or has48) {
         mm.Write(MiniMalta::SC_MASK_COL   , cols);
         mm.Write(MiniMalta::SC_MASK_ROW_1 , 0x00000000);
         mm.Write(MiniMalta::SC_MASK_ROW_2 , 0x00000000);
      } else {
         mm.Write(MiniMalta::SC_MASK_COL   , 0x0000);
         mm.Write(MiniMalta::SC_MASK_ROW_1 , rows);
         mm.Write(MiniMalta::SC_MASK_ROW_2 , rows2);
      }  

      //      if(attempt == 1) mm.Send();
      //      else mm.Recv();

      mm.Send();
      //////////////////////mm.Recv();	
      attempt+=1;

      mm.Dump(false);

      isOk=mm.CheckConsistency();
        //if (not isOk) cout << "FAILED consistency on attempt " << attempt << endl;
        //else          cout << "SUCCEDDED consistency on attempt " << attempt << endl;
    } while (not isOk and attempt<limit); //was 2000
    ///if( isOk == false) break;
    //cout << isOk << " , ATTEMPT: " << attempt << endl; 
    
    uint32_t words[98];   
    uint32_t numwords=98;
    for (uint32_t i=0;i<numwords;i+=2) {
      words[i+0]=0;
      words[i+1]=0;
    }
    
    //mm.Send();
    //mm.Dump();
    mm.SetFastClock(true);
    mm.SetSlowClock(true);
    
    if (cFast.flags() & cFast.GIVEN) {
      cout << "I am enabling fast readout .... watch what you are doing " << endl;
      mm.SetSlowReadOut(false);//for Florian
      //exit(-1);
    } else {
      mm.SetSlowReadOut(true);
    }
    mm.ResetL1Counter();

    mm.SetSlowReadOutFifo(false);
    mm.ResetSlowFifo();
    mm.SetSlowReadOutFifo(true);
    
    uint32_t defaultV=0;
    defaultV=mm.Read(MiniMalta::SCCONTROL);    
    
    int cNdaq_counter = 0;
    for(uint32_t i=0; i<((uint32_t)cNdaq)/maxPulse; i++){
      //Pulse
      mm.Pulse(false,defaultV);
    }
    
    mm.SetSlowReadOutFifo(false);
    
    vector<int> v_hits;
    for (unsigned int p=0; p<selPixX.size(); p++) {
      v_hits.push_back(0);
    }
    
    std::vector<uint32_t> words_1;
    std::vector<uint32_t> words_2;
    //new code
    mm.GetFifoStatus(true);
    bool fifo2Empty= mm.IsFifoIpSlowProgEmpty();
    //cout << "    fifo2Empty: " << fifo2Empty << endl;
    while( fifo2Empty==false and !(mm.IsFifoIpSlowEmpty() or mm.IsFifoIpSlowAlmostEmpty()) ) {
      mm.ReadMaltaWord(words,numwords);   
      for (uint32_t i=0;i<numwords;i+=2) {
        words_1.push_back(words[i+0]);
        words_2.push_back(words[i+1]);
      }
      mm.GetFifoStatus(true);
      fifo2Empty = mm.IsFifoIpSlowProgEmpty();
    } 
    
    //cout << " now reading normal mode after count is: " << words_1.size() << " , " << words_2.size() << endl;
    //old way: full readout straight on
    while(cNdaq_counter<80000){      
        mm.ReadMaltaWord(words,2);   
        uint32_t word1=words[0];
        uint32_t word2=words[1];
        if(word1 == 0){ break; } //continue;}//break; }//continue;}
        if(word2 == 0){ continue;}
        words_1.push_back(word1);
        words_2.push_back(word2);
      }

      if ( words_1.size() != words_2.size() ) {
        std::cout << "Size of the 2 words does not match: " << words_1.size() << " != " << words_2.size() << " ... ABORTING ... " << endl;
        exit(-1);
      }
      
      //this could have been done better .....
      for (unsigned int m=0; m<words_1.size(); m++) {
        md.setWord1( words_1.at(m) );
        md.setWord2( words_2.at(m) );
        md.unpack();
        //if(md.getNhits() == 0) md.dump();
        //md.dump();
        //recognise hits (could be optimised)

        // modification from Chiara to clear up afterpulses
        
        for (unsigned int h=0; h<md.getNhits(); h++) {
          int xVal=md.getHitColumn(h);
          int yVal=md.getHitRow(h);
          for (unsigned int p=0; p<selPixX.size(); p++) {
            //int bcid=md.getBcid(); 
            if (selPixX[p]<8){
	      //if (bcid< 50 and bcid>60) continue;
              //if (bcid> 90 and bcid<110) continue;
              //if (bcid>160 and bcid<180) continue;
              //if (bcid>230 and bcid<250) continue;
              //if (bcid>305 and bcid<325) continue;
              //if (bcid>380 and bcid<400) continue;
		}
             else if (selPixX[p]>=8){
               //if (bcid> 60 and bcid< 80) continue;
               //if (bcid>140 and bcid<160) continue;
               //if (bcid>210 and bcid<230) continue;
               //if (bcid>285 and bcid<305) continue;
               //if (bcid>355 and bcid<375) continue;
		}
            if (xVal==selPixX[p] and yVal==selPixY[p]) {
              (v_hits.at(p))+=1;
            }
          }
        }
        mt.Set(&md);
        mt.Fill();
      
        cNdaq_counter++;
      }
    
      for (unsigned int p=0; p<selPixX.size(); p++) {
        int error=sqrt(v_hits.at(p));
        if (error==0) error=1.0;
        (v_plots.at(p))->SetPoint(count,paramVal,v_hits.at(p));
        (v_plots.at(p))->SetPointError(count,0.0,error);
      }

      cout<<"vlow is "<<paramVal<<"; passed "<<cNdaq_counter<<" after "<<cNdaq<<" pulses"<<endl;
      if (count==0) {
        if (cNdaq_counter==0) break;
      }
    }
    //end of V loop
   
    //plotting & tree filling
    for (unsigned int p=0; p<selPixX.size(); p++) {
      ostringstream nam;
      nam << "Pixel__" << selPixX[p] << "_" << selPixY[p] << "__Scan";
      TCanvas* can=new TCanvas(nam.str().c_str(),nam.str().c_str(),800,600);
      gStyle->SetOptStat(0);
      gStyle->SetOptFit(1111);
      h_base->SetMaximum(cNdaq*1.50);
      h_base->SetMinimum(0.0);
      h_base->Draw("AXIS");
      (v_plots.at(p))->Draw("P");
    
      TF1* fit=new TF1( ("fit_"+nam.str()).c_str(),"[0]/2*(1-TMath::Erf(1/TMath::Sqrt(2)*(x-[1])/[2]))",vLow-1,vHigh);
      fit->SetParName(0,"base");
      fit->SetParName(1,"#mu");
      fit->SetParName(2,"#sigma");
      fit->SetParameter(0,cNdaq);
      fit->SetParLimits(0, cNdaq*0.6,cNdaq*1.4);
      fit->SetParameter(1,(vHigh-vLow)/2);
      fit->SetParLimits(1,vLow+1.5*vStep,vHigh-1.5*vStep);
      fit->SetParameter(2,10);
      fit->SetParLimits(2,0.2,30);
      //result=    TFitResultPtr 	
      (v_plots.at(p))->Fit(fit,"RE0S");
      fit->SetLineStyle(2);
      fit->SetLineColor(2);
      TLine l=TLine(fit->GetParameter(1),0,fit->GetParameter(1),h_base->GetMaximum()*0.95);
      l.SetLineWidth(1);
      l.Draw("SAMEC");
      fit->Draw("SAMEC");

      float th_DAC=fit->GetParameter(1);
      float th_mV =th_DAC*7+579;
      float th_el =(1615-th_mV)*1.43; //1780
      float noise_el = fit->GetParameter(2)*1.43*7;
      cout << endl;
      cout << "Threshold value::: "<<th_DAC<< " VL; "<<th_mV<< " mV; " << th_el << " electrons" << endl;
      cout << "Noise Value:: " << noise_el << " electrons" << endl;
      cout << endl;
      TLatex lx;
      lx.SetNDC();
      lx.DrawLatex(0.6, 0.6, Form("#scale[0.6]{Threshold is %3.0f e}",th_el));
      lx.DrawLatex(0.6, 0.52, Form("#scale[0.6]{Noise is %3.0f e}",noise_el));
      lx.DrawLatex(0.6, 0.46, Form("#scale[0.6]{Pixel is [%2i, %2i]}",selPixX[p], selPixY[p]));

      can->Update();
      can->Print( (outdir+"/"+nam.str()+".pdf").c_str() );
      delete can;
      ///systemcall("evince "+(outdir+"/"+nam.str()+".pdf")+" &");

      //miniTree filling
      tree_pixX = selPixX[p];
      tree_pixY = selPixY[p];
      tree_thres = th_el;
      tree_sigma = noise_el;
      tree_chi2  = fit->GetChisquare()/fit->GetNDF();
      if ((v_plots.at(p))->GetN()==1) tree_chi2=666;
      th_tree->Fill();
    }
  }

  th_tree->Write();
  th_ntuple ->Close();

  //Close the tree                                                                                                                                                                                                               
  mt.Close();  

  cout << "Running summary plot macro..." << endl;  
  //std::string command_p = "python PlotFromTree_miniMalta.py -i "+outdir+"/th_ntuple_IDB_"+cIDB_str.str()+"_ITHR_"+cITHR_str.str()+"_IBIAS_"+cIBIAS_str.str()+".root -f "+outdir;
  std::string command_p = "python scripts_OLD/PlotFromTree_miniMalta.py -i "+outdirtot+" -f "+outdir;
  cout<< command_p.c_str() << endl;
  system(command_p.c_str());    
  if (cNotSave.flags() & cNotSave.GIVEN) {
  cout << "Deleting thrscan.root file ..." << endl;  
  std::string command_p1 = "rm "+outdir+"/thrscan.root";
  system(command_p1.c_str());  }
  cout << "Have a nice day" << endl;
  return 0;

}
