#include "MiniMalta/MiniMaltaData.h"
#include <sstream>
#include <iostream>
#include <bitset>

using namespace std;

MiniMaltaData::MiniMaltaData(){
  m_refbit = 0;
  m_pixel = 0;
  m_group = 0;
  m_dummyGroup = 0;
  m_parity = 0;
  m_dcolumn = 0;
  m_chipbcid = 0;
  m_chipid = 0;
  m_phase = 0;
  m_winid = 0;
  m_l1id = 0;
  m_bcid = 0;
  m_word1 = 0;
  m_word2 = 0;
  m_rows.resize(16);
  m_columns.resize(16);
}

MiniMaltaData::~MiniMaltaData(){}

void MiniMaltaData::setRefbit(uint32_t value){
  m_refbit = value;
}

void MiniMaltaData::setPixel(uint32_t value){
  m_pixel = value;
}

void MiniMaltaData::setGroup(uint32_t value){
  m_group = value;
}

void MiniMaltaData::setDummyGroup(uint32_t value){
  m_dummyGroup = value;
}

void MiniMaltaData::setParity(uint32_t value){
  m_parity = value;
}

void MiniMaltaData::setDcolumn(uint32_t value){
  m_dcolumn = value;
}

void MiniMaltaData::setChipbcid(uint32_t value){
  m_chipbcid = value;
}

void MiniMaltaData::setChipid(uint32_t value){
  m_chipid = value;
}

void MiniMaltaData::setPhase(uint32_t value){
  m_phase = value;
}

void MiniMaltaData::setWinid(uint32_t value){
  m_winid = value;
}

void MiniMaltaData::setL1id(uint32_t value){
  m_l1id = value;
}

void MiniMaltaData::setBcid(uint32_t value){
  m_bcid = value;
}

void MiniMaltaData::setWord1(uint32_t value){
  m_word1 = value;
}

void MiniMaltaData::setWord2(uint32_t value){
  m_word2 = value;
}

uint32_t MiniMaltaData::getRefbit(){
  return m_refbit;
}

uint32_t MiniMaltaData::getPixel(){
  return m_pixel;
}

uint32_t MiniMaltaData::getGroup(){
  return m_group;
}

uint32_t MiniMaltaData::getParity(){
  return m_parity;
}

uint32_t MiniMaltaData::getDcolumn(){
  return m_dcolumn;
}

uint32_t MiniMaltaData::getChipbcid(){
  return m_chipbcid;
}

uint32_t MiniMaltaData::getChipid(){
  return m_chipid;
}

uint32_t MiniMaltaData::getPhase(){
  return m_phase;
}

uint32_t MiniMaltaData::getWinid(){
  return m_winid;
}

uint32_t MiniMaltaData::getDummyGroup(){
  return m_dummyGroup;
}

uint32_t MiniMaltaData::getNhits(){
  return m_nhits;
}

uint32_t MiniMaltaData::getBcid(){
  return m_bcid;
}

uint32_t MiniMaltaData::getL1id(){
  return m_l1id;
}

uint32_t MiniMaltaData::getWord1(){
  return m_word1;
}

uint32_t MiniMaltaData::getWord2(){
  return m_word2;
}

uint32_t MiniMaltaData::getHitRow(uint32_t hit){
  return m_rows[hit];
}

uint32_t MiniMaltaData::getHitColumn(uint32_t hit){
  return m_columns[hit];
}

bool MiniMaltaData::getValidDummyBits(){
    return m_dummiesValid;
}

void MiniMaltaData::pack(){

  // Gray conversions
  if     (m_chipbcid==2) m_chipbcid=3;
  else if(m_chipbcid==3) m_chipbcid=2;
  else if(m_chipbcid==4) m_chipbcid=6;
  else if(m_chipbcid==5) m_chipbcid=7;
  else if(m_chipbcid==6) m_chipbcid=5;
  else if(m_chipbcid==7) m_chipbcid=4;
  
  if     (m_winid==2) m_winid=3;
  else if(m_winid==3) m_winid=2;
  else if(m_winid==4) m_winid=6;
  else if(m_winid==5) m_winid=7;
  else if(m_winid==6) m_winid=5;
  else if(m_winid==6) m_winid=4;
  else if(m_winid==8) m_winid=12;
  else if(m_winid==9) m_winid=13;
  else if(m_winid==10) m_winid=15;
  else if(m_winid==11) m_winid=14;
  else if(m_winid==12) m_winid=10;
  else if(m_winid==13) m_winid=11;
  else if(m_winid==14) m_winid=9;
  else if(m_winid==15) m_winid=8;

  m_word1 = 0;
  m_word2 = 0;

  m_word1 |= (m_bcid       & 0x7FFF) <<  0;
  m_word1 |= (m_chipbcid   & 0x7)    << 15;
  m_word1 |= (m_refbit     & 0x1)    << 18;
  m_word1 |= (m_winid      & 0xF)    << 19;
  m_word1 |= (m_group      & 0x3)    << 23;
  m_word1 |= (m_dummyGroup & 0x7)    << 25;
  m_word1 |= (m_pixel      & 0x7)    << 28;
  m_word2 |= (m_pixel      >>  3) & 0x1FFF;
  m_word2 |= (m_parity     & 0x1)    << 13;//44-31
  m_word2 |= (m_dcolumn    & 0x7)    << 14;//45-31
  m_word2 |= (m_l1id       & 0xFFF)  << 17;//48-31

}


void MiniMaltaData::unPack(){
  unpack();
}

void MiniMaltaData::unpack(){
  
  m_bcid       = (m_word1 >>  0) & 0x7FFF;
  m_chipbcid   = (m_word1 >> 15) & 0x7; 
  m_refbit     = (m_word1 >> 18) & 0x1;
  m_winid      = (m_word1 >> 19) & 0xF;
  m_group      = (m_word1 >> 23) & 0x3;
  m_dummyGroup = (m_word1 >> 25) & 0x7;
  m_pixel      = ((m_word2 & 0x1FFF) << 3) | ((m_word1 >> 28) & 0x7);
  m_parity     = (m_word2 >> 13) & 0x1;
  m_dcolumn    = (m_word2 >> 14) & 0x7;
  m_l1id       = (m_word2 >> 17) & 0xFFF;
  

  // Gray conversions
  if     (m_chipbcid==3) m_chipbcid=2;
  else if(m_chipbcid==2) m_chipbcid=3;
  else if(m_chipbcid==6) m_chipbcid=4;
  else if(m_chipbcid==7) m_chipbcid=5;
  else if(m_chipbcid==5) m_chipbcid=6;
  else if(m_chipbcid==4) m_chipbcid=7;
  
  if     (m_winid==3) m_winid=2;
  else if(m_winid==2) m_winid=3;
  else if(m_winid==6) m_winid=4;
  else if(m_winid==7) m_winid=5;
  else if(m_winid==5) m_winid=6;
  else if(m_winid==4) m_winid=7;
  else if(m_winid==12) m_winid=8;
  else if(m_winid==13) m_winid=9;
  else if(m_winid==15) m_winid=10;
  else if(m_winid==14) m_winid=11;
  else if(m_winid==10) m_winid=12;
  else if(m_winid==11) m_winid=13;
  else if(m_winid==9) m_winid=14;
  else if(m_winid==8) m_winid=15;
  
  //moved from above 
  //hand made phase adjustment
  //if (m_winid>7) m_winid=m_winid-8;

  //dummy group
  m_dummiesValid = (m_dummyGroup == 7 && m_refbit == 1) ? true : false; 

  m_nhits = 0;
  for(uint32_t i=0; i<16;i++){
    if(((m_pixel>>i)&0x1)==0){continue;}
    
    int column = m_dcolumn*2;
    if(i>7){column+=1;}
    
    //cout << " .............. my group is: " << m_group << endl;
    int row = m_group*16;
    if(m_parity==0) { row+=8; }
    if     (i==0 || i== 8){row+=0;}
    else if(i==1 || i== 9){row+=1;}
    else if(i==2 || i==10){row+=2;}
    else if(i==3 || i==11){row+=3;}
    else if(i==4 || i==12){row+=4;}
    else if(i==5 || i==13){row+=5;}
    else if(i==6 || i==14){row+=6;}
    else if(i==7 || i==15){row+=7;}
    m_rows[m_nhits]=row;
    m_columns[m_nhits]=column;
    m_nhits++;
  }    
}

string MiniMaltaData::toString(){
  ostringstream os;
  os << "|" << bitset<12>(m_l1id)
     << "|" << bitset< 3>(m_dcolumn)
     << "|" << bitset< 1>(m_parity)
     << "|" << bitset<16>(m_pixel)
     << "|" << bitset< 3>(m_dummyGroup)
     << "|" << bitset< 2>(m_group)
     << "|" << bitset< 4>(m_winid)
     << "|" << bitset< 1>(m_refbit)
     << "|" << bitset< 3>(m_chipbcid)
     << "|" << bitset<15>(m_bcid)
     << "|";
  return os.str();
}

string MiniMaltaData::getInfo(){
  //cout << "Raw bits from word1 and word2 (in this order!): " << endl;
  ostringstream os;
  //cout << endl << "VALID: #### " << ((m_dummiesValid)?"T R U E":"F A L S E") << " ####" << endl << endl;
  //cout << MakeBinary(m_word1) << "  " << MakeBinary(m_word2) << endl;
  os    << "DCol: "         << std::bitset<3>(getDcolumn())
        << ", Parity: "     << std::bitset<1>(getParity())
        << ", Pixel: "      << std::bitset<16>(getPixel())
        << ", DummyGroup: " << std::bitset<3>(getDummyGroup())
        << ", Group: "      << std::bitset<2>(getGroup())
        << ", WinID: "      << std::bitset<4>(getWinid())
        << ", RefBit: "     << std::bitset<1>(getRefbit())
        << ", ChipBCID: "   << std::bitset<3>(getChipbcid())
        << ", BCID: "       << std::bitset<15>(getBcid())
        << "  L1ID: "       << getL1id();
    // << "  BCID: " << getBcid();
  return os.str();
}

std::string MiniMaltaData::MakeBinary(uint32_t val){
  std::string binString;
  std::string padString;
  //get binary expression of val
  while(val != 0){
    binString += std::to_string(val%2);
    val /= 2;
  }
  //do 32 bit padding
  for(unsigned int i = 0; i < 32-binString.length(); ++i) padString += "0";
  return "0b"+padString+binString;
}

void MiniMaltaData::dump(){
  cout << getInfo() << endl;
}
