/************************************
 * MiniMalta
 * Brief: Python module for MiniMalta
 * 
 * Author: Carlos.Solans@cern.ch
 *         Ignacio.Asensi@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "MiniMalta/MiniMalta.h"
#include <iostream>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif

#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
  static struct PyModuleDef moduledef = {		\
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	\
  };							\
  m = PyModule_Create(&moduledef);			\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000

/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MiniMalta *obj;
  std::vector<uint32_t> vec;
} PyMiniMalta;

static int _PyMiniMalta_init(PyMiniMalta *self)
{

    self->obj = new MiniMalta();
    return 0;
}

static void _PyMiniMalta_dealloc(PyMiniMalta *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject * _PyMiniMalta_Connect(PyMiniMalta *self, PyObject *args)
{
    char * str;
    if(PyArg_ParseTuple(args, (char *) "s",&str)){
      self->obj->Connect(std::string(str));
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_Write(PyMiniMalta *self, PyObject *args)
{
    uint32_t pos;
    uint32_t val;
    if (!PyArg_ParseTuple(args, (char *) "II",&pos, &val)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    self->obj->Write(pos, val);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_WriteAll(PyMiniMalta *self)
{
      self->obj->WriteAll();
      Py_INCREF(Py_None);
      return Py_None;
}

PyObject * _PyMiniMalta_Read(PyMiniMalta *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    uint32_t ret = self->obj->Read(pos);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}



PyObject * _PyMiniMalta_SetDefaults(PyMiniMalta *self)
{
    self->obj->SetDefaults();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_GetDefault(PyMiniMalta *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    uint32_t ret = self->obj->GetDefault(pos);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMiniMalta_Send(PyMiniMalta *self)
{   
    self->obj->Send();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_Recv(PyMiniMalta *self)
{
    self->obj->Recv();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_ReadAll(PyMiniMalta *self)
{
    self->obj->ReadAll();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_TestMethod(PyMiniMalta *self)
{
    PyObject *py_ret;
    int ret = self->obj->TestMethod();
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}


PyObject * _PyMiniMalta_Reset(PyMiniMalta *self)
{
    self->obj->Reset();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_IsOverriden(PyMiniMalta *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
     PyObject * return_flag = Py_True;
     if(!self->obj->IsOverriden(pos)){ return_flag = Py_False; }
     Py_INCREF(return_flag);
     return return_flag;
    //self->obj->IsOverriden(pos);
    //Py_INCREF(Py_None);
    //return Py_None;
}

PyObject * _PyMiniMalta_Override(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_flag = NULL;
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "IO",&pos,&py_flag)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool flag = py_flag? (bool) PyObject_IsTrue(py_flag) : false;
  if(py_flag!=NULL){
    self->obj->Override(pos, flag);
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta_SetIPbus(PyMiniMalta *self, PyObject *args)
{
    /*
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    self->obj->SetIPbus(pos);
    */
  Py_INCREF(Py_None);
  return Py_None;

}

PyObject * _PyMiniMalta_GetIPbus(PyMiniMalta *self)
{
  //ipbus::Uhal* ipb = self->obj->GetIPbus();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta_SetVerbose(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetVerbose(enable);
  Py_INCREF(Py_None);
  return Py_None;
} 


PyObject * _PyMiniMalta_IsMonitored(PyMiniMalta *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
     PyObject * return_flag = Py_True;
     if(!self->obj->IsMonitored(pos)){ return_flag = Py_False; }
     Py_INCREF(return_flag);
     return return_flag;
    //self->obj->IsMonitored(pos);
    //Py_INCREF(Py_None);
    //return Py_None;
}

PyObject * _PyMiniMalta_Monitor(PyMiniMalta *self, PyObject *args)
{
    PyObject *py_flag = NULL;
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "IO",&pos,&py_flag)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool flag = py_flag? (bool) PyObject_IsTrue(py_flag) : false;
    if(py_flag!=NULL){
      self->obj->Monitor(pos, flag);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_Dump(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_update = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_update)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool update = py_update? (bool) PyObject_IsTrue(py_update) : false;
  self->obj->Dump(update);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta_Pulse(PyMiniMalta *self, PyObject *args)
{
    PyObject *py_flag = NULL;
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "OI",&py_flag,&pos)){
	Py_INCREF(Py_None);
    return Py_None;
    }
    bool flag = py_flag? (bool) PyObject_IsTrue(py_flag) : false;
    if(py_flag!=NULL){
	self->obj->Pulse(flag, pos);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_ResetSlowFifo(PyMiniMalta *self)
{
  self->obj->ResetSlowFifo();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta_SetSlowReadOut(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetSlowReadOut(enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta_SetSlowReadOutFifo(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetSlowReadOutFifo(enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta_GetFifoStatus(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->GetFifoStatus(enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMalta_IsFifoIpSlowEmpty(PyMiniMalta *self)
{ 
  PyObject * isfifo = Py_True;
  if(!self->obj->IsFifoIpSlowEmpty()){ isfifo = Py_False; }
  Py_INCREF(isfifo);
  return isfifo;
}

PyObject * _PyMiniMalta_IsFifoIpSlowFull(PyMiniMalta *self)
{
  PyObject * isfifo = Py_True;
  if(!self->obj->IsFifoIpSlowFull()){ isfifo = Py_False; }
  Py_INCREF(isfifo);
  return isfifo;
}

PyObject * _PyMiniMalta_SetSCClock(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetSCClock(enable);
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMiniMalta_SetFastClock(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetFastClock(enable);
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMiniMalta_SetSlowClock(PyMiniMalta *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetSlowClock(enable);
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMiniMalta_decodeHotEncoded(PyMiniMalta *self, PyObject *args)
{
    uint32_t val;
    if (!PyArg_ParseTuple(args, (char *) "I",&val)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    uint32_t ret = self->obj->decodeHotEncoded(val);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMiniMalta_getValue(PyMiniMalta *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    uint32_t ret = self->obj->getValue(pos);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMiniMalta_MaskAll(PyMiniMalta *self, PyObject *args)
{
    PyObject *py_enable = NULL;
    if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
    self->obj->MaskAll(enable);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMiniMalta_GetRegisterName(PyMiniMalta *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    std::string ret = self->obj->GetRegisterName(pos);
    py_ret = PyUnicode_FromString(ret.c_str());
    return py_ret;
}

////////////////////////////////////////////////////////////////


static PyMethodDef PyMiniMalta_methods[] = {
  {(char *) "Connect",(PyCFunction) _PyMiniMalta_Connect, METH_VARARGS, NULL },
  {(char *) "Write",(PyCFunction) _PyMiniMalta_Write, METH_VARARGS, NULL },
  {(char *) "WriteAll",(PyCFunction) _PyMiniMalta_WriteAll, METH_NOARGS, NULL },
  {(char *) "Read",(PyCFunction) _PyMiniMalta_Read, METH_VARARGS, NULL },
  {(char *) "Read",(PyCFunction) _PyMiniMalta_Read, METH_NOARGS, NULL },
  {(char *) "ReadAll",(PyCFunction) _PyMiniMalta_ReadAll, METH_NOARGS, NULL },
  {(char *) "SetIPbus",(PyCFunction) _PyMiniMalta_SetIPbus, METH_VARARGS, NULL },
  {(char *) "GetIPbus",(PyCFunction) _PyMiniMalta_GetIPbus, METH_NOARGS, NULL },
  {(char *) "SetVerbose",(PyCFunction) _PyMiniMalta_SetVerbose, METH_VARARGS, NULL },
  {(char *) "SetDefaults",(PyCFunction) _PyMiniMalta_SetDefaults, METH_NOARGS, NULL },
  {(char *) "GetDefault",(PyCFunction) _PyMiniMalta_GetDefault, METH_VARARGS, NULL },
  {(char *) "Send",(PyCFunction) _PyMiniMalta_Send, METH_NOARGS, NULL },
  {(char *) "Recv",(PyCFunction) _PyMiniMalta_Recv, METH_NOARGS, NULL },
  {(char *) "Pulse",(PyCFunction) _PyMiniMalta_Pulse, METH_VARARGS, NULL },
  {(char *) "ResetSlowFifo",(PyCFunction) _PyMiniMalta_ResetSlowFifo, METH_NOARGS, NULL }, 
  {(char *) "SetSlowReadOut",(PyCFunction)   _PyMiniMalta_SetSlowReadOut, METH_VARARGS, NULL },
  {(char *) "SetSlowReadOutFifo",(PyCFunction)   _PyMiniMalta_SetSlowReadOutFifo, METH_VARARGS, NULL },
  {(char *) "IsFifoIpSlowEmpty",(PyCFunction)   _PyMiniMalta_IsFifoIpSlowEmpty, METH_NOARGS, NULL },
  {(char *) "IsFifoIpSlowFull",(PyCFunction)   _PyMiniMalta_IsFifoIpSlowFull, METH_NOARGS, NULL },
  {(char *) "GetFifoStatus",(PyCFunction)   _PyMiniMalta_GetFifoStatus, METH_VARARGS, NULL },
  {(char *) "SetSCClock",(PyCFunction)   _PyMiniMalta_SetSCClock, METH_VARARGS, NULL },
  {(char *) "SetFastClock",(PyCFunction) _PyMiniMalta_SetFastClock, METH_VARARGS, NULL },
  {(char *) "SetSlowClock",(PyCFunction) _PyMiniMalta_SetSlowClock, METH_VARARGS, NULL },
  {(char *) "TestMethod",(PyCFunction) _PyMiniMalta_TestMethod, METH_VARARGS, NULL },
  {(char *) "IsOverriden",(PyCFunction) _PyMiniMalta_IsOverriden, METH_VARARGS, NULL },
  {(char *) "Override",(PyCFunction) _PyMiniMalta_Override, METH_VARARGS, NULL },
  {(char *) "Monitor",(PyCFunction) _PyMiniMalta_Monitor, METH_VARARGS, NULL },
  {(char *) "Reset",(PyCFunction) _PyMiniMalta_Reset, METH_NOARGS, NULL },
  {(char *) "Dump",(PyCFunction) _PyMiniMalta_Dump, METH_VARARGS, NULL },
  {(char *) "IsMonitored",(PyCFunction) _PyMiniMalta_IsMonitored, METH_VARARGS, NULL },
  {(char *) "decodeHotEncoded",(PyCFunction) _PyMiniMalta_decodeHotEncoded, METH_VARARGS, NULL },
  {(char *) "getValue",(PyCFunction) _PyMiniMalta_getValue, METH_VARARGS, NULL },
  {(char *) "MaskAll",(PyCFunction) _PyMiniMalta_MaskAll, METH_VARARGS, NULL },
  {(char *) "GetRegisterName",(PyCFunction) _PyMiniMalta_GetRegisterName, METH_VARARGS, NULL },
  {NULL, NULL, 0, NULL}
};

PyTypeObject PyMiniMalta_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMiniMalta.MiniMalta",            /* tp_name */
    sizeof(PyMiniMalta),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    /* methods */
    (destructor) _PyMiniMalta_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    NULL,                       /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMiniMalta_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMiniMalta_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
	,0                                   /* tp_version_tag */
#endif
};

static PyMethodDef PyModule_methods[] = {
  {NULL, NULL, 0, NULL}
};


MOD_INIT(PyMiniMalta)
{
    PyObject *m;
    MOD_DEF(m, "PyMiniMalta", NULL, PyModule_methods);
    if(PyType_Ready(&PyMiniMalta_Type)<0){
      return MOD_ERROR;
    }
    PyModule_AddObject(m, (char *) "MiniMalta", (PyObject *) &PyMiniMalta_Type);
    return MOD_RETURN(m);

}
