//! -*-C++-*-
#include "MiniMalta/MiniMalta.h"
#include "MiniMalta/MiniMaltaData.h"
#include "MiniMalta/MiniMaltaTree.h"
#include "MiniMalta/MiniMaltaMaskLoop.h"
#include "MiniMalta/MiniMaltaNoiseHandler.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <bitset>

#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"
#include "TLegend.h"

using namespace std;

float DAQwindow=50e-6; //hardcoded

bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

stringstream systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}

int main(int argc, char *argv[]) {

  cout << "#####################################" << endl
       << "# Welcome to MiniMALTA Noise Scan   #" << endl
       << "#####################################" << endl;
  
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cAddress('a',"address","address","ipbus address");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  CmdArgStr  cPath('f',"basefolder","basefolder","output basePath directory",CmdArg::isREQ);
  CmdArgIntList cPixels('p',"pixels","pairs","pixels to select [x y]");
  CmdArgBool cMask('m',"mask","turn on masking of the noisy pixels");
  CmdArgInt cNdaq('t',"time","time","time for the single repetition",CmdArg::isREQ);
  CmdArgInt cVlow('l',"idblow" ,"idblow", "idblow value (5-255)",CmdArg::isREQ);
  CmdArgInt cVhigh('h',"idbhigh" ,"idbhigh", "idbhigh value (5-255)",CmdArg::isREQ);
  CmdArgInt cStep('s',"idbstep" ,"idbstep", "step of idb scan (1-255)",CmdArg::isREQ);
  CmdArgInt cRep('r',"repetition" ,"repetition", "nimber of repetitions",CmdArg::isREQ);
  CmdArgInt cBval('b',"ibias" ,"ibias", "ibias value");
  CmdArgInt cChip('c',"chip" ,"chip", "chip to talk to");
  CmdArgStr cNoiseMapPath('n',"noise_map","noise_map","path to the noise map text file (should contain [x,y] coordinates)");
  CmdArgStr  cReg('x',"register","register","sweep option");

  CmdArgStr  cMaskAll('z',"maskAll","maskAll","mask the entire chip");


  CmdLine cmdl(*argv,&cVerbose,&cMask,&cOutdir,&cAddress,&cPixels,&cVlow,&cVhigh,&cStep,&cNdaq,&cRep,&cBval,&cChip,&cPath,&cNoiseMapPath,&cReg,&cMaskAll,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  int chip=1;
  if (cChip.flags() & CmdArg::GIVEN) {
    chip=cChip;
  }
  ostringstream cString;
  cString << chip;

  int maskAll=0;
  if (cMaskAll.flags() & CmdArg::GIVEN) {
    maskAll = 1;
  }
	

  string address;
  if(cAddress.flags() & CmdArg::GIVEN){
    address=cAddress;
  }else{
    address=systemcall("python -c 'import MaltaSetup; print MaltaSetup.MaltaSetup().getConnStr("+cString.str()+")' | tail -n1").str();
    address.erase(address.size()-1); // to remove newline character
  }

  string thrRegister;
  if(cReg.flags() & CmdArg::GIVEN){
    thrRegister=cReg;
  }else{
    thrRegister="IDB";
  }

  if(thrRegister.compare("IDB") != 0 && thrRegister.compare("ITHR") != 0 ){
	cout<<"Wrong name of the register!"<<endl;
	return 1;

}

  std::cout << address <<std::endl;
  string ChipFolder;
  ChipFolder=cPath;
  string baseDir="NoiseScansMiniMalta/"+ChipFolder+"/";
  string outdir = baseDir+(cOutdir.flags() & CmdArg::GIVEN?cOutdir:"Base");

  /*  
  // switch to single pixel occupancy
  bool onePix_occupancy = false;
  if(cPixels.flags() & CmdArg::GIVEN){
    onePix_occupancy=true;
    cout<<"p arg is given, probing only ONE pixel"<<endl;
  }
  */

  if (outdir!="") systemcall("mkdir -p "+outdir);

  vector<int> selPixX,selPixY;  
  for(uint32_t i=0; i<cPixels.count(); i++){
    std::cout << " val: " << cPixels[i] << std::endl;
    if(i%2==0){selPixX.push_back(cPixels[i]);}
    else      {selPixY.push_back(cPixels[i]);}
  }

  //signal(SIGINT,handler);

  //Connect to MALTA
  std::cout << "before creating malta" <<std::endl;
  MiniMalta mm;
  mm.Connect(address);
    
  mm.SetFastClock(false);
  mm.SetSlowClock(false);
  mm.Reset();
  mm.SetSlowReadOut(true);
  mm.ResetL1Counter();
  mm.SetExternalL1A(true);

  /*
  for (unsigned int t=0; t<10000; t++) {
    usleep(10000);
    //mm.Pulse(true,0);
    std::cout << "L1A counter word is: " << mm.GetL1Acounter() << std::endl;
  }
  
std::cout << "S L1A counter word is: " << mm.GetL1Acounter() << std::endl;
  mm.Trigger(true,0);
  std::cout << "S L1A counter word is: " << mm.GetL1Acounter() << std::endl;
  mm.Trigger(true,0);
  mm.SetExternalL1A(true);
  std::cout << "T L1A counter word is: " << mm.GetL1Acounter() << std::endl;
  mm.Trigger(true,0);
  std::cout << "R L1A counter word is: " << mm.GetL1Acounter() << std::endl;
  mm.ResetL1Counter();
  std::cout << "R L1A counter word is: " << mm.GetL1Acounter() << std::endl;
 

  cout << endl;
  return 1;
  */

/*
  uint32_t cols=0;
  uint32_t rows=0;
  uint32_t rows2=0;
  for(uint32_t i=0; i<selPixX.size(); i++){
      cols |= 1<<selPixX[i];
  }
  for(uint32_t i=0; i<selPixY.size(); i++){
      rows |= 1<<selPixY[i];
  }
  cout<<"cols: " <<cols<<endl;
  cout<<"rows: " <<rows<<endl;
 */

  cout << "#####################################" << endl
       << "# Start Repetition loop              #" << endl
       << "#####################################" << endl;
  MiniMaltaData md;
 
  //Create ntuple
  cout << "Create ntuple: ";
  ostringstream os;
  os << outdir  << "/noise_scan.root";
  string fname = os.str();
  cout << fname << endl;
  MiniMaltaTree mt;
  mt.Open(fname,"RECREATE");
  int vLow = cVlow;
  if(vLow < 5 || vLow > 255){
    cout<<"you gave me vlow = "<<vLow<<", please give valid cLow number!!!"<<endl;
    return 1;
  }

  int vHigh = cVhigh;
  if(vHigh < 0 || vHigh > 255 || vLow >= vHigh){
    cout<<"you gave me vHigh = "<<vHigh<<", please give valid cHigh number!!!"<<endl;
    return 1;
  }

  int vStep = cStep;
  if(vStep == 0 ) { //> (vHigh-vLow)){
    cout<<"you gave me vStep = "<<vStep<<", please give valid cStep number!!!"<<endl;
    return 1;
  } else {
    cout<<"vStep = "<<vStep<<endl;
  } 
  
  if(cBval.flags() & CmdArg::GIVEN){
    if(cBval < 0 || cBval  > 255 ){
      cout<<"you gave me IBIAS = "<<cBval <<", please give valid ibias number!!!"<<endl;
      return 1;
    }
  }
 
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //instantiating stuff
  TFile *noise_sumf = new TFile( (outdir+"/noise_summary.root").c_str(), "RECREATE");

  TH2F* h2_base=new TH2F("Occupancy","Occupancy;x;y;counts",16,-0.5,15.5,64,-0.5,63.5);
  h2_base->SetDirectory(0);
  TH1F* h_base=new TH1F("base","base;IDB (DAC);Noise Rate [KHz]",100,vLow-vStep,vHigh+vStep);
  h_base->SetDirectory(0);
  vector<TGraphErrors*> v_plots;

  TH1F* h_time=new TH1F("time","Time of single repetition [mus];;Time",1,0.,1.);
  h_time->Fill(0., (double)cNdaq);
  TH1F* h_rep=new TH1F("nrep","Number of repetitions;;Repetitions",1,0.,1.);
  h_rep->Fill(0., (double)cRep);
  
  TGraphErrors* nhits_vs_idb =new TGraphErrors();
  nhits_vs_idb->SetName("hits_vs_idb"); nhits_vs_idb->SetTitle("Total number of hits vs IDB;IDB;Total number of hits;");
  nhits_vs_idb->SetMarkerStyle(20); nhits_vs_idb->SetMarkerSize(0.9); nhits_vs_idb->SetLineWidth(2);
  TGraphErrors* npix_vs_idb =new TGraphErrors();
  npix_vs_idb->SetName("npix_vs_idb"); npix_vs_idb->SetTitle("Number of pixels with >0 entries vs IDB;IDB;Number of pixels with >0 entries;");
  npix_vs_idb->SetMarkerStyle(20); npix_vs_idb->SetMarkerSize(0.9); npix_vs_idb->SetLineWidth(2);
    TGraphErrors* rate_vs_idb =new TGraphErrors();
  rate_vs_idb->SetName("rate_vs_idb"); rate_vs_idb->SetTitle("rate vs IDB;IDB;Rate [kHz];");
  rate_vs_idb->SetMarkerStyle(20); rate_vs_idb->SetMarkerSize(0.9); rate_vs_idb->SetLineWidth(2);
  
  TGraphErrors* nhits_vs_idb_l =new TGraphErrors();
  nhits_vs_idb_l->SetName("hits_vs_idb_l"); nhits_vs_idb_l->SetTitle("Total number of hits vs IDB (pixels 0-7);IDB;Total number of hits;");
  nhits_vs_idb_l->SetMarkerStyle(22); nhits_vs_idb_l->SetMarkerSize(0.9); nhits_vs_idb_l->SetLineWidth(2); nhits_vs_idb_l->SetMarkerColor(kRed);
  TGraphErrors* npix_vs_idb_l =new TGraphErrors();
  npix_vs_idb_l->SetName("npix_vs_idb_l"); npix_vs_idb_l->SetTitle("Number of pixels with >0 entries vs IDB;IDB (pixels 0-7);Number of pixels with >0 entries;");
  npix_vs_idb_l->SetMarkerStyle(22); npix_vs_idb_l->SetMarkerSize(0.9); npix_vs_idb_l->SetLineWidth(2); npix_vs_idb_l->SetMarkerColor(kRed);
    TGraphErrors* rate_vs_idb_l =new TGraphErrors();
  rate_vs_idb_l->SetName("rate_vs_idb_l"); rate_vs_idb_l->SetTitle("rate vs IDB (pixels 0-7);IDB;Rate [kHz];");
  rate_vs_idb_l->SetMarkerStyle(22); rate_vs_idb_l->SetMarkerSize(0.9); rate_vs_idb_l->SetLineWidth(2); rate_vs_idb_l->SetMarkerColor(kRed);
  
  TGraphErrors* nhits_vs_idb_r =new TGraphErrors();
  nhits_vs_idb_r->SetName("hits_vs_idb_r"); nhits_vs_idb_r->SetTitle("Total number of hits vs IDB (pixels 8-15);IDB;Total number of hits;");
  nhits_vs_idb_r->SetMarkerStyle(33); nhits_vs_idb_r->SetMarkerSize(0.9); nhits_vs_idb_r->SetLineWidth(2); nhits_vs_idb_r->SetMarkerColor(kBlue);
  TGraphErrors* npix_vs_idb_r =new TGraphErrors();
  npix_vs_idb_r->SetName("npix_vs_idb_r"); npix_vs_idb_r->SetTitle("Number of pixels with >0 entries vs IDB;IDB (pixels 8-15);Number of pixels with >0 entries;");
  npix_vs_idb_r->SetMarkerStyle(33); npix_vs_idb_r->SetMarkerSize(0.9); npix_vs_idb_r->SetLineWidth(2); npix_vs_idb_r->SetMarkerColor(kBlue);
    TGraphErrors* rate_vs_idb_r =new TGraphErrors();
  rate_vs_idb_r->SetName("rate_vs_idb_r"); rate_vs_idb_r->SetTitle("rate vs IDB (pixels 8-15);IDB;Rate [kHz];");
  rate_vs_idb_r->SetMarkerStyle(33); rate_vs_idb_r->SetMarkerSize(0.9); rate_vs_idb_r->SetLineWidth(2); rate_vs_idb_r->SetMarkerColor(kBlue);

  ///////////////
  TGraphErrors* npix_vs_idb_40Hz =new TGraphErrors();
  npix_vs_idb_40Hz->SetName("npix_vs_idb_40Hz"); npix_vs_idb_40Hz->SetTitle("Number of pixels with rate above 40Hz;IDB;Number of pixels with rate above 40Hz;");
  npix_vs_idb_40Hz->SetMarkerStyle(20); npix_vs_idb_40Hz->SetMarkerSize(0.9); npix_vs_idb_40Hz->SetLineWidth(2);
  TGraphErrors* npix_vs_idb_40Hz_l =new TGraphErrors();
  npix_vs_idb_40Hz_l->SetName("npix_vs_idb_40Hz_l"); npix_vs_idb_40Hz_l->SetTitle("Number of pixels with rate above 40Hz;IDB (pixels 0-7);Number of pixels with rate above 40Hz;");
  npix_vs_idb_40Hz_l->SetMarkerStyle(22); npix_vs_idb_40Hz_l->SetMarkerSize(0.9); npix_vs_idb_40Hz_l->SetLineWidth(2); npix_vs_idb_40Hz_l->SetMarkerColor(kRed);
  TGraphErrors* npix_vs_idb_40Hz_r =new TGraphErrors();
  npix_vs_idb_40Hz_r->SetName("npix_vs_idb_40Hz_r"); npix_vs_idb_40Hz_r->SetTitle("Number of pixels with rate above 40Hz;IDB (pixels 8-15);Number of pixels with rate above 40Hz;");
  npix_vs_idb_40Hz_r->SetMarkerStyle(33); npix_vs_idb_40Hz_r->SetMarkerSize(0.9); npix_vs_idb_40Hz_r->SetLineWidth(2); npix_vs_idb_40Hz_r->SetMarkerColor(kBlue);

  
  TLine l1( 7.5, 0, 7.5, 64);
  TLine l2(15.5, 0,15.5, 64);
  TLine l3(32, 0,32, 64);
  TLine l4( -0.5,15.5, 15.5,  15.5);
  TLine l5( -0.5,31.5, 15.5,  31.5);
  TLine l6( -0.5,47.5, 15.5,  47.5);

  

/*  TTree *th_tree = new TTree("Noise","DataStream");
  int tree_pixX, tree_pixY;
  float tree_idb, tree_sigma, tree_chi2;
  th_tree->Branch("pixX",   &tree_pixX,   "pixX/i");
  th_tree->Branch("pixY",   &tree_pixY,   "pixY/i");
  th_tree->Branch("idb",  &tree_idb,  "idb/i");
  th_tree->Branch("sigma",  &tree_sigma,  "sigma/f");
  th_tree->Branch("chi2",   &tree_chi2,   "chi2/f");
 */ 

  uint32_t words[2];   
  uint32_t numwords=2;
  
  int count=-1;
  int paramVal=0;
  double nNoHitWords = 0.0;
  double nWords = 0.0;
  // main loop over IDB values
  for (paramVal=vHigh; paramVal>=vLow; paramVal-=vStep){
    
    mm.Reset();   
    count++; 
    ostringstream thresh;
    if(!thrRegister.compare("IDB")) thresh << "IDB_" << paramVal;
    else if(!thrRegister.compare("ITHR")) thresh << "ITHR" << paramVal;
    TH2F* the_h2= (TH2F*)h2_base->Clone(thresh.str().c_str());
    the_h2->SetDirectory(0);
    the_h2->Reset();
    
    mm.SetFastClock(false);
    mm.SetSlowClock(false);

    int limit=2;
    if (chip==2) limit=2;
    bool isOk=false;
    int attempt=0;
    do { 
      //load confg
      mm.SetDefaults();
      //mm.Write(MiniMalta::SC_DB        , cBval);
     if(!thrRegister.compare("IDB")) {
      mm.Write(MiniMalta::SC_IDB        , paramVal+256);
      mm.Write(MiniMalta::SC_ITHR      , 20);  
      }
     else if(!thrRegister.compare("ITHR")) {
      mm.Write(MiniMalta::SC_IDB        , 20+256);
      mm.Write(MiniMalta::SC_ITHR      , paramVal);  
      }

      mm.Write(MiniMalta::SC_ICASN       , 10);

	mm.Write(MiniMalta::SC_VCASN      , 170 ); // trick for hi-doped new miniMalta to go up with PWELL

     mm.Write(MiniMalta::SC_VRESET_D      , 133 );

      //mm.Write(MiniMalta::SC_IRESET     , 15); //IVAN's TRICK
      //mm.Write(MiniMalta::SC_VRESET_P   , 90); //IVAN's TRICK
      //playing with setup
      //mm.Write(MiniMalta::SC_ICASN       , 0x001); //IVAN's TRICK for super-high threshold
      //////////////////////////////////////////////////////////mm.Write(MiniMalta::SC_VRESET_D    , 0x07F); 
      //mm.Write(MiniMalta::SC_ITHR        , 0x0FE); //IVAN's TRICK for super-high threshold
      //if(cBval.flags() & CmdArg::GIVEN){ mm.Write(MiniMalta::SC_IBIAS       , paramVal);} // we see the transition from 0 hits to some hits with Fe source (0x01F - no hits , 0x05F - some hits)
      if(cBval.flags() & CmdArg::GIVEN){ mm.Write(MiniMalta::SC_IBIAS       , cBval);} // we see the transition from 0 hits to some hits with Fe source (0x01F - no hits , 0x05F - some hits)
      
      ///////////////mm.Write(MiniMalta::SC_VRESET_D  , 0x085); 
      ///////////////mm.Write(MiniMalta::SC_VRESET_P  , 0x1ff); 
      //mm.Write(MiniMalta::SC_MASK_DIAG  , 0x0000);
      //mm.Write(MiniMalta::SC_MASK_COL   , 0x0000);
      //mm.Write(MiniMalta::SC_MASK_ROW_1 , 0x00000000);
      //mm.Write(MiniMalta::SC_MASK_ROW_2 , 0x0000ffff);
      //mm.Write(MiniMalta::SC_MASK_ROW_2 , 0xffff0000);
      
      //IVAN'S STUFF FOR HIGH THRESHOLD:
      //mm.Write(MiniMalta::SC_ITHR , 127 );

      //PS02 had these 2 uncommented
      //mm.Write(MiniMalta::SC_ITHR        , 1); /
      //mm.Write(MiniMalta::SC_IBIAS       , 150);

      // add masking switch
      if(cMask){
        
        cout << "Masking is ON; ";
        
        if(ChipFolder.find("W1R6") != std::string::npos){
          cout << "Masking W1R6 chip" << endl;  
        }
        else if(ChipFolder.find("W2R6") != std::string::npos){
          //W2R6 (removing  [14,61] , [10,27], [5,48] )
          cout << "Masking W2R6 chip" << endl;           
        }
        else if(ChipFolder.find("W4R6") != std::string::npos){
          //W4R6 (removing  [8,16] , [4,16], [0,19], [10,9], [11,9], [9,52]  [8,4]           
          cout << "Masking W4R6 chip" << endl;           
        }
        
        else if(ChipFolder.find("W5R6") != std::string::npos){
          //W5R6 (removing  [15,49] , second noisiest pixel (not removed is [2,11] )
          cout << "Masking W5R6 chip" << endl;            
        }
        else if(ChipFolder.find("W1R10") != std::string::npos){
          //W1R10 (removing  [13,30] ,[9,26], [13,11] adding afterburn [14,61] , [9,29], [4,31], [11,16] 
          cout << "Masking W1R10 chip" << endl;           
        }
        
        else if(ChipFolder.find("W4R9") != std::string::npos){
          //W4R6 (removing  [9,18], [11,49], [6,31], [0,56], [9,5], [8,21] )
          cout << "Masking W4R9 chip" << endl;           
        }
        else if(ChipFolder.find("W5R9") != std::string::npos){
          cout << "Masking W5R9 chip" << endl;             
        }
        else if(ChipFolder.find("W1R9") != std::string::npos){
          cout << "Masking W5R9 chip" << endl;            
        }
        else if(ChipFolder.find("W5R1") != std::string::npos){
          cout << "Masking W5R1 chip" << endl;           
        }
        else if(ChipFolder.find("W1R1") != std::string::npos){
          cout << "Masking W1R1 chip" << endl;     
        }
        else if(ChipFolder.find("W2R2") != std::string::npos){
          cout << "Masking W2R2 chip" << endl;     
        }
        
        // new handling of noisy pixels masking (via txt files)
	MiniMaltaNoiseHandler nh_WXRX("WXRX", true); // true for irradiared chips (but does not really matter...)
        //Might want to look for the files here: getenv("ITK_INST_PATH")+"/share/data/MiniMaltaNoiseMaps/"
        if (cNoiseMapPath.flags() & CmdArg::GIVEN){
          string noise_str;
          noise_str  = cNoiseMapPath;
          nh_WXRX.ProcessTextFile(noise_str);
        }
      
        mm.Write(MiniMalta::SC_MASK_COL   , nh_WXRX.GetMaskColWord()  );
        mm.Write(MiniMalta::SC_MASK_ROW_1 , nh_WXRX.GetMaskRowWord1() );
        mm.Write(MiniMalta::SC_MASK_ROW_2 , nh_WXRX.GetMaskRowWord2() );
        mm.Write(MiniMalta::SC_MASK_DIAG  , nh_WXRX.GetMaskDiagWord() );
	if (maskAll) mm.Write(MiniMalta::SC_MASK_DIAG  , 0xFFFFFFFF);
      }
      
      mm.Send();
      attempt+=1;
      isOk=mm.CheckConsistency();
      //if (not isOk) cout << "FAILED consistency on attempt " << attempt << endl;
      //else          cout << "SUCCEDDED consistency on attempt " << attempt << endl;
    } while (not isOk and attempt<limit);
    cout << isOk << " , ATTEMPT: " << attempt << endl; 
    
    for (uint32_t i=0;i<numwords;i+=2) {
      words[i+0]=0;
      words[i+1]=0;
    }
    
    
    mm.SetFastClock(true);
    mm.SetSlowClock(true);
    
    int full_counter=0;
    int singleRep   =0;
    nNoHitWords=0;
    nNoHitWords=0;

    int nTrigger=(int) ( (float)(cNdaq*1e-6)/DAQwindow );

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for( int r=0; r<cRep; r++) {
      singleRep=0;
      
      uint32_t defaultV=0;
      defaultV=mm.Read(MiniMalta::SCCONTROL);    

      mm.SetSlowReadOutFifo(false);
      mm.ResetSlowFifo();
      mm.SetSlowReadOutFifo(true);
      
      //usleep(cNdaq); //old style
      for(uint32_t i=0; i<(uint32_t)nTrigger; i++){
        mm.Trigger(false,defaultV);
      }
      
      mm.SetSlowReadOutFifo(false);
      
      std::vector<uint32_t> words_1;
      std::vector<uint32_t> words_2;
      
      //new code
      mm.GetFifoStatus(true);
      bool fifo2Empty= mm.IsFifoIpSlowProgEmpty();
      //cout << "    fifo2Empty: " << fifo2Empty << " , EMPTY: " << mm.IsFifoIpSlowEmpty() << " , AL: " << mm.IsFifoIpSlowAlmostEmpty() << endl;
      while( fifo2Empty==false and !(mm.IsFifoIpSlowEmpty() or mm.IsFifoIpSlowAlmostEmpty()) ) {
        mm.ReadMaltaWord(words,numwords);   
        for (uint32_t i=0;i<numwords;i+=2) {
          words_1.push_back(words[i+0]);
          words_2.push_back(words[i+1]);
        }
        mm.GetFifoStatus(true);
        fifo2Empty = mm.IsFifoIpSlowProgEmpty();
        //cout << "    fifo2Empty: " << fifo2Empty << " , EMPTY: " << mm.IsFifoIpSlowEmpty() << " , AL: " << mm.IsFifoIpSlowAlmostEmpty() << endl;
      } 

      //cout << " now reading normal mode after count is: " << words_1.size() << " , " << words_2.size() << endl;
      //old way: full readout straight on
      while(1) {
        mm.ReadMaltaWord(words,2);   
        uint32_t word1=words[0];
        uint32_t word2=words[1];
        if(word1 == 0){ break; } 
        if(word2 == 0){ continue;}
        words_1.push_back(word1);
        words_2.push_back(word2);
      } 

      if ( words_1.size() != words_2.size() ) {
        std::cout << "Size of the 2 words does not match: " << words_1.size() << " != " << words_2.size() << " ... ABORTING ... " << endl;
        exit(-1);
      }

      //this could have been done better .....
      for (unsigned int m=0; m<words_1.size(); m++) {

        md.setWord1( words_1.at(m) );
        md.setWord2( words_2.at(m) );
        md.unpack();
        
        nWords++;
        if(md.getNhits() == 0){
          if (md.getL1id()==0) {
            //md.dump();
            //cout << " W1: " << words_1.at(m) << " , W2: " << words_2.at(m) << endl;
          }
          nNoHitWords++;
          ///cout << endl << "Current fraction of emtpy (no pixels) words: " << nNoHitWords/nWords << endl;
        }
        if (md.getPixel()==65535 ) {
          md.dump();	
	  continue;
        }
        if (md.getBcid()*25>50e3) continue;
        if (md.getNhits()>6 ) {
          md.dump();	
          continue;
        }
	md.dump();	
	
        //if (md.getNhits()==0) cout << "I got a word with no hits!!!" << endl; 
        //recognise hits (could be optimised)
        for (unsigned int h=0; h<md.getNhits(); h++) {
          int xVal=md.getHitColumn(h);
          int yVal=md.getHitRow(h);
	  cout << "    My fucking hit" << xVal << " , " << yVal << endl; 
          the_h2->Fill(xVal,yVal);  
          
          if(cVerbose){
            uint32_t m_pulseRow1=0x00000000;
            uint32_t m_pulseRow2=0x00000000;
            uint32_t m_pulseCol =0x0000;
            int diagonal = ( (yVal-xVal)>=0 ? (yVal-xVal)%16 : 16+(yVal-xVal)%16 );
            uint32_t m_diag =0x0000;
            m_diag |= 1<< diagonal;
            m_pulseCol |= 1<<xVal;
            if (yVal<32) m_pulseRow1 |= 1<<yVal;
            else         m_pulseRow2 |= 1<<(yVal-32);
          
            // cout << "Potentially noisy pixel: x=" << xVal <<" y="<< yVal 
           //  << ";   Masking column: " << std::bitset<16>(m_pulseCol) << ", pix1: " << std::bitset<32>(m_pulseRow1) << ", pix2: " << std::bitset<32>(m_pulseRow2) << endl;
             
             cout << "Potentially noisy pixel: x=" << std::dec << xVal <<" y="<< yVal <<" diag="<< diagonal;
             cout << ";   Masking column: " <<"0x"<< std::hex<< (0xffff-m_pulseCol) << ", pix1: " <<"0x"<< std::hex<<(0xffffffff-m_pulseRow1) 
                  << ", pix2: " <<"0x"<< std::hex<<(0xffffffff-m_pulseRow2) << ", diag: " <<"0x"<< std::hex<<(0xffff-m_diag) << std::dec << endl;           
          }
          
        }
        mt.Set(&md);
        mt.Fill();
        
        singleRep++;
      }
      full_counter+=singleRep;
      
      nhits_vs_idb->SetPoint(count,paramVal,the_h2->GetEntries());    
      nhits_vs_idb->SetPointError(count,0.0,sqrt(the_h2->GetEntries()));
      rate_vs_idb->SetPoint(count,paramVal,(float)the_h2->GetEntries()/(float)cRep/(float)cNdaq*20 );  //was 1e3
      rate_vs_idb->SetPointError( count,0.0,sqrt(the_h2->GetEntries())/(float)cRep/(float)cNdaq*20 ); //was 1e3
      
      nhits_vs_idb_l->SetPoint(count,paramVal,the_h2->Integral(1,8,1,-1));    
      nhits_vs_idb_l->SetPointError(count,0.0,sqrt(the_h2->Integral(1,8,1,-1)));
      rate_vs_idb_l->SetPoint(count,paramVal,(float)the_h2->Integral(1,8,1,-1)/(float)cRep/(float)cNdaq*20 );  //was 1e3
      rate_vs_idb_l->SetPointError( count,0.0,sqrt(the_h2->Integral(1,8,1,-1))/(float)cRep/(float)cNdaq*20 ); //was 1e3
      
      nhits_vs_idb_r->SetPoint(count,paramVal,the_h2->Integral(9,-1,1,-1));    
      nhits_vs_idb_r->SetPointError(count,0.0,sqrt(the_h2->Integral(9,-1,1,-1)));
      rate_vs_idb_r->SetPoint(count,paramVal,(float)the_h2->Integral(9,-1,1,-1)/(float)cRep/(float)cNdaq*20 );  //was 1e3
      rate_vs_idb_r->SetPointError( count,0.0,sqrt(the_h2->Integral(9,-1,1,-1))/(float)cRep/(float)cNdaq*20 ); //was 1e3
      
      // get the number of pixels with any activity
      int active_pix = 0;
      int active_pix_l = 0;
      int active_pix_r = 0;
      int active_pix_40Hz = 0;
      int active_pix_40Hz_l = 0;
      int active_pix_40Hz_r = 0;
      for(int i=0; i<the_h2->GetNbinsX();i++){
        for(int j=0; j<the_h2->GetNbinsY();j++){
          if(the_h2->GetBinContent(i+1,j+1) > 0){
            active_pix++;
            if(i<=7) active_pix_l++;
            else     active_pix_r++;
           // cout<<(float)the_h2->GetBinContent(i+1,j+1)/(float)cRep/(float)cNdaq*2e4<<endl;
           if((float)the_h2->GetBinContent(i+1,j+1)/(float)cRep/(float)cNdaq*2e4 > 40.){ //was 1e6
            active_pix_40Hz++;
            if(i<=7) active_pix_40Hz_l++;
            else     active_pix_40Hz_r++;
            }
          }
        }
      }
      npix_vs_idb->SetPoint(count,paramVal,active_pix);
      npix_vs_idb_l->SetPoint(count,paramVal,active_pix_l);
      npix_vs_idb_r->SetPoint(count,paramVal,active_pix_r);
      
      npix_vs_idb_40Hz->SetPoint(count,paramVal,active_pix_40Hz);
      npix_vs_idb_40Hz_l->SetPoint(count,paramVal,active_pix_40Hz_l);
      npix_vs_idb_40Hz_r->SetPoint(count,paramVal,active_pix_40Hz_r);
      
      cout << "    Repetition: " << r << " has " << singleRep << " hits" << endl;
    }
    gStyle->SetOptStat(11);
    gStyle->SetPalette(1);
    TCanvas* can=new TCanvas(thresh.str().c_str(),thresh.str().c_str(),800,600);
    can->SetRightMargin(0.2);
    the_h2->SetMinimum(0);
    the_h2->Draw("COLZ");
    l1.Draw(); l2.Draw(); l3.Draw(); l4.Draw(); l5.Draw(); l6.Draw();
    can->Update();
    can->Print( ( outdir+"/"+(thresh.str())+".pdf").c_str() );
    noise_sumf->WriteObject(the_h2,("OccMap_"+thresh.str()).c_str());
    
    //filling Graph
    //      for (unsigned int p=0; p<selPixX.size(); p++) {
    //        int error=sqrt(v_hits.at(p));
    //        if (error==0) error=1.0;
    //        (v_plots.at(p))->SetPoint(count,paramVal,v_hits.at(p));
    //        (v_plots.at(p))->SetPointError(count,0.0,error);
    //}

    if(!thrRegister.compare("IDB")) cout<< "noise test; IDB value: " << paramVal << "  HAS: " << full_counter << " hits" << endl;
    if(!thrRegister.compare("ITHR")) cout<< "noise test; ITHR value: " << paramVal << "  HAS: " << full_counter << " hits" << endl;
    if (nNoHitWords != 0 && nWords !=0)
      cout << endl << "    -----> Current fraction of emtpy (no pixels) words: " << nNoHitWords/nWords << endl;

    //  for (unsigned int p=0; p<selPixX.size(); p++) {
    //       h_base->Fill(selPixX.at(p), selPixY.at(p), (float)cNdaq_counter/(float)cNdaq);
    //  }
    
  } //end of param loop
  
  //plotting & tree filling
  /*
    for (unsigned int p=0; p<selPixX.size(); p++) {

    //TCanvas* can=new TCanvas(nam.str().c_str(),nam.str().c_str(),800,600);
      gStyle->SetOptStat(0);
      gStyle->SetOptFit(1111);
      h_base->SetMaximum(cNdaq*1.20);
      h_base->SetMinimum(0.0);
      h_base->Draw("AXIS");
      (v_plots.at(p))->Draw("P");
    
     TF1* fit=new TF1( ("fit_"+nam.str()).c_str(),"[0]/2*(1-TMath::Erf(1/TMath::Sqrt(2)*(x-[1])/[2]))",vLow-1,vHigh);
      fit->SetParName(0,"base");
      fit->SetParName(1,"#mu");
      fit->SetParName(2,"#sigma");
      fit->SetParameter(0,cNdaq);
      fit->SetParLimits(0, cNdaq*0.4,cNdaq*2);
      fit->SetParameter(1,(vHigh-vLow)/2);
      fit->SetParLimits(1,vLow+2*vStep,vHigh-2*vStep);
      fit->SetParameter(2,30);
      fit->SetParLimits(2,1.0,50);
      //result=    TFitResultPtr 	
      (v_plots.at(p))->Fit(fit,"RE0S");
      fit->SetLineStyle(2);
      fit->SetLineColor(2);
      TLine l=TLine(fit->GetParameter(1),0,fit->GetParameter(1),h_base->GetMaximum()*0.95);
      l.SetLineWidth(1);
      l.Draw("SAMEC");
      fit->Draw("SAMEC");

      float th_DAC=fit->GetParameter(1);
      float th_mV =th_DAC*7+579;
      float th_el =(1780-th_mV)*1.43;
      float noise_el = fit->GetParameter(2)*1.43*7;
      cout << endl;
      cout << "Threshold value::: "<<th_mV<< " mV; " << th_el << " electrons" << endl;
      cout << "Noise Value:: " << noise_el << " electrons" << endl;
      cout << endl;
      TLatex lx;
      lx.SetNDC();
      lx.DrawLatex(0.6, 0.6, Form("#scale[0.6]{Threshold is %3.0f e}",th_el));
      lx.DrawLatex(0.6, 0.52, Form("#scale[0.6]{Noise is %3.0f e}",noise_el));
      lx.DrawLatex(0.6, 0.46, Form("#scale[0.6]{Pixel is [%2i, %2i]}",selPixX[p], selPixY[p]));

      //delete can;
      ///systemcall("evince "+(outdir+"/"+nam.str()+".pdf")+" &");

      //miniTree filling
      tree_pixX = selPixX[p];
      tree_pixY = selPixY[p];
      tree_idb = cIDB;
      tree_sigma = 0;
      tree_chi2  = 0;
      th_tree->Fill();
    }
  */


  mt.Close();
  
  /*
  TCanvas* can=new TCanvas("","",800,600);
  h_base->Draw("colz");
  ostringstream nam;
  nam << "Pixel_Noise_IDB"+cIDB_str.str();
  can->Update();
  can->Print( (outdir+"/"+nam.str()+".pdf").c_str() );
  */
  
  //th_tree->Write();
  
  TLegend *leg = new TLegend(0.65,0.65,0.85,0.85);
  leg->SetFillColor(0); leg->SetBorderSize(0); leg->SetTextSize(0.04); leg->SetTextFont(42);
  leg->AddEntry(nhits_vs_idb, "Full range", "lp");
  leg->AddEntry(nhits_vs_idb_l, "columns 0-7", "lp");
  leg->AddEntry(nhits_vs_idb_r, "columns 8-15", "lp");

  
  TCanvas* can2=new TCanvas("","",400,300);
  can2->SetLogy(1);
  can2->Update(); 
   nhits_vs_idb->SetMinimum(0.1);
  nhits_vs_idb->Draw("AP");
  nhits_vs_idb_l->Draw("sameP");
  nhits_vs_idb_r->Draw("sameP");
  leg->Draw();
  if(!thrRegister.compare("IDB")) can2->Print( ( outdir+"/nhits_vs_idb.pdf").c_str() );
  if(!thrRegister.compare("ITHR")) can2->Print( ( outdir+"/nhits_vs_ithr.pdf").c_str() );
  
  can2->Update();
  npix_vs_idb->SetMinimum(0.1);
  npix_vs_idb->Draw("AP");
  npix_vs_idb_l->Draw("sameP");
  npix_vs_idb_r->Draw("sameP");
  leg->Draw();
  if(!thrRegister.compare("IDB")) can2->Print( ( outdir+"/npix_vs_idb.pdf").c_str() );
  if(!thrRegister.compare("ITHR")) can2->Print( ( outdir+"/npix_vs_ithr.pdf").c_str() );
  
  can2->Update();
  npix_vs_idb_40Hz->SetMinimum(0.1);
  npix_vs_idb_40Hz->Draw("AP");
  npix_vs_idb_40Hz_l->Draw("sameP");
  npix_vs_idb_40Hz_r->Draw("sameP");
  leg->Draw();
  if(!thrRegister.compare("IDB")) can2->Print( ( outdir+"/npix_vs_idb_40Hz.pdf").c_str() );
  if(!thrRegister.compare("ITHR")) can2->Print( ( outdir+"/npix_vs_ithr_40Hz.pdf").c_str() );
  
  can2->Update();
  rate_vs_idb->SetMinimum(0.1/(float)cRep/(float)cNdaq*20); //was 1e3 
  rate_vs_idb->Draw("AP");
  rate_vs_idb_l->Draw("sameP");
  rate_vs_idb_r->Draw("sameP");
  leg->Draw();
  TLine l=TLine(vLow,1./(float)cRep/(float)cNdaq*20,vHigh,1./(float)cRep/(float)cNdaq*20); //was 1e3
  l.SetLineWidth(1);
  l.Draw("SAMEC");
  if(!thrRegister.compare("IDB")) can2->Print( ( outdir+"/rate_vs_idb.pdf").c_str() );
  if(!thrRegister.compare("ITHR")) can2->Print( ( outdir+"/rate_vs_ithr.pdf").c_str() );
  
  noise_sumf->WriteObject(nhits_vs_idb,"nhits_vs_idb");
  noise_sumf->WriteObject(npix_vs_idb,"npix_vs_idb");
  noise_sumf->WriteObject(rate_vs_idb,"rate_vs_idb");
    noise_sumf->WriteObject(nhits_vs_idb_l,"nhits_vs_idb_l");
  noise_sumf->WriteObject(npix_vs_idb_l,"npix_vs_idb_l");
  noise_sumf->WriteObject(rate_vs_idb_l,"rate_vs_idb_l");
    noise_sumf->WriteObject(nhits_vs_idb_r,"nhits_vs_idb_r");
  noise_sumf->WriteObject(npix_vs_idb_r,"npix_vs_idb_r");
  noise_sumf->WriteObject(rate_vs_idb_r,"rate_vs_idb_r");
  
  noise_sumf->WriteObject(npix_vs_idb_40Hz,"npix_vs_idb_40Hz");
  noise_sumf->WriteObject(npix_vs_idb_40Hz_l,"npix_vs_idb_40Hz_l");
  noise_sumf->WriteObject(npix_vs_idb_40Hz_r,"npix_vs_idb_40Hz_r");
  
  
  noise_sumf->Write();
  noise_sumf ->Close();
  
  cout << "Have a nice day" << endl;
  return 0;

}
