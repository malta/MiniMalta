#include "MiniMalta/MiniMaltaTree.h"

using namespace std;

MiniMaltaTree::MiniMaltaTree(){
  pixel=0;
  group=0;
  parity=0;
  dcolumn=0;
  chipbcid=0;
  chipid=0;
  phase=0;
  winid=0;
  bcid=0;
  run=0;
  l1id=0;
  l1idC=0;
  isDuplicate=0;
  ithres=0;
  timer=0;
  word1=0;
  word2=0;
  coordX=0;
  coordY=0;
  vlow=0;
  vhigh=0;
  maskI=0;
  m_entry=0;
}

MiniMaltaTree::~MiniMaltaTree(){
  if(m_file) delete m_file;
}

void MiniMaltaTree::Open(string filename, string options){
  m_file = TFile::Open(filename.c_str(),options.c_str());
  m_tree = new TTree("MALTA","DataStream");
  m_tree->Branch("pixel",&pixel,"pixel/i");
  m_tree->Branch("group",&group,"group/i");
  m_tree->Branch("parity",&parity,"parity/i");
  m_tree->Branch("dcolumn",&dcolumn,"dcolumn/i");
  m_tree->Branch("chipbcid",&chipbcid,"chipbcid/i");
  m_tree->Branch("chipid",&chipid,"chipid/i");
  m_tree->Branch("phase",&phase,"phase/i");
  m_tree->Branch("winid",&winid,"winid/i");
  m_tree->Branch("bcid",&bcid,"bcid/i");
  m_tree->Branch("runNumber",&run,"runNumber/i");
  m_tree->Branch("l1id",&l1id,"l1id/i");
  m_tree->Branch("l1idC",&l1idC,"l1idC/i"); //Valerio
  //////m_tree->Branch("isDuplicate",&isDuplicate,"isDuplicate/i");
  m_tree->Branch("ithres",&ithres,"ithres/i");
  m_tree->Branch("timer",&timer,"timer/f");
  m_tree->Branch("vhigh",&vhigh,"vhigh/i");
  m_tree->Branch("vlow",&vlow,"vlow/i");
  m_tree->Branch("maskI",&maskI,"maskI/i");
  m_tree->Branch("word1",&word1,"word1/i");
  m_tree->Branch("word2",&word2,"word2/i");

  m_t0 = chrono::steady_clock::now();
  m_entry = 0;
}

void MiniMaltaTree::Set(MiniMaltaData * data){
  pixel = data->getPixel();
  group = data->getGroup();
  parity = data->getParity();
  dcolumn = data->getDcolumn();
  chipbcid = data->getChipbcid();
  chipid = data->getChipid();
  phase = data->getPhase();
  winid = data->getWinid();
  bcid = data->getBcid();
  l1id = data->getL1id();
  word1 = data->getWord1();
  word2 = data->getWord2();
  timer = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()-m_t0).count()/1000.;
}

int MiniMaltaTree::GetIthres(){
  return ithres;
}

int MiniMaltaTree::GetRunNumber(){
  return run;
}

uint32_t MiniMaltaTree::GetL1idC(){
  return l1idC;
}

int MiniMaltaTree::GetIsDuplicate(){
  return isDuplicate;
}

float MiniMaltaTree::GetTimer(){
  return timer;
}

int MiniMaltaTree::GetVhigh(){
  return vhigh;
}

int MiniMaltaTree::GetVlow(){
  return vlow;
}

void MiniMaltaTree::SetIthres(int v){
  ithres = v;
}

void MiniMaltaTree::SetMaskI(int v){
  maskI = v;
}

void MiniMaltaTree::SetRunNumber(int v){
  run = v;
}

void MiniMaltaTree::SetL1idC(uint32_t v){
  l1idC = v;
}

void MiniMaltaTree::SetIsDuplicate(int v){
  isDuplicate = v;
}

void MiniMaltaTree::SetVhigh(int v){
  vhigh = v;
}

void MiniMaltaTree::SetVlow(int v){
  vlow = v;
}

void MiniMaltaTree::Fill(){
  m_tree->Fill();
}

int MiniMaltaTree::Next(){

  int ret=m_tree->GetEntry(m_entry);
  m_entry++;
  return ret;

}

MiniMaltaData * MiniMaltaTree::Get(){
  m_data.setPixel(pixel);
  m_data.setGroup(group);
  m_data.setParity(parity);
  m_data.setDcolumn(dcolumn);
  m_data.setChipbcid(chipbcid);
  m_data.setChipid(chipid);
  m_data.setPhase(phase);
  m_data.setWinid(winid);
  m_data.setBcid(bcid);
  m_data.setL1id(l1id);
  m_data.setWord1(word1);
  m_data.setWord2(word2);
  return &m_data;
}

void MiniMaltaTree::Close(){
  m_file->cd();
  m_tree->Write();
  m_file->Close();
}
  
TFile* MiniMaltaTree::GetFile(){
  return m_file;
}
