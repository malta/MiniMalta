#include "MiniMalta/MiniMaltaNoiseHandler.h"

#include <iostream>
#include <unistd.h>
#include <bitset>
#include <fstream> 

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"

using namespace std;

MiniMaltaNoiseHandler::MiniMaltaNoiseHandler(std::string chipName, bool is_irradiated) {
  m_verbose=false;
  ////m_strategy = mask;
  
  m_loopIndex=0;

  m_posX.clear();
  m_posY.clear();
  m_pulseCol =0x0000;
  m_pulseRow1=0x00000000;
  m_pulseRow2=0x00000000;
  m_maskCol  =0x0000;
  m_maskRow1 =0x00000000;
  m_maskRow2 =0x00000000;
  m_maskDiag =0x0000;
  
  m_chipNames.clear();
  m_chipName=chipName;
  m_is_irradiated = is_irradiated;
/*  
  m_chipNames.push_back("W4R6");
  m_chipNames.push_back("W5R6");
  m_chipNames.push_back("W2R6");
  m_chipNames.push_back("W1R10");
  m_chipNames.push_back("W4R9");
  m_chipNames.push_back("W5R9");
  m_chipNames.push_back("W1R9");
  m_chipNames.push_back("W2R9");
  
  */
}

MiniMaltaNoiseHandler::~MiniMaltaNoiseHandler(){}


void MiniMaltaNoiseHandler::SetVerbose(bool enable){
  m_verbose=enable;
}


int MiniMaltaNoiseHandler::GetNSteps() {
  /*
  if      (m_strategy==MASK_SINGLE) return 16*64;
  else if (m_strategy==MASK_2     ) return 16*32;
  else if (m_strategy==MASK_2DC   ) return  8*64;
  else if (m_strategy==MASK_4     ) return 16*16;
  else if (m_strategy==MASK_16    ) return 16* 8;
  else if (m_strategy==MASK_16    ) return  8* 8;
  */
  return 0;
}


bool MiniMaltaNoiseHandler::ProcessFile(std::string fileName) {
/*  
  for (int i=0; i< m_chipNames.size(); i++){
    if(fileName.find(m_chipNames.at(i)) != std::string::npos){
       cout << "Chip name: " << m_chipNames.at(i)  <<" found in the filename, good" <<endl;
       m_chipName = m_chipNames.at(i);
       }
  }
*/
  if(fileName.find(m_chipName) == std::string::npos){
    cout << "Chip name: " << m_chipName  <<" not found in the filename!" <<endl;
    return false;
  }
  
//  int idb;
//  sscanf( fileName.c_str(), "%*[^_]IDB_%d", &idb );
//  cout << "IDB: " << idb  <<" extracted from filename" <<endl;
  
  TFile ff(fileName.c_str());
  if(!ff.IsOpen()){
    cout << "Unable to open the file:" << fileName  <<". Check the path..."<<endl;
    return false;
  }
  
  m_posX.clear();
  m_posY.clear();
  m_maskCol =0xffff;
  m_maskRow1=0xffffffff;
  m_maskRow2=0xffffffff;
  m_maskDiag=0xffff;

  TH2F *map40, *map30, *map20;
  
  if(m_is_irradiated){
    map40 = (TH2F*)ff.Get("OccMap_IDB_120");
    if(map40==0){
      cout << "Unable to find noise map for IDB=120!"<<endl;
      return false;
    }   
    map30 = (TH2F*)ff.Get("OccMap_IDB_110");
    map20 = (TH2F*)ff.Get("OccMap_IDB_100");
  }
  else{
    map40 = (TH2F*)ff.Get("OccMap_IDB_40");
    if(map40==0){
      cout << "Unable to find noise map for IDB=40!"<<endl;
      return false;
    }   
    map30 = (TH2F*)ff.Get("OccMap_IDB_30");
    map20 = (TH2F*)ff.Get("OccMap_IDB_20");
  }
  
  // set to 1-5% of total occupancy
  float noise_fraction40 = 0.01;
  float noise_fraction30 = 0.07;
  float noise_fraction20 = 0.05;
  
  float tot_occ_40 = map40->GetEntries();
  int n_noisyPix40 =0;
  
  for(int i=0; i<map40->GetNbinsX();i++){
    for(int j=0; j<map40->GetNbinsY();j++){    
      if((float)map40->GetBinContent(i+1,j+1) > noise_fraction40*tot_occ_40){
        m_posX.push_back(i);
        m_posY.push_back(j);
        int diagonal = ( (j-i)>=0 ? (j-i)%16 : 16+(j-i)%16 );
        
        uint32_t tmp_pulseRow1=0x00000000;
        uint32_t tmp_pulseRow2=0x00000000;
        uint32_t tmp_pulseCol =0x0000;
        uint32_t tmp_diag =0x0000;
        
        tmp_diag |= 1<< diagonal;
        tmp_pulseCol |= 1<<i;
        if (j<32) tmp_pulseRow1 |= 1<<j;
        else      tmp_pulseRow2 |= 1<<(j-32); 
        
        m_maskCol  = m_maskCol  & (0xffff-tmp_pulseCol);
        m_maskRow1 = m_maskRow1 & (0xffffffff-tmp_pulseRow1); 
        m_maskRow2 = m_maskRow2 & (0xffffffff-tmp_pulseRow2);
        m_maskDiag = m_maskDiag   & (0xffff-tmp_diag);
        
        n_noisyPix40++;  
      }
    }
  }

  if(m_is_irradiated){
    cout<<"Ran at IDB=120, found "<<n_noisyPix40<<" noisy pixels"<<endl;
    cout<<"Re-running at IDB=110..."<<endl;
  }
  else{
    cout<<"Ran at IDB=40, found "<<n_noisyPix40<<" noisy pixels"<<endl;
    cout<<"Re-running at IDB=30..."<<endl;
  }

  // now check IDB30...
  for(int i=0; i<map30->GetNbinsX();i++){
    for(int j=0; j<map30->GetNbinsY();j++){  
      for(uint32_t k=0; k<m_posX.size();k++){      
        if(i == m_posX.at(k) && j == m_posY.at(k)){ 
          map30->SetBinContent(i+1,j+1, 0);
          map20->SetBinContent(i+1,j+1, 0);
        }
      }
    }
  }
  int n_noisyPix30 =0;
  int n_noisyPix20 =0;

  float tot_occ_30 = map30->GetSumOfWeights();
  
  for(int i=0; i<map30->GetNbinsX();i++){
    for(int j=0; j<map30->GetNbinsY();j++){
      
      if((float)map30->GetBinContent(i+1,j+1) > noise_fraction30*tot_occ_30){
        m_posX.push_back(i);
        m_posY.push_back(j);
        int diagonal = ( (j-i)>=0 ? (j-i)%16 : 16+(j-i)%16 );
        
        uint32_t tmp_pulseRow1=0x00000000;
        uint32_t tmp_pulseRow2=0x00000000;
        uint32_t tmp_pulseCol =0x0000;
        uint32_t tmp_diag =0x0000;
        
        tmp_diag |= 1<< diagonal;
        tmp_pulseCol |= 1<<i;
        if (j<32) tmp_pulseRow1 |= 1<<j;
        else      tmp_pulseRow2 |= 1<<(j-32); 
        m_maskCol  = m_maskCol  & (0xffff-tmp_pulseCol);
        m_maskRow1 = m_maskRow1 & (0xffffffff-tmp_pulseRow1); 
        m_maskRow2 = m_maskRow2 & (0xffffffff-tmp_pulseRow2);
        m_maskDiag = m_maskDiag   & (0xffff-tmp_diag);
        n_noisyPix30++; 
      }
    }
  }
  if(m_is_irradiated){
    cout<<"Ran at IDB=110, found "<<n_noisyPix30<<" noisy pixels"<<endl;
    cout<<"Re-running at IDB=100..."<<endl;
  }
  else{
    cout<<"Ran at IDB=30, found "<<n_noisyPix30<<" noisy pixels"<<endl;
    cout<<"Re-running at IDB=20..."<<endl;
  }
  
  // now check IDB20...
  for(int i=0; i<map20->GetNbinsX();i++){
    for(int j=0; j<map20->GetNbinsY();j++){  
      for(uint32_t k=0; k<m_posX.size();k++){
        if(i == m_posX.at(k) && j == m_posY.at(k)){ 
          map20->SetBinContent(i+1,j+1, 0);
        }
      }
    }
  }

  
  float tot_occ_20 = map20->GetSumOfWeights();
    
  for(int i=0; i<map20->GetNbinsX();i++){
    for(int j=0; j<map20->GetNbinsY();j++){
      
      if((float)map20->GetBinContent(i+1,j+1) > noise_fraction20*tot_occ_20){
        m_posX.push_back(i);
        m_posY.push_back(j);
        int diagonal = ( (j-i)>=0 ? (j-i)%16 : 16+(j-i)%16 );
        
        uint32_t tmp_pulseRow1=0x00000000;
        uint32_t tmp_pulseRow2=0x00000000;
        uint32_t tmp_pulseCol =0x0000;
        uint32_t tmp_diag =0x0000;
        
        tmp_diag |= 1<< diagonal;
        tmp_pulseCol |= 1<<i;
        if (j<32) tmp_pulseRow1 |= 1<<j;
        else      tmp_pulseRow2 |= 1<<(j-32); 
        
        m_maskCol  = m_maskCol  & (0xffff-tmp_pulseCol);
        m_maskRow1 = m_maskRow1 & (0xffffffff-tmp_pulseRow1); 
        m_maskRow2 = m_maskRow2 & (0xffffffff-tmp_pulseRow2);
        m_maskDiag = m_maskDiag   & (0xffff-tmp_diag);
        
        n_noisyPix20++;
      }
    }
  }

  if(m_is_irradiated){
    cout<<"Ran at IDB=100, found "<<n_noisyPix20<<" noisy pixels"<<endl;
  }
  else{
    cout<<"Ran at IDB=20, found "<<n_noisyPix20<<" noisy pixels"<<endl;
  }
  
  float n_noisyPix = n_noisyPix20+n_noisyPix30+n_noisyPix40;
  cout << "Total number of noisy pixels is: " << n_noisyPix << endl;
  cout << "masking pattern: Col=" << std::hex << m_maskCol 
       << " Row1=" << std::hex << m_maskRow1
       << " Row2=" << std::hex << m_maskRow2
       << " Diag=" << std::hex<<  m_maskDiag << std::dec << endl;
  
  for (unsigned int p=0; p<m_posX.size(); p++) {
    cout << "  [ " << m_posX.at(p) << " , " << m_posY.at(p) << " ] " << endl;
  }
  cout << endl;
         
  /*
  if ( loopIndex>=GetNSteps() ) {
    cout << "LoopIndex: " << loopIndex << " is greater than the allowed number for this strategy " << GetNSteps() << " .... please check " << endl;
    return false;
  }

  m_loopIndex=loopIndex;
  */

  return true;
}


bool MiniMaltaNoiseHandler::ProcessPairs(std::vector<int> xpair, std::vector<int> ypair) {

  if(xpair.size() != ypair.size()){
    cout << "Size of the passed pixel arrays do not match!!!!" <<endl;
    return false;
  }

  
  m_posX.clear();
  m_posY.clear();
  m_maskCol =0xffff;
  m_maskRow1=0xffffffff;
  m_maskRow2=0xffffffff;
  m_maskDiag=0xffff;

  
  for(uint32_t p=0; p<xpair.size();p++){
	
	int i = xpair.at(p);
	int j = ypair.at(p);
        m_posX.push_back(i);
        m_posY.push_back(j);
        int diagonal = ( (j-i)>=0 ? (j-i)%16 : 16+(j-i)%16 );
        
        uint32_t tmp_pulseRow1=0x00000000;
        uint32_t tmp_pulseRow2=0x00000000;
        uint32_t tmp_pulseCol =0x0000;
        uint32_t tmp_diag =0x0000;
        
        tmp_diag |= 1<< diagonal;
        tmp_pulseCol |= 1<<i;
        if (j<32) tmp_pulseRow1 |= 1<<j;
        else      tmp_pulseRow2 |= 1<<(j-32); 
        
        m_maskCol  = m_maskCol  & (0xffff-tmp_pulseCol);
        m_maskRow1 = m_maskRow1 & (0xffffffff-tmp_pulseRow1); 
        m_maskRow2 = m_maskRow2 & (0xffffffff-tmp_pulseRow2);
        m_maskDiag = m_maskDiag   & (0xffff-tmp_diag);
  }


  cout << "Masking from vector pair; Total number of masked noisy pixels is: " << xpair.size() << endl;
  cout << "masking pattern: Col=" << std::hex << m_maskCol 
       << " Row1=" << std::hex << m_maskRow1
       << " Row2=" << std::hex << m_maskRow2
       << " Diag=" << std::hex<<  m_maskDiag << std::dec << endl;
  
  for (unsigned int p=0; p<m_posX.size(); p++) {
    cout << "  [ " << m_posX.at(p) << " , " << m_posY.at(p) << " ] " << endl;
  }
  cout << endl;

  return true;
}


bool MiniMaltaNoiseHandler::ProcessTextFile(std::string fileName) {

  std::vector<int> xpair, ypair;
  std::vector<int> diagcoord;

  int ppx, ppy;
  std::ifstream myfile(fileName);
  
  if(myfile.fail()){
    cout << "File: "<<fileName<< "does not exist, check the path"<<endl;
    return false;
  }
 

 
  while ( myfile >> ppx >> ppy){
     xpair.push_back(ppx);
     ypair.push_back(ppy);
  }
  myfile.close();

  if(xpair.size() != ypair.size()){
    cout << "Size of the passed pixel arrays do not match!!!!" <<endl;
    return false;
  }

  
  m_posX.clear();
  m_posY.clear();
  m_maskCol =0xffff;
  m_maskRow1=0xffffffff;
  m_maskRow2=0xffffffff;
  m_maskDiag=0xffff;

  
  for(uint32_t p=0; p<xpair.size();p++){
	
	int i = xpair.at(p);
	int j = ypair.at(p);
        m_posX.push_back(i);
        m_posY.push_back(j);
        int diagonal = ( (j-i)>=0 ? (j-i)%16 : 16+(j-i)%16 );
        diagcoord.push_back(diagonal);
        
        uint32_t tmp_pulseRow1=0x00000000;
        uint32_t tmp_pulseRow2=0x00000000;
        uint32_t tmp_pulseCol =0x0000;
        uint32_t tmp_diag =0x0000;
        
        tmp_diag |= 1<< diagonal;
        tmp_pulseCol |= 1<<i;
        if (j<32) tmp_pulseRow1 |= 1<<j;
        else      tmp_pulseRow2 |= 1<<(j-32); 
        
        m_maskCol  = m_maskCol  & (0xffff-tmp_pulseCol);
        m_maskRow1 = m_maskRow1 & (0xffffffff-tmp_pulseRow1); 
        m_maskRow2 = m_maskRow2 & (0xffffffff-tmp_pulseRow2);
        m_maskDiag = m_maskDiag   & (0xffff-tmp_diag);
  }


  cout << "Masking from TXT file; Total number of masked pixels is: " << xpair.size() << endl;
  cout << "masking pattern: Col=" << std::hex << m_maskCol 
       << " Row1=" << std::hex << m_maskRow1
       << " Row2=" << std::hex << m_maskRow2
       << " Diag=" << std::hex<<  m_maskDiag << std::dec << endl;
  
  for (unsigned int p=0; p<m_posX.size(); p++) {
    cout << "  [ " << m_posX.at(p) << " , " << m_posY.at(p) << " ]   , diag="<<diagcoord.at(p) << endl;
  }
  cout << endl;

  return true;
}


vector<int> MiniMaltaNoiseHandler::GetPixelPosX() {return  m_posX;}


vector<int> MiniMaltaNoiseHandler::GetPixelPosY() {return  m_posY;}


uint32_t MiniMaltaNoiseHandler::GetMaskColWord()   {return m_maskCol;}


uint32_t MiniMaltaNoiseHandler::GetMaskRowWord1()   {return m_maskRow1;}


uint32_t MiniMaltaNoiseHandler::GetMaskRowWord2()   {return m_maskRow2;}

uint32_t MiniMaltaNoiseHandler::GetMaskDiagWord()   {return m_maskDiag;}


void MiniMaltaNoiseHandler::Dump() {
  cout << endl;
  cout << "Enabled pixels: " << endl;
  for (unsigned int p=0; p<m_posX.size(); p++) {
    cout << " ( " << m_posX.at(p) << " , " << m_posY.at(p) << " )" << endl;
  }
  cout << "Pulsing words: " <<  std::bitset<16>(m_pulseCol) 
       << " , " << std::bitset<32>(m_pulseRow1) << " - " << std::bitset<32>(m_pulseRow2) << endl;
  cout << "Masking column: " << std::bitset<16>(m_maskCol) << endl;
}




/*
void MiniMaltaNoiseHandler::PrepareLoop_Single() {
  if (m_verbose) cout << "I will be preparing stage: " <<  m_loopIndex << " for strategy: SINGLE " << endl;
  
  //loop will proceed over all the col in a given row

  //pixX
  int pixY= (int)(m_loopIndex/16);
  int pixX= m_loopIndex%16;
  m_posX.push_back( pixX );
  m_posY.push_back( pixY );
   
  for(uint32_t i=0; i<m_posX.size(); i++){
    m_pulseCol |= 1<<m_posX[i];
  }
  m_pulseCol = 
  m_maskCol  = m_pulseCol;
  
  for(uint32_t i=0; i<m_posY.size(); i++){
    if (m_posY[i]<32) m_pulseRow1 |= 1<<m_posY[i];
    else              m_pulseRow2 |= 1<<(m_posY[i]-32);
  }
  if (m_verbose) Dump();
}
*/
