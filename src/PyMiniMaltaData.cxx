/************************************
 * PyMiniMaltaDataData
 * Brief: Python module for MiniMaltaDataData
 *
 * Author: Carlos.Solans@cern.ch
 *         Ignacio.Asensi@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "MiniMalta/MiniMaltaData.h"
#include <iostream>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif


#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
  static struct PyModuleDef moduledef = {		\
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	\
  };							\
  m = PyModule_Create(&moduledef);			\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000


/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MiniMaltaData *obj;
  std::vector<uint32_t> vec;
} PyMiniMaltaData;

static int _PyMiniMaltaData_init(PyMiniMaltaData *self)
{
  self->obj = new MiniMaltaData();
  return 0;
}

static void _PyMiniMaltaData_dealloc(PyMiniMaltaData *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}


PyObject * _PyMiniMaltaData_setRefbit(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setRefbit(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setPixel(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setPixel(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setGroup(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setGroup(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setDummyGroup(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setDummyGroup(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setParity(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setParity(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setDcolumn(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setDcolumn(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setChipbcid(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setChipbcid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setChipid(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setChipid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setPhase(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
      return Py_None;
  }
  self->obj->setPhase(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setWinid(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWinid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setL1id(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setL1id(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setBcid(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setBcid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setWord1(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWord1(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_setWord2(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWord2(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_getRefbit(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getRefbit();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}


PyObject * _PyMiniMaltaData_getPixel(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getPixel();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getGroup(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getGroup();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getDummyGroup(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getDummyGroup();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getParity(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getParity();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getDcolumn(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getDcolumn();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getChipbcid(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getChipbcid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getChipid(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getChipid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getPhase(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getPhase();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getWinid(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWinid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getNhits(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getNhits();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getBcid(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getBcid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getL1id(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getL1id();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getWord1(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWord1();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getWord2(PyMiniMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWord2();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getHitRow(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
    
  PyObject *py_ret;
  int ret = self->obj->getHitRow(pos);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_getHitColumn(PyMiniMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }

  PyObject *py_ret;
  int ret = self->obj->getHitColumn(pos);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMiniMaltaData_pack(PyMiniMaltaData *self)
{
  self->obj->pack();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_unpack(PyMiniMaltaData *self, PyObject *args)
{
  self->obj->unPack();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMiniMaltaData_toString(PyMiniMaltaData *self)
{
    PyObject *py_ret;
    py_ret=PyUnicode_FromString(self->obj->toString().c_str());
    return py_ret;
}

PyObject * _PyMiniMaltaData_getInfo(PyMiniMaltaData *self)
{
    PyObject *py_ret;
    py_ret=PyUnicode_FromString(self->obj->getInfo().c_str());
    return py_ret;
}


static PyMethodDef PyMiniMaltaData_methods[] = {
    {(char *) "setRefbit",(PyCFunction) _PyMiniMaltaData_setRefbit , METH_VARARGS, NULL },
    {(char *) "setPixel",(PyCFunction) _PyMiniMaltaData_setPixel , METH_VARARGS, NULL },
    {(char *) "setGroup",(PyCFunction) _PyMiniMaltaData_setGroup , METH_VARARGS, NULL },
    {(char *) "setDummyGroup",(PyCFunction) _PyMiniMaltaData_setDummyGroup , METH_VARARGS, NULL },
    {(char *) "setDcolumn",(PyCFunction) _PyMiniMaltaData_setDcolumn,  METH_VARARGS, NULL },
    {(char *) "setParity",(PyCFunction) _PyMiniMaltaData_setParity , METH_VARARGS, NULL },
    {(char *) "setChipbcid",(PyCFunction) _PyMiniMaltaData_setChipbcid , METH_VARARGS, NULL },
    {(char *) "setChipid",(PyCFunction) _PyMiniMaltaData_setChipid , METH_VARARGS, NULL },
    {(char *) "setPhase",(PyCFunction) _PyMiniMaltaData_setPhase , METH_VARARGS, NULL },
    {(char *) "setWinid",(PyCFunction) _PyMiniMaltaData_setWinid , METH_VARARGS, NULL },
    {(char *) "setL1id",(PyCFunction) _PyMiniMaltaData_setL1id , METH_VARARGS, NULL },
    {(char *) "setBcid",(PyCFunction) _PyMiniMaltaData_setBcid , METH_VARARGS, NULL },
    {(char *) "setWord1",(PyCFunction) _PyMiniMaltaData_setWord1 , METH_VARARGS, NULL },
    {(char *) "setWord2",(PyCFunction) _PyMiniMaltaData_setWord2 , METH_VARARGS, NULL },
    {(char *) "getRefbit",(PyCFunction) _PyMiniMaltaData_getRefbit,  METH_NOARGS, NULL },
    {(char *) "getPixel",(PyCFunction) _PyMiniMaltaData_getPixel,  METH_NOARGS, NULL },
    {(char *) "getGroup",(PyCFunction) _PyMiniMaltaData_getGroup,  METH_NOARGS, NULL },
    {(char *) "getDummyGroup",(PyCFunction) _PyMiniMaltaData_getDummyGroup,  METH_NOARGS, NULL },
    {(char *) "getParity",(PyCFunction) _PyMiniMaltaData_getParity,  METH_NOARGS, NULL },
    {(char *) "getDcolumn",(PyCFunction) _PyMiniMaltaData_getDcolumn,  METH_NOARGS, NULL },
    {(char *) "getChipbcid",(PyCFunction) _PyMiniMaltaData_getChipbcid,  METH_NOARGS, NULL },
    {(char *) "getChipid",(PyCFunction) _PyMiniMaltaData_getChipid,  METH_NOARGS, NULL },
    {(char *) "getPhase",(PyCFunction) _PyMiniMaltaData_getPhase,  METH_NOARGS, NULL },
    {(char *) "getWinid",(PyCFunction) _PyMiniMaltaData_getWinid,  METH_NOARGS, NULL },
    {(char *) "getNhits",(PyCFunction) _PyMiniMaltaData_getNhits,  METH_NOARGS, NULL },
    {(char *) "getHitColumn",(PyCFunction) _PyMiniMaltaData_getHitColumn,  METH_VARARGS, NULL },
    {(char *) "getHitRow",(PyCFunction) _PyMiniMaltaData_getHitRow,  METH_VARARGS, NULL },
    {(char *) "getBcid",(PyCFunction) _PyMiniMaltaData_getBcid,  METH_NOARGS, NULL },
    {(char *) "getL1id",(PyCFunction) _PyMiniMaltaData_getL1id,  METH_NOARGS, NULL },
    {(char *) "getWord1",(PyCFunction) _PyMiniMaltaData_getWord1,  METH_NOARGS, NULL },
    {(char *) "getWord2",(PyCFunction) _PyMiniMaltaData_getWord2,  METH_NOARGS, NULL },
    {(char *) "toString",(PyCFunction) _PyMiniMaltaData_toString,  METH_NOARGS, NULL },
    {(char *) "getInfo",(PyCFunction) _PyMiniMaltaData_getInfo,  METH_NOARGS, NULL },
    {(char *) "pack",(PyCFunction) _PyMiniMaltaData_pack,  METH_NOARGS, NULL },
    {(char *) "unpack",(PyCFunction) _PyMiniMaltaData_unpack,  METH_NOARGS, NULL },
    {NULL, NULL, 0, NULL}
};

PyTypeObject PyMiniMaltaData_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMiniMaltaData.MiniMaltaData",            /* tp_name */
    sizeof(PyMiniMaltaData),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    /* methods */
    (destructor) _PyMiniMaltaData_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    NULL,                       /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMiniMaltaData_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMiniMaltaData_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
	,0                                   /* tp_version_tag */
#endif
};

static PyMethodDef Module_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMiniMaltaData)
{
  PyObject *m;
  MOD_DEF(m, "PyMiniMaltaData", NULL, Module_methods);
  if(PyType_Ready(&PyMiniMaltaData_Type)<0){
    return MOD_ERROR;
  }
  PyModule_AddObject(m, (char *) "MiniMaltaData", (PyObject *) &PyMiniMaltaData_Type);
  return MOD_RETURN(m);
}
