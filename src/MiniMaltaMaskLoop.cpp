#include "MiniMalta/MiniMaltaMaskLoop.h"

#include <iostream>
#include <unistd.h>
#include <bitset>

using namespace std;

MiniMaltaMaskLoop::MiniMaltaMaskLoop( MaskStrategy mask) {
  m_verbose=false;
  m_strategy = mask;
  
  m_loopIndex=0;

  m_posX.clear();
  m_posY.clear();
  m_pulseCol =0x0000;
  m_pulseRow1=0x00000000;
  m_pulseRow2=0x00000000;
  m_maskCol  =0x0000;
  m_maskRow1 =0x00000000;
  m_maskRow2 =0x00000000;
}

MiniMaltaMaskLoop::~MiniMaltaMaskLoop(){}


void MiniMaltaMaskLoop::SetVerbose(bool enable){
  m_verbose=enable;
}


int MiniMaltaMaskLoop::GetNSteps() {
  if      (m_strategy==MASK_SINGLE) return 16*64;
  else if (m_strategy==MASK_2     ) return 16*32;
  else if (m_strategy==MASK_2DC   ) return  8*64;
  else if (m_strategy==MASK_4     ) return 16*16;
  // else if (m_strategy==MASK_16    ) return 16* 8;
  else if (m_strategy==MASK_16    ) return  1*64;
  return 0;
}


bool MiniMaltaMaskLoop::ProcessLoop(int loopIndex) {
  if ( loopIndex>=GetNSteps() ) {
    cout << "LoopIndex: " << loopIndex << " is greater than the allowed number for this strategy " << GetNSteps() << " .... please check " << endl;
    return false;
  }
  m_posX.clear();
  m_posY.clear();
  m_pulseCol =0x0000;
  m_pulseRow1=0x00000000;
  m_pulseRow2=0x00000000;
  m_maskCol  =0x0000;

  m_loopIndex=loopIndex;
  if      (m_strategy==MASK_SINGLE) PrepareLoop_Single();
  else if (m_strategy==MASK_2     ) PrepareLoop_2();
  else if (m_strategy==MASK_2DC   ) PrepareLoop_2dc();
  else if (m_strategy==MASK_4     ) PrepareLoop_4();
  else if (m_strategy==MASK_8     ) PrepareLoop_8();
  else if (m_strategy==MASK_16    ) PrepareLoop_16();
  return true;
}


vector<int> MiniMaltaMaskLoop::GetPixelPosX() {return  m_posX;}


vector<int> MiniMaltaMaskLoop::GetPixelPosY() {return  m_posY;}


uint32_t MiniMaltaMaskLoop::GetPulseColWord()  {return m_pulseCol;}


uint32_t MiniMaltaMaskLoop::GetPulseRowWord1() {return m_pulseRow1;}


uint32_t MiniMaltaMaskLoop::GetPulseRowWord2() {return m_pulseRow2;}


uint32_t MiniMaltaMaskLoop::GetMaskColWord()   {return m_maskCol;}


uint32_t MiniMaltaMaskLoop::GetMaskRowWord1()   {return m_maskRow1;}


uint32_t MiniMaltaMaskLoop::GetMaskRowWord2()   {return m_maskRow2;}


void MiniMaltaMaskLoop::Dump() {
  cout << endl;
  cout << "Enabled pixels: " << endl;
  for (unsigned int p=0; p<m_posX.size(); p++) {
    cout << " ( " << m_posX.at(p) << " , " << m_posY.at(p) << " )" << endl;
  }
  cout << "Pulsing words: " <<  std::bitset<16>(m_pulseCol) 
       << " , " << std::bitset<32>(m_pulseRow1) << " - " << std::bitset<32>(m_pulseRow2) << endl;
  cout << "Masking column: " << std::bitset<16>(m_maskCol) << endl;
}

void MiniMaltaMaskLoop::PrepareLoop_Single() {
  if (m_verbose) cout << "I will be preparing stage: " <<  m_loopIndex << " for strategy: SINGLE " << endl;
  
  //loop will proceed over all the col in a given row

  //pixX
  int pixY= (int)(m_loopIndex/16);
  int pixX= m_loopIndex%16;
  m_posX.push_back( pixX );
  m_posY.push_back( pixY );
   
  for(uint32_t i=0; i<m_posX.size(); i++){
    m_pulseCol |= 1<<m_posX[i];
  }
  m_pulseCol = 
  m_maskCol  = m_pulseCol;
  
  for(uint32_t i=0; i<m_posY.size(); i++){
    if (m_posY[i]<32) m_pulseRow1 |= 1<<m_posY[i];
    else              m_pulseRow2 |= 1<<(m_posY[i]-32);
  }
  if (m_verbose) Dump();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void MiniMaltaMaskLoop::PrepareLoop_2() {
  if (m_verbose) cout << "I will be preparing stage: " <<  m_loopIndex << " for strategy: MASK_2 " << endl;
  
  int OpixY= (int)(m_loopIndex/16);
  int pixY=0;
  //ugly as hell
  if (OpixY<8) pixY=OpixY;
  else if (OpixY<16) pixY=OpixY+8;
  else if (OpixY<24) pixY=OpixY+16;
  else if (OpixY<31) pixY=OpixY+24;
  int pixX= m_loopIndex%16;
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY+8 );
  //m_posY.push_back( pixY+32 );   

  for(uint32_t i=0; i<m_posX.size(); i++){
    m_pulseCol |= 1<<m_posX[i];
  }
  m_pulseCol = 
  m_maskCol  = m_pulseCol;
  
  for(uint32_t i=0; i<m_posY.size(); i++){
    if (m_posY[i]<32) m_pulseRow1 |= 1<<m_posY[i];
    else              m_pulseRow2 |= 1<<(m_posY[i]-32);
  }
  if (m_verbose) Dump();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void MiniMaltaMaskLoop::PrepareLoop_2dc() {
  if (m_verbose) cout << "I will be preparing stage: " <<  m_loopIndex << " for strategy: MASK_2DC " << endl;
  
  int pixY=(int)(m_loopIndex%64);
  int pixX= (int)(m_loopIndex/64);
  m_posX.push_back( 2*pixX   );
  m_posX.push_back( 2*pixX+1 );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );

  for(uint32_t i=0; i<m_posX.size(); i++){
    m_pulseCol |= 1<<m_posX[i];
  }
  m_pulseCol = 
  m_maskCol  = m_pulseCol;
  
  for(uint32_t i=0; i<m_posY.size(); i++){
    if (m_posY[i]<32) m_pulseRow1 |= 1<<m_posY[i];
    else              m_pulseRow2 |= 1<<(m_posY[i]-32);
  }
  if (m_verbose) Dump();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void MiniMaltaMaskLoop::PrepareLoop_4() {
  if (m_verbose) cout << "I will be preparing stage: " <<  m_loopIndex << " for strategy: MASK_4 " << endl;
  
  int pixY=(int)(m_loopIndex%64);
  int pixX= (int)(m_loopIndex/64);
  m_posX.push_back( 4*pixX   );
  m_posX.push_back( 4*pixX+1 );
  m_posX.push_back( 4*pixX+2 );
  m_posX.push_back( 4*pixX+3 );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );

  /*
  int pixY= (int)(m_loopIndex/16);
  int pixX= m_loopIndex%16;
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY+16 );
  m_posY.push_back( pixY+32 );   
  m_posY.push_back( pixY+48 );
  */

  for(uint32_t i=0; i<m_posX.size(); i++){
    m_pulseCol |= 1<<m_posX[i];
  }
  m_pulseCol = 
  m_maskCol  = m_pulseCol;
  
  for(uint32_t i=0; i<m_posY.size(); i++){
    if (m_posY[i]<32) m_pulseRow1 |= 1<<m_posY[i];
    else              m_pulseRow2 |= 1<<(m_posY[i]-32);
  }
  if (m_verbose) Dump();
}


void MiniMaltaMaskLoop::PrepareLoop_8() {
  if (m_verbose) cout << "I will be preparing stage: " <<  m_loopIndex << " for strategy: MASK_8 " << endl;
  
  int pixY= (int)(m_loopIndex/16);
  int pixX= m_loopIndex%16;
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posY.push_back( pixY );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY+8 );
  m_posY.push_back( pixY+16 );   
  m_posY.push_back( pixY+24 );
  m_posY.push_back( pixY+32 );
  m_posY.push_back( pixY+40 );   
  m_posY.push_back( pixY+48 );
  m_posY.push_back( pixY+56 );

  for(uint32_t i=0; i<m_posX.size(); i++){
    m_pulseCol |= 1<<m_posX[i];
  }
  m_pulseCol = 
  m_maskCol  = m_pulseCol;
  
  for(uint32_t i=0; i<m_posY.size(); i++){
    if (m_posY[i]<32) m_pulseRow1 |= 1<<m_posY[i];
    else              m_pulseRow2 |= 1<<(m_posY[i]-32);
  }
  if (m_verbose) Dump();
}


void MiniMaltaMaskLoop::PrepareLoop_16() {
  if (m_verbose) cout << "I will be preparing stage: " <<  m_loopIndex << " for strategy: MASK_16 " << endl;

  /*
  int pixX= 2*(m_loopIndex%8);
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  m_posX.push_back( pixX );
  m_posX.push_back( pixX+1 );
  int pixY= (int)(m_loopIndex/8);
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY+8 );
  m_posY.push_back( pixY+8 );
  m_posY.push_back( pixY+16 );
  m_posY.push_back( pixY+16 );
  m_posY.push_back( pixY+24 );
  m_posY.push_back( pixY+24 );
  m_posY.push_back( pixY+32 );
  m_posY.push_back( pixY+32 );
  m_posY.push_back( pixY+40 );
  m_posY.push_back( pixY+40 );
  m_posY.push_back( pixY+48 );
  m_posY.push_back( pixY+48 );
  m_posY.push_back( pixY+56 );
  m_posY.push_back( pixY+56 );
  */
  //int pixX= (m_loopIndex%16);
  m_posX.push_back( 0 );
  m_posX.push_back( 1 );
  m_posX.push_back( 2 );
  m_posX.push_back( 3 );
  m_posX.push_back( 4 );
  m_posX.push_back( 5 );
  m_posX.push_back( 6 );
  m_posX.push_back( 7 );
  m_posX.push_back( 8 );
  m_posX.push_back( 9 );
  m_posX.push_back(10 );
  m_posX.push_back(11 );
  m_posX.push_back(12 );
  m_posX.push_back(13 );
  m_posX.push_back(14 );
  m_posX.push_back(15 );

  int pixY= (int)(m_loopIndex);
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );
  m_posY.push_back( pixY );

  for(uint32_t i=0; i<m_posX.size(); i++){
    m_pulseCol |= 1<<m_posX[i];
  }
  m_pulseCol = 
  m_maskCol  = m_pulseCol;
  
  for(uint32_t i=0; i<m_posY.size(); i++){
    if (m_posY[i]<32) m_pulseRow1 |= 1<<m_posY[i];
    else              m_pulseRow2 |= 1<<(m_posY[i]-32);
  }

  if (m_verbose) Dump();
}
