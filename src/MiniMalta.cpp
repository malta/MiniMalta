//Minimalta class
#include "MiniMalta/MiniMalta.h"
#include <math.h>
#include <map>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <bitset>

using namespace std;

MiniMalta::MiniMalta(){
  verbose=false;

  defs[SC_VCASN]=0x07F;
  defs[SC_VCASP]=0x010;
  defs[SC_VRESET_D]=0x085;//0x073;
  defs[SC_VRESET_P]=0x0FF;//0x04A;
  defs[SC_VPULSE_H]=0x0FF;
  defs[SC_VPULSE_L]=0x000;
  defs[SC_VCLIP]=0x0FF;
  defs[SC_ICASN]=0x014;
  defs[SC_IBIAS]=0x064;
  defs[SC_ITHR]=0x014;
  defs[SC_IDB]=0x064;
  defs[SC_IRESET]=0x0FF;
  defs[SC_MASK_DIAG]=0xFFFF;
  defs[SC_MASK_COL]=0xFFFF;
  defs[SC_MASK_ROW_1]=0xFFFFFFFF;
  defs[SC_MASK_ROW_2]=0xFFFFFFFF;
  defs[SC_PULSE_COL]=0x0002;
  defs[SC_PULSE_ROW_1]=0x00000003;
  defs[SC_PULSE_ROW_2]=0x00000000;
  defs[SC_PIX_MON_IDAC]=0x95;
  defs[SC_ENABLE_CONFIG]=0x000B;
  defs[SC_TEMP_0]=0x0390;
  defs[SC_TEMP_1]=0xD27D;
  defs[SC_TEMP_2]=0x8000;
  defs[SC_DTU_1]=0x80CA;
  defs[SC_DTU_2]=0x0071;
  defs[SC_SEU_COUNT]=0x0;
  
  names[SC_VCASN]="SC_VCASN";
  names[SC_VCASP]="SC_VCASP";
  names[SC_VRESET_D]="SC_VRESET_D";
  names[SC_VRESET_P]="SC_VRESET_P";
  names[SC_VPULSE_H]="SC_VPULSE_H";
  names[SC_VPULSE_L]="SC_VPULSE_L";
  names[SC_VCLIP]="SC_VCLIP";
  names[SC_ICASN]="SC_ICASN";
  names[SC_IBIAS]="SC_IBIAS";
  names[SC_ITHR]="SC_ITHR";
  names[SC_IDB]="SC_IDB";
  names[SC_IRESET]="SC_IRESET";
  names[SC_MASK_DIAG]="SC_MASK_DIAG";
  names[SC_MASK_COL]="SC_MASK_COL";
  names[SC_MASK_ROW_1]="SC_MASK_ROW_1";
  names[SC_MASK_ROW_2]="SC_MASK_ROW_2";
  names[SC_PULSE_COL]="SC_PULSE_COL";
  names[SC_PULSE_ROW_1]="SC_PULSE_ROW_1";
  names[SC_PULSE_ROW_2]="SC_PULSE_ROW_2";
  names[SC_PIX_MON_IDAC]="SC_PIX_MON_IDAC";
  names[SC_ENABLE_CONFIG]="SC_ENABLE_CONFIG";
  names[SC_TEMP_0]="SC_TEMP_0";
  names[SC_TEMP_1]="SC_TEMP_1";
  names[SC_TEMP_2]="SC_TEMP_2";
  names[SC_DTU_1]="SC_DTU_1";
  names[SC_DTU_2]="SC_DTU_2";
  names[SC_SEU_COUNT]="SC_SEU_COUNT";
  
  m_fifo_info=0;
}

MiniMalta::~MiniMalta(){}

void MiniMalta::SetVerbose(bool enable){
  verbose=enable;
}

bool MiniMalta::Connect(std::string connstr){
    m_ipb = new ipbus::Uhal(connstr);
    uint32_t version;
    m_ipb->Read(0, version);
    cout << endl << "MiniMalta::Connect" << " VERSION: " << version << endl << endl;
    return m_ipb->IsSynced();
}

void MiniMalta::SetIPbus(ipbus::Uhal * ipb){
  m_ipb=ipb;
}

ipbus::Uhal * MiniMalta::GetIPbus(){
  return m_ipb;
}

void MiniMalta::Pulse(bool update, uint32_t prev){
  uint32_t value=prev;
  if (update) {
    m_ipb->Read(SCCONTROL,value);
    /////cout << "VALERIO SAYS: " << value << endl;
  }
  vector<uint32_t> commands;
  /* //old single 3mu sec pulse
  for (unsigned int V=0; V<50; V++) {
    commands.push_back( value | (1<< SC_PULSE) );
  }   
  commands.push_back( value &  ~(1 << SC_PULSE) );
  */
  //int nP=2;
  unsigned int V=0;
  //for (unsigned int p=0; p<nP; p++) {

/*
  commands.push_back( value | (1<< SC_PULSE) | (1<<TRIGGER)     );
  commands.push_back( (value | (1<< SC_PULSE) ) &  ~(1 <<TRIGGER) );
  for (V=0; V<15; V++) commands.push_back( value | (1<< SC_PULSE) );
  for (V=0; V<13; V++) commands.push_back( value &  ~(1 << SC_PULSE) );

  //commands.push_back( value | (1<< SC_PULSE) | (1<<TRIGGER)     );
  //commands.push_back( (value | (1<< SC_PULSE) ) &  ~(1 <<TRIGGER) );
  for (V=0; V<15; V++) commands.push_back( value | (1<< SC_PULSE) );
  for (V=0; V<13; V++) commands.push_back( value &  ~(1 << SC_PULSE) );
  
  //commands.push_back( value | (1<< SC_PULSE) | (1<<TRIGGER)     );
  //commands.push_back( (value | (1<< SC_PULSE) ) &  ~(1 <<TRIGGER) );
  for (V=0; V<15; V++) commands.push_back( value | (1<< SC_PULSE) );
  for (V=0; V<13; V++) commands.push_back( value &  ~(1 << SC_PULSE) );
  
  //commands.push_back( value | (1<< SC_PULSE) | (1<<TRIGGER)     );
  //commands.push_back( (value | (1<< SC_PULSE) ) &  ~(1 <<TRIGGER) );
  for (V=0; V<15; V++) commands.push_back( value | (1<< SC_PULSE) );
  for (V=0; V<13; V++) commands.push_back( value &  ~(1 << SC_PULSE) );
  
  //commands.push_back( value | (1<< SC_PULSE) | (1<<TRIGGER)     );
  //commands.push_back( (value | (1<< SC_PULSE) ) &  ~(1 <<TRIGGER) );
  for (V=0; V<15; V++) commands.push_back( value | (1<< SC_PULSE) );
  commands.push_back( value &  ~(1 << SC_PULSE) );

  //}
*/

  // IVAN'STUFF FOR LONGER PULSE
  commands.push_back( value | (1<< SC_PULSE) | (1<<TRIGGER)     );
  commands.push_back( (value | (1<< SC_PULSE) ) &  ~(1 <<TRIGGER) );
  for (V=0; V<200; V++) commands.push_back( value | (1<< SC_PULSE) );
  for (V=0; V<15; V++) commands.push_back( value &  ~(1 << SC_PULSE) );

  m_ipb->Write(SCCONTROL,commands,true);
  usleep(10);
}

void MiniMalta::PulseN(uint32_t numPulses, uint32_t length){
  uint32_t value;
  m_ipb->Read(SCCONTROL,value);
  vector<uint32_t> commands;
  commands.push_back( value | (1<< SC_PULSE) | (1<<TRIGGER)     );
  commands.push_back( (value | (1<< SC_PULSE) ) &  ~(1 <<TRIGGER) );
  for (uint32_t V=0; V<length; V++) commands.push_back( value | (1<< SC_PULSE) );
  for (uint32_t V=0; V<15; V++) commands.push_back( value &  ~(1 << SC_PULSE) );
  for (unsigned int numPulse=0; numPulse<numPulses; numPulse++) {
    m_ipb->Write(SCCONTROL,commands,true);
    usleep(10);
  }
}


void MiniMalta::Trigger(bool update, uint32_t prev){
  uint32_t value=prev;
  if (update)   m_ipb->Read(SCCONTROL,value);
  //std::cout << "word is: " << std::bitset<32>(value) << std::endl;
  vector<uint32_t> commands;
  commands.push_back( value | (1<<TRIGGER) );
  commands.push_back( value &  ~(1 <<TRIGGER) );
  //for (unsigned i=0; i<100; i++)  commands.push_back( value &  ~(1 <<TRIGGER) );
  //commands.push_back( value | (1<<TRIGGER) );
  //commands.push_back( value &  ~(1 <<TRIGGER) );
  //for (unsigned int c=0; c<commands.size(); c++) {
  //  cout << " " << std::bitset<32>(commands.at(c)) << " , ";
  //}
  //cout << endl;
  m_ipb->Write(SCCONTROL,commands,true);
  usleep(10);
}

void MiniMalta::SetSCClock(bool enable){
  //pls note that the logic is reverted: bit up=disable
  uint32_t value;
  m_ipb->Read(SCCONTROL,value);
  if (enable==false) {
    m_ipb->Write(SCCONTROL,value | (1<< SC_CLKEN) );
  } else {
    m_ipb->Write(SCCONTROL,value & ~(1 << SC_CLKEN));
  }

}

void MiniMalta::SetSlowReadOut(bool enable){
  //pls note that the logic is reverted: bit up=disable
  uint32_t value;
  m_ipb->Read(SCCONTROL,value);
  if (enable==true) {
    m_ipb->Write(SCCONTROL,value | (1<< SC_SLEN) );
  } else {
    m_ipb->Write(SCCONTROL,value & ~(1 << SC_SLEN));
  }

}


void MiniMalta::SetSlowReadOutFifo(bool enable){        
  uint32_t value;
  m_ipb->Read(RO_CONFIG_1,value);
  if (enable==true) {
    m_ipb->Write(RO_CONFIG_1,value | (1 << RO_SLOW_FIFO_EN) );     
  } else {
    m_ipb->Write(RO_CONFIG_1,value & ~(1 << RO_SLOW_FIFO_EN));
  }
}

void MiniMalta::ResetSlowFifo(){        
  uint32_t value;
  m_ipb->Read(2,value);   

  vector<uint32_t> cmds;
  cmds.push_back(value | (1<< 16) | (1<< 17));
  cmds.push_back(value & ~(1<<16));
  cmds.push_back(value & ~(1<<17));
  m_ipb->Write(2,cmds,true);

}

void MiniMalta::SetConfigFromString(const std::string& str){
  
  vector<std::string> tokens;
  std::string delimiters = " :=\n\r,;.-";
  
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);
  
  while (std::string::npos != pos || std::string::npos != lastPos){
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
  
  for(uint i=0; i<tokens.size();i+=2){
    try{
      uint32_t reg_num=0;
      uint32_t reg_value=0;
      // check if reg value is provided in hex format
      if(tokens.at(i+1).find("0x") != std::string::npos) reg_value = std::stoi(tokens.at(i+1), 0, 16);
      else reg_value = std::stoi(tokens.at(i+1));
      
      // finding mapped value in "names" map
      map<uint32_t, std::string> ::iterator itr;
      bool reg_found = false;
      for(itr = names.begin(); itr != names.end(); ++itr){
	if(itr->second == tokens.at(i)){
	  reg_num = itr->first;
	  reg_found = true;
	}
      }
      if(reg_found){
	cout<< "MiniMalta::SetConfigFromTextFile: "
	    << "write " << tokens.at(i) << " "
	    << "(0x" << hex << reg_num << dec << ") "
	    << "value: " << reg_value << " "
	    << "(0x" << hex << reg_value << dec << ")" << endl;
	m_ipb->Write(reg_num,reg_value);
      }else{
	cout << "MiniMalta::SetConfigFromTextFile: "
	     << "Cannot find [" << tokens.at(i) << "] in the register map !!!" << endl;
      }
    }catch(std::invalid_argument& ia){
      cout << "MiniMalta::SetConfigFromTextFile: "
	   << "Unable to parse [" << tokens.at(i) << " " << tokens.at(i+1) << "]" << endl;
    }
  }
  
}



bool MiniMalta::IsFifoIpSlowEmpty(){
  bool  FifoEmpty = ( (m_fifo_info >> RO_INFO_SLOW_EMPTY) & 0x1)!=0; 
  return FifoEmpty;
}

bool MiniMalta::IsFifoIpSlowProgEmpty(){
  bool  FifoEmpty = ( (m_fifo_info >> RO_INFO_SLOW_PROG_EMPTY) & 0x1)!=0; 
  return FifoEmpty;
}

bool MiniMalta::IsFifoIpSlowAlmostEmpty(){
  bool  FifoEmpty = ( (m_fifo_info >> RO_INFO_SLOW_ALMOST_EMPTY) & 0x1)!=0; 
  return FifoEmpty;
}

bool MiniMalta::IsFifoIpSlowFull(){
  bool  FifoFull = ( (m_fifo_info >> RO_INFO_SLOW_FULL) & 0x1)!=0; 
  return FifoFull;
}

void MiniMalta::GetFifoStatus(bool quiet){

  m_fifo_info=0;
  m_ipb->Read(RO_INFO,m_fifo_info); 
 
  if (!quiet) {
    std::cout << "word is: " << std::bitset<32>(m_fifo_info) << std::endl;
  }

}

uint32_t MiniMalta::GetFifoFillStatus(){
  m_fifo_fill_status = 0;
  m_ipb->Read(IPBUS_FIFO_FILL_STATUS,m_fifo_fill_status);
  return m_fifo_fill_status;
}


void MiniMalta::SetFastClock(bool enable){
  uint32_t value;
  m_ipb->Read(SCCONTROL,value);
  if (enable==false) {
    m_ipb->Write(SCCONTROL,value | (1<< FAST_K_EN) );
  } else {
    m_ipb->Write(SCCONTROL,value & ~(1 << FAST_K_EN));
  }
  //uint32_t value2;
  //m_ipb->Read(SCCONTROL,value2);
  //std::cout << "SCCONTROL FASTclk: " << std::hex << value << std::endl;
  //std::cout << "SCCONTROL FASTclk: " << value << "  ..... before: " << value2 << std::endl;
}


void MiniMalta::SetSlowClock(bool enable){
  uint32_t value;
  m_ipb->Read(SCCONTROL,value);
  if (enable==false) {
    m_ipb->Write(SCCONTROL,value | (1<< SLOW_K_EN) );
  } else {
    m_ipb->Write(SCCONTROL,value & ~(1 << SLOW_K_EN));
  }
  //uint32_t value2;
  //m_ipb->Read(SCCONTROL,value2);
  //std::cout << "SCCONTROL FASTclk: " << std::hex << value << std::endl;
  //std::cout << "SCCONTROL SLOWclk: " << value << "  ..... before: " << value2 << std::endl;
}


bool MiniMalta::Send(){
  SetSCClock(true);  
  uint32_t value;
  m_ipb->Read(SCCONTROL,value);
  vector<uint32_t> cmds;
  cmds.push_back(value |  (1<<2));
  cmds.push_back(value & ~(1<<2));
  cmds.push_back(value |  (1<<3));
  cmds.push_back(value & ~(1<<3));
  m_ipb->Write(SCCONTROL,cmds,true);
  usleep(500);
  Recv();
  SetSCClock(false);
  return true;
}

bool MiniMalta::Recv(){
  SetSCClock(true);
  uint32_t value;
  m_ipb->Read(SCCONTROL,value);
  vector<uint32_t> cmds;
  cmds.push_back(value |  (1<<0));
  cmds.push_back(value & ~(1<<0));
  cmds.push_back(value |  (1<<1));
  cmds.push_back(value & ~(1<<1));
  m_ipb->Write(SCCONTROL,cmds,true);
  usleep(500);
  SetSCClock(false);
  return true;
}

void MiniMalta::Dump(bool update){
  if(verbose){cout << "MiniMalta::Dump update=" << update << endl;}
  if(update){ReadAll();}
  for( uint32_t a = SC_REG0; a <= SC_REGN; a++) {
    string test="";
    if (values_2[a]!=values[a]) test="  ## MISMATCH ##";
    //cout << " " << names[a] << " = 0x" << hex << values_2[a] << hex << "   expected: " << hex << values[a] << hex << test <<endl;
    //cout << setw(20) << names[a] << " = " << MakeBinary(values_2[a]) 
         //<< "   expected: " << MakeBinary(values[a]) 
         //<< " (hex: " << hex << setw(5) << values_2[a] << "/" << setw(5) << values[a] << " )" << test << dec << endl;
  }
}

std::string MiniMalta::MakeBinary(uint32_t val){
  std::string binString;
  std::string padString;
  //get binary expression of val
  ostringstream test;
  test << std::bitset<32>(val);
  return test.str();
  while(val != 0){
    binString += std::to_string(val%2);
    val /= 2;
  }
  //do 32 bit padding
  for(unsigned int i = 0; i < 32-binString.length(); ++i) padString += "0";
  return "0b"+padString+binString;
}


void MiniMalta::Write(int pos, int value, bool force){
  if(verbose) cout << " MiniMalta R[" << pos << "] <= 0x" << hex << value << dec << endl;
  values[pos]=value;
  if(force) m_ipb->Write(pos, value);
}

void MiniMalta::WriteAll(){
  //Create a vector with all the configuration
  vector <uint32_t> cachewords;
  for( uint32_t a = SC_REG0; a <= SC_REGN; a++) {
    cachewords.push_back(values[a]);
    if(verbose) cout << " MiniMalta R[" << a << "] <= 0x" << hex << values[a] << dec << endl;
  }
  //Write all the slow control registers in one go
  m_ipb->Write(SC_REG0, cachewords);
}

void MiniMalta::ReadAll(){
  //Create an emtpy vector to read the configuration
  vector <uint32_t> cachewords;
  //Read all the configuration in one go
  m_ipb->Read(SC_REG0, cachewords, SC_NREGS);
  //Set the internal memories to those values
  for( uint32_t a = SC_REG0; a <= SC_REGN; a++) {
    if(verbose) cout << " MiniMalta R[" << a << "] = 0x" << hex << cachewords[a-SC_REG0] << dec << endl;
    values_2[a]=cachewords[a-SC_REG0];
  }
}

uint32_t MiniMalta::Read(int pos, bool force){
  if(force){
    uint32_t cacheWord;
    m_ipb->Read(pos, cacheWord);
    values_2[pos]=cacheWord;
  }
  return values_2[pos];
}

void MiniMalta::SetDefaults(bool force){
  for( uint32_t a = SC_REG0; a <= SC_REGN; a++) {
    values[a]=defs[a];
  }
  if(force){
    WriteAll();
  }
}

uint32_t MiniMalta::GetDefault(int pos){
  return defs[pos];
}

//Minimalta
bool MiniMalta::IsOverriden(int pos){
  return (values[pos] >> 9  & 0x1);
}

void MiniMalta::Override(int pos, bool flag){
  uint32_t mask= 0xFFFF ^ (1<<9);
  uint32_t cacheWord;
  m_ipb->Read(pos,cacheWord);
  if (flag == true){
    m_ipb->Write(pos, cacheWord | (1<<9) );
    values[pos]=cacheWord | (1<<9) ;
  }else{
	m_ipb->Write(pos, cacheWord & mask );
	values[pos]=cacheWord & mask;
  }
}

bool MiniMalta::IsMonitored(int pos){
    return (values[pos] >> 8  & 0x1);
}

void MiniMalta::Monitor(int pos, bool flag){
    std::cout << flag;
    std::cout << pos;
    uint32_t mask= 0xFFFFFFFF ^ (1<<8);
    uint32_t cacheWord;
    m_ipb->Read(pos,cacheWord);
    if (flag == true){
	m_ipb->Write(pos, cacheWord | (1<<8) );
	values[pos]=cacheWord | (1<<8) ;
    }else{
	m_ipb->Write(pos, cacheWord & mask );
	values[pos]=cacheWord & mask;
    }
}

uint32_t MiniMalta::decodeHotEncoded(int val){
    return 1 + log(val)/log(2);
}

uint32_t MiniMalta::getValue(int pos){//IS HOT ENCODED
    return decodeHotEncoded(values[pos]);
}

void MiniMalta::Reset(){
  uint32_t value;
  m_ipb->Read( SCCONTROL,value);
  m_ipb->Write(SCCONTROL,value | (1<< SC_RESET) );
  m_ipb->Write(SCCONTROL,value & ~(1 << SC_RESET));

  //m_ipb->Write(SCCONTROL,value | (1<< RO_RESET) );
  //m_ipb->Write(SCCONTROL,value & ~(1 << RO_RESET));

  //m_ipb->Write(1,16); 
  //m_ipb->Write(1,0); 
  usleep(2000);
}

int MiniMalta::TestMethod(){
  /*
    vector <unsigned int> cachewords;
    m_ipb->Read(4, cachewords, 26);
    for( int a = 4; a < 27; a = a + 1 ) {
	values[a]=cachewords[a-4];
    }
    return false;*/
  std::cout << "MiniMalta TestMethod...\n";
  for(auto it = defs.cbegin(); it != defs.cend(); ++it)
    {
      std::cout << it->first << " " << it->second << "\n";
    }
  return defs[4];
}


void MiniMalta::ReadMaltaWord(uint32_t * values, uint32_t numwords){
  m_ipb->Read(44,values,numwords,true);
}


bool MiniMalta::CheckConsistency() {
  ReadAll();
  bool check=true;
  for( uint32_t a = SC_REG0+1; a <= SC_REGN-1; a++) {
    if (values_2[a]!=values[a]) {
      //Dump(false);
      //return false;
      check=false;
      break;
    }
  }
  if (check==false) {
    //cout << endl;
    //Dump(false);
    return false;
  }
  return true;
}

void MiniMalta::SetExternalL1A(bool enable){
  uint32_t cacheWord;
  m_ipb->Read(TIMING_CONTROL,cacheWord);
  if(enable) cacheWord |= (1<<0);
  else       cacheWord &= (~(1<<0));
  //cout << " " << std::bitset<32>(cacheWord) << " , ";
  m_ipb->Write(TIMING_CONTROL, cacheWord);
  usleep(2000);  
}

void MiniMalta::ResetL1Counter(){
  uint32_t cacheWord;
  m_ipb->Read(TIMING_CONTROL,cacheWord);
  uint32_t maskRESET= 0xFFFFFFFF ^ (1<<1);
  vector<uint32_t> cmds;
  cmds.push_back(cacheWord | (1<<1));
  cmds.push_back(cacheWord & maskRESET);
  m_ipb->Write(TIMING_CONTROL,cmds,true);
  usleep(2000);
}

uint32_t MiniMalta::GetL1Acounter(){
  uint32_t cacheWord;
  m_ipb->Read(L1A_COUNTER, cacheWord);
  return cacheWord;
}

string MiniMalta::GetRegisterName(uint32_t reg){
  if(reg<SC_REG0 || reg>SC_REGN){ return ""; }
  return names[reg];
}

void MiniMalta::MaskAll(bool enable){
  if(enable){
    m_ipb->Write(SC_MASK_DIAG, 0xFFFF);
    m_ipb->Write(SC_MASK_COL,  0xFFFF);
    m_ipb->Write(SC_MASK_ROW_1,0xFFFFFFFF);
    m_ipb->Write(SC_MASK_ROW_2,0xFFFFFFFF);
  }else{
    m_ipb->Write(SC_MASK_DIAG, 0x0);
    m_ipb->Write(SC_MASK_COL,  0x0);
    m_ipb->Write(SC_MASK_ROW_1,0x0);
    m_ipb->Write(SC_MASK_ROW_2,0x0);
  }
}
